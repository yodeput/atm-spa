<?php

namespace App\Imports;

use App\Models\Master\Customer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomersImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['kode'] != null) {
                $data = DB::table('m_customer')->where('code',  $row['kode'] )->first();
                if($data!=null){
                    $data = Customer::find($data->id);
                    $data->code = $row['kode'];
                    $data->name = $row['nama'];
                    $data->save();
                } else {
                    Customer::create([
                        'code' => $row['kode'],
                        'name' => $row['nama'],
                    ]);
                }

            }
        }
    }
}
