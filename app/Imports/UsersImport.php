<?php

namespace App\Imports;

use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Avatar;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['ce_code'] != null) {
                $ce_code = str_replace(' ', '', $row['ce_code']);
                $qlid = str_replace(' ', '', $row['qlid']);
                $ce_name = $row['ce_name'];
                $email = $row['email'];
                $ce_name= $row['ce_name'];
                $service_base = $row['service_base'];
                //$region = str_replace(' ', '', $row['region']);
                //$regionDb = Region::where('code', 'LIKE', '%' . $region . '%')->orWhere('name', 'LIKE', '%' . $region . '%')->first();
                $service_baseDb = ServiceBase::where('code', 'LIKE', '%' . $service_base . '%')->orWhere('name', 'LIKE', '%' . $service_base . '%')->first();

                    $user = User::create([
                        'name' => $ce_name,
                        'username' => $qlid,
                        'ce_code' => $ce_code,
                        'qlid' => $qlid,
                        'email' => $email,
                        'password' => Hash::make('pass@word1'),
                        'service_base_id' => $service_baseDb !== null ? $service_baseDb->id : null,
                        'region_id' => $service_baseDb !== null ? $service_baseDb->region_id : null,
                        'is_active' => true,
                        'email_verified_at' => now()
                    ]);
                    $user->avatar = '/images/user.png';
                    $user->save();
            }
        }
    }
}
