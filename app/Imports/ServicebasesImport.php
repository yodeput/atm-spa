<?php

namespace App\Imports;

use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ServicebasesImport implements ToCollection, WithHeadingRow
{
    /*public function model(array $row)
    {
        $region = Region::query()
                ->where('code', 'LIKE', '%' .$row['region']. '%')
                ->get();
        $code =  $row['kode'];
        $name =  $row['nama'];
        return new ServiceBase([
                    'code' => $code,
                    'name' => $name,
                    'region_id' => $region[0]['id'],
        ]);
    }
}*/
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['kode'] !== null) {
                $regionCode = str_replace(' ', '', $row['region']);
                $servicebaseCode = str_replace(' ', '', $row['kode']);
                $region = Region::where('code', 'LIKE', '%' . $regionCode . '%')->orWhere('name', 'LIKE', '%' . $regionCode . '%')
                    ->first();
                $data = DB::table('m_servicebase')->where('code',  $servicebaseCode )->first();
                if($data!==null){
                    $data = ServiceBase::find($data->id);
                    $data->code = $servicebaseCode;
                    $data->name = $row['nama'];
                    $data->region_id = $region->id;
                    $data->save();
                } else {
                    ServiceBase::create([
                        'code' => $servicebaseCode,
                        'name' => $row['nama'],
                        'region_id' => $region->id,
                    ]);
                }
            }
        }
    }
}
