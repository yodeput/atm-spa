<?php

namespace App\Imports;

use App\Models\Master\PartNumber;
use App\Models\Visit\Visit;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PartNumberImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['type'] != null) {
                $part = PartNumber::where('part_number', $row['part_number'])->first();
                if($part){
                    $part->type = $row['type'];
                    $part->description = $row['description'];
                    $part->save();
                }else {
                    PartNumber::create([
                        'type' => $row['type'],
                        'part_number' => $row['part_number'],
                        'description' => $row['description'],
                    ]);
                }

            }
        }
    }
}
