<?php

namespace App\Imports;

use App\Models\Master\Customer;
use App\Models\Master\Location;
use App\Models\Master\ServiceBase;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Validators\ValidationException;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;


class LocationsImport implements ToCollection, WithValidation, WithHeadingRow, WithEvents
{

    use Importable, RegistersEventListeners;

    public static function beforeImport(BeforeImport $event)
    {
        $worksheet = $event->reader->getActiveSheet();
        $highestRow = $worksheet->getHighestRow(); // e.g. 10

        if ($highestRow < 2) {
            $error = \Illuminate\Validation\ValidationException::withMessages([]);
            $failure = new Failure(1, 'rows', [0 => 'Now enough rows!']);
            $failures = [0 => $failure];
            throw new ValidationException($error, $failures);
        }
    }

    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {

                $customer = str_replace(' ', '', $row['customer_id']);
                $serialNumber = str_replace(' ', '', $row['serial_number']);
                $servicebase = str_replace(' ', '', $row['servicebase']);
                $machineId = str_replace(' ', '', $row['machine_id']);
                $model = $row['model'];
                $name = $row['location_name'];
                $address = $row['address'];
                $city = $row['city'];
                $postal_code = $row['postal_code'];
                $install_date = $row['install_date'];
                $status = $row['status'];

                $customer = Customer::query()->where('code', 'LIKE', '%' . $customer . '%')
                        ->orWhere('name', 'LIKE', '%' . $customer . '%')->first();

                $servicebase = ServiceBase::query()->where('code', 'LIKE', '%' . $servicebase . '%')
                        ->orWhere('name', 'LIKE', '%' . $servicebase . '%')->first();

                $location = Location::withTrashed()->where('serial_number', $serialNumber)->orWhere('mc_id', $machineId)->first();

                $install_date = $install_date !== null  && $install_date !== '' && $install_date !== '-' ? \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($install_date) : null;

                if ($location) {
                    $location->mc_id = $machineId;
                    $location->model = $model;
                    $location->install_date = $install_date;
                    $location->location_name = $name;
                    $location->status = $status;
                    $location->customer_id = $customer !== null ? $customer->id : null;
                    $location->service_base_id = $servicebase !== null ? $servicebase->id : null;
                    $location->region_id = $servicebase !== null ? $servicebase->region_id : null;
                    $location->address = $address;
                    $location->city = $city;
                    $location->postal = $postal_code;
                    $location->save();

                } else {

                    Location::create([
                        'mc_id' => $machineId,
                        'model' => $model,
                        'install_date' => $install_date,
                        'location_name' => $name,
                        'status' => $status,
                        'serial_number' => $serialNumber,
                        'customer_id' => $customer !== null ? $customer->id : null,
                        'service_base_id' => $servicebase !== null ? $servicebase->id : null,
                        'region_id' => $servicebase !== null ? $servicebase->region_id : null,
                        'address' => $address,
                        'city' => $city,
                        'postal' => $postal_code,
                    ]);

        }
    }
}

public
function sheets(): array
{
    return [
        // Select by sheet index
        0 => new LocationsImport(),
    ];
}

public
function rules(): array
{
    return [

    ];
}
}
