<?php

namespace App\Imports;

use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use App\Models\Master\WoType;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class WoTypeImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['name'] != null) {
                WoType::create([
                    'description' => $row['description']?? null,
                    'name' => $row['name'],
                ]);
            }
        }
    }
}
