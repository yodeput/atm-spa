<?php

namespace App\Imports;

use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RegionsImport implements ToCollection, WithHeadingRow
{
    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            if ($row['kode'] != null) {
                Region::create([
                    'code' => $row['kode'],
                    'name' => $row['nama'],
                ]);
            }
        }
    }
}
