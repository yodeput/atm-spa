<?php

namespace App\Imports;

use App\Http\Controllers\Controller;
use App\Models\Master\Customer;
use App\Models\Master\Location;
use App\Models\Task\TaskDetail;
use App\Models\Task\TaskUser;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TaskImport extends Controller implements ToCollection, WithHeadingRow
{

    /**
     * @param Collection $rows
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected $task;

    public function __construct($task)
    {
        $this->task = $task;
    }

    public function collection(Collection $rows)
    {

        foreach ($rows as $row) {
            $ce_code = str_replace(' ', '', $row['ce_code']);
            $user = User::query()->where('ce_code', $ce_code)->first();
            $userId = null;
            if ($user) {
                $userId =  $user->id;
            }
            $taskUser = TaskUser::query()->where('task_id',$this->task->id)->where('user_id', $userId)->first();
            $location = Location::query()->where('serial_number', $row['serial_number'])->orWhere('mc_id', $row['serial_number'])->first();

            if($taskUser){
                if($location){
                    TaskDetail::create([
                        'type' => $row['type'],
                        'location_id' => $location->id,
                        'task_user_id' => $taskUser->id,
                    ]);
                }
            } else {
                $taskUser = TaskUser::create([
                    'task_id' => $this->task->id,
                    'user_id' => $userId
                ]);

                if($location){
                    TaskDetail::create([
                        'type' => $row['type'],
                        'location_id' => $location->id,
                        'task_user_id' => $taskUser->id,
                    ]);
                }
            }
        }
    }
}
