<?php

namespace App\Imports;

use App\Models\Customer;
use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Visit;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use OneSignal;

class OpenWoImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        //DB::table('t_open_wo')->truncate();
        foreach ($rows as $row) {
            if ($row['work_order'] != null) {
                $work_order = str_replace(' ', '', $row['work_order']);
                $ce_code = str_replace(' ', '', $row['ce_code']);
                $visit = Visit::query()->where('work_order', $work_order)->first();
                $user = User::query()->where('ce_code', $ce_code)->first();
                $userId = null;
                $openWo = OpenWo::query()->where('work_order', $work_order)->first();
                if ($user !== null) {
                    $userId =  $user->id;
                }

                if ($openWo !== null) {
                    $openWo->user_id = $userId;
                    if ($visit !== null) {
                        if($visit->checkout_time !== null){
                            $openWo->close_time = $visit->checkout_time;
                            $openWo->status = 'Close';
                        } else {
                            $openWo->close_time = null;
                            $openWo->status = 'On Progress';
                        }
                    }
                    $openWo->save();
                } else {
                    if ($visit === null) {
                        OpenWo::create([
                            'work_order' => $work_order,
                            'user_id' => $userId,
                            'close_time' => null,
                            'status' => 'Open',
                        ]);


                    } else {
                        if($visit->checkout_time !== null){
                            OpenWo::create([
                                'work_order' => $work_order,
                                'user_id' => $userId,
                                'close_time' => $visit->checkout_time,
                                'status' => 'Close',
                            ]);
                        } else {
                            OpenWo::create([
                                'work_order' => $work_order,
                                'user_id' => $userId,
                                'close_time' => null,
                                'status' => 'On Progress',
                            ]);
                        }

                    }

                }

            }
        }
    }
}
