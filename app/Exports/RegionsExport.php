<?php

namespace App\Exports;

use App\Models\Master\Region;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RegionsExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Region::query()->whereNull('deleted_at');
    }

    /**
     * @var Region $data
     */
    public function map($data): array
    {
        return [
            $data->code,
            $data->name,
        ];
    }

    public function headings(): array
    {
        return [
            'Code',
            'Region',
        ];
    }
}
