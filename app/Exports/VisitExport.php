<?php

namespace App\Exports;

use App\Http\Controllers\Controller;
use App\Models\Visit\Visit;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use Excel;
use Maatwebsite\Excel\Sheet;
use PhpOffice\PhpSpreadsheet\Cell\Hyperlink;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Throwable;

class VisitExport implements  FromQuery, WithMapping, WithHeadings, WithStyles, WithEvents, ShouldQueue
{
    use Exportable;
    protected $customer;
    protected $user;
    protected $role;
    protected $location;
    protected $start_end;
    

    public function __construct($customer, $user, $location, $role, $start_end)
    {
        $this->customer = $customer;
        $this->user = $user;
        $this->location = $location;
        $this->role = $role;
        $this->start_end = $start_end;
    }

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function query()
    {
        $query = Visit::query();

    /*    if (!$this->IsNullOrEmptyString($this->customer)) {
            $customer = $this->customer;
            $query->whereHas('location', function ($query) use ($customer) {
                $query->where('customer_id', $customer);
            });
        }

        if (!$this->IsNullOrEmptyString($this->location)) {
            $query->where('location_id', $this->location);
        }

        if (!$this->IsNullOrEmptyString($this->user)) {
            $user = $this->user;
            $query->where('user_id', $user);

        }*/
        if ($this->start_end) {
            $query->whereBetween('checkin_time', $this->start_end)
                ->whereBetween('checkout_time', $this->start_end);
        }


        /*$user = auth()->user();
        $role = $this->role;

        switch ($role) {
            case 'vendor':
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
                break;
            case 'engineer':
            case 'supervisor':
                $query->where('user_id', $user->id);
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                break;
        }*/
        $query->orderBy('id', 'asc')
            ->get()
            ->load('checklists');

        return $query;
    }
    /**
     * @var Visit $data
     */
    public function map($data): array
    {

        $this->checklist = $data->checklists;
        $dataRow = array($data->work_order,
            $data->location->serial_number ?? '-',
            $data->location->mc_id ?? '-',
            $data->location->customer->name ?? '-',
            $data->location->model ?? '-',
            $data->tipe_kunjungan,
            $data->detail_kunjungan ?? '-',
            $data->user->ce_code ?? '-',
            $data->location->location_name ?? '-',
            $data->location->service_base->name ?? '-',
            $data->location->region->code ?? '-',
            $data->pengelola,
            $data->pengelola_pic,
            $data->pengelola_phone,
            $data->created_at ?? '-',
            $data->checkin_time ?? '-',
            $data->checkout_time ??'-',
            $data->photo ? config('app.url').$data->photo  : '-',
            $data->photo_sn ? config('app.url').$data->photo_sn  : '-',
            $data->photo_ar ? config('app.url').$data->photo_ar  : '-',
            $data->latitude ?? '-',
            $data->longitude ?? '-',
            $data->location->address ?? '-',
            $data->location->city ?? '-',
            $data->location->postal ?? '-',);

        foreach ($this->checklist as $check){
            $dataCheck = $check->deskripsi . ' => ' .$check->value;
            if($check->photo){
                $dataCheck = $check->deskripsi . ' => ' .$check->value . '[ ' . config('app.url').$check->photo . ' ] ';
            }
            array_push($dataRow, $dataCheck);
        }

        return $dataRow;
    }

    public function headings(): array
    {

        $title = array('Work Order',
            'Serial Number',
            'Mesin_ID',
            'Customer',
            'Model',
            'Tipe_Kunjungan',
            'Problem_Reported',
            'ID_CE',
            'Nama_Lokasi',
            'Servicebase',
            'Region',
            'Penglola',
            'Penglola_PIC',
            'Penglola_PIC_Number',
            'Tanggal_Input',
            'Tanggal_Mulai',
            'Tanggal_Selesai',
            'Photo_Selfie',
            'Photo_SN',
            'Activity_Report',
            'Latitude',
            'Longitude',
            'Alamat',
            'Kota',
            'Kodepos',);


        return $title;
    }

    public function styles($sheet)
    {
        return [
            1    => ['font' => ['bold' => true]],
        ];
    }

    // ...
    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                /** @var Worksheet $sheet */
                foreach ($event->sheet->getColumnIterator('R') as $row) {
                    foreach ($row->getCellIterator() as $cell) {
                        if ($cell->getValue() !== null && str_contains($cell->getValue(), '://')) {

                            // Upd: Link styling added
                            $event->sheet->getStyle($cell->getCoordinate())->applyFromArray([
                                'font' => [
                                    'color' => ['rgb' => '0000FF'],
                                    'underline' => 'single'
                                ]
                            ]);
                        }
                    }
                }
            },
        ];
    }

    public function failed(Throwable $exception): void
    {

    }

}
