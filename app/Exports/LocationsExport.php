<?php

namespace App\Exports;

use App\Models\Location;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class LocationsExport implements FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */



    use Exportable;

    public function query()
    {
        $query = Location::query();
        $query->where('deleted_at', 'LIKE', '');
        return $query;
    }

    /**
     * @inheritDoc
     */
    public function collection()
    {
        return Location::all();
    }
}
