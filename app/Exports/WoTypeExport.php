<?php

namespace App\Exports;

use App\Models\Master\Region;
use App\Models\Master\WoType;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class WoTypeExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function query()
    {
        return WoType::query();
    }

    /**
     *
     * @var WoType $data
     */
    public function map($data): array
    {
        return [
            $data->name,
            $data->description,
        ];
    }

    public function headings(): array
    {
        return [
            'Name',
            'Description',
        ];
    }
}
