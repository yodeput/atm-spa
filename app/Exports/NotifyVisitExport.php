<?php

namespace App\Exports;

use App\Mail\VisitExportMail;
use App\User;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class NotifyVisitExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $filename;
    protected $uid;

    /**
     * NotifyVisitExport constructor.
     */
    public function __construct($filename, $uid)
    {
        $this->filename = $filename;
        $this->uid = $uid;
    }

    public function handle()
    {
        $user = User::find($this->uid);
        Mail::to($user->email)->send(new VisitExportMail($this->filename, $user));
    }
}
