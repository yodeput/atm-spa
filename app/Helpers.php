<?php
namespace App;

use Illuminate\Support\Facades\Cache;

class Helpers
{
    /**
     * Fetch Cached settings from database
     *
     * @return string
     */
    public static function settings($key)
    {
        return Cache::get('settings')->where('slug_name', $key)->first()->value;
    }
}
