<?php
namespace App\Traits;
use App\Models\Activity;
use carbon\carbon;
use Illuminate\Support\Facades\Auth;

trait ActivityTraits
{

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function logCreatedActivity($logModel,$changes,$request)
    {
        $activity = activity()
            ->causedBy(Auth::user())
            ->performedOn($logModel)
            ->withProperties(['attributes'=>$request])
            ->log('CREATE-'.$changes);
        $lastActivity = Activity::all()->last();

        return true;
    }

    public function logCreatedActivityNoModel($changes)
    {
        $activity = activity()
            ->causedBy(\Auth::user())
            ->withProperties(['attributes'=>''])
            ->log('CREATE-'.$changes);
        $lastActivity = Activity::all()->last();

        return true;
    }

    public function logUpdatedActivity($list,$before,$list_changes)
    {
        unset($list_changes['updated_at']);
        $old_keys = [];
        $old_value_array = [];
        if(empty($list_changes)){
            $changes = 'No attribute changed';

        }else{

            if(count($before)>0){

                foreach($before as $key=>$original){
                    if(array_key_exists($key,$list_changes)){

                        $old_keys[$key]=$original;
                    }
                }
            }
            $old_value_array = $old_keys;
            $changes = 'Attributes '.implode(', ',array_keys($old_keys)).' with '.implode(', ',array_values($old_keys)).' to '.implode(', ',array_values($list_changes));
        }

        $properties = [
            'attributes'=>$list_changes,
            'old' =>$old_value_array
        ];

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($list)
            ->withProperties($properties)
            ->log('UPDATE-'.$changes.' by '.\Auth::user()->name);

        return true;
    }

    public function logDeletedActivity($list,$changeLogs)
    {
        $attributes = $this->unsetAttributes($list);

        $properties = [
            'attributes' => $attributes->toArray()
        ];

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($list)
            ->withProperties($properties)
            ->log('DELETE-'.$changeLogs.' by '.\Auth::user()->name);

        return true;
    }

    public function logRecoveredActivity($list,$changeLogs)
    {
        $attributes = $this->unsetAttributes($list);

        $properties = [
            'attributes' => $attributes->toArray()
        ];

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($list)
            ->withProperties($properties)
            ->log('RESTORE-'.$changeLogs.' by '.\Auth::user()->name);

        return true;
    }

    public function logLoginDetails($user)
    {
        $updated_at = Carbon::now()->format('d/m/Y H:i:s');
        $properties = [
            'attributes' =>['name'=>$user->email,'description'=>'Login into the system by '.$updated_at]
        ];

        //$changes = 'User '.$user->email.' loged in into the system';
        $changes = 'LOGIN-'.$user->name.' / '.$user->email.'';

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($user)
            ->withProperties($properties)
            ->log($changes);

        return true;
    }

    public function logLogoutDetails($user)
    {
        $updated_at = Carbon::now()->format('d/m/Y H:i:s');
        $properties = [
            'attributes' =>['name'=>$user->email,'description'=>'Logout from system by '.$updated_at]
        ];

        $changes = 'LOGOUT-'.$user->name.' / '.$user->email.'';

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($user)
            ->withProperties($properties)
            ->log($changes);

        return true;
    }

    public function logGeneral($data, $changes)
    {
        $updated_at = Carbon::now()->format('d/m/Y H:i:s');
        $properties = [
            'attributes' =>['data'=>$data,'description'=>$updated_at]
        ];

        $activity = activity()
            ->causedBy(\Auth::user())
            ->performedOn($data)
            ->withProperties($properties)
            ->log($changes);

        return true;
    }

    public function unsetAttributes($model){
        unset($model->created_at);
        unset($model->updated_at);
        return $model;
    }

}
