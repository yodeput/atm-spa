<?php
namespace App\Traits;
use App\Models\Activity;
use carbon\carbon;
use Illuminate\Http\Response;

trait CustomResponse {
    public function HttpOk($error, $msg)
    {
        return response()->json([
            'error' => $error,
            'msg' => $msg,
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }

    public function HttpOkWithData($error, $msg, $data)
    {
        return response()->json([
            'error' => $error,
            'msg' => $msg,
            'data' => $data,
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }

    public function HttpBadRequest($error, $msg)
    {
        return response()->json([
            'error' => $error,
            'msg' => $msg,
        ])->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
    }

    public function HttpCreated($error, $msg)
    {
        return response()->json([
            'error' => $error,
            'msg' => $msg,
        ])->setStatusCode(Response::HTTP_CREATED, Response::$statusTexts[Response::HTTP_CREATED]);
    }


}
