<?php

namespace App;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use App\Providers\OAuthProvider;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Wildside\Userstamps\Userstamps;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Userstamps;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'ce_code', 'qlid', 'alamat', 'telepon', 'avatar', 'region_id', 'service_base_id', 'customer_id', 'is_active','email_verified_at','session_id'
    ];
    protected $guard_name = 'api';
    protected $with = ['customer','vendor', 'region', 'service_base'];
    protected $appends = [
        'avatar_url', 'display_name', 'code_username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatarUrlAttribute()
    {
        return config('filesystems.disks.azure.url') . 'avatars/' . $this->id . '/' . $this->avatar;
    }

    public function getDisplayNameAttribute()
    {
        return $this->ce_code ? $this->ce_code . ' | ' . $this->name : $this->username . ' | ' . $this->name ;
    }

    public function getCodeUsernameAttribute()
    {
        return $this->ce_code ?: $this->username;
    }

    public function getAllPermissionsAttribute()
    {
        $permissions = [];
        foreach (Permission::all() as $permission) {
            if (Auth::user()->can($permission->name)) {
                $permissions[] = $permission->name;
            }
        }
        return $permissions;
    }

    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Master\Customer');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Master\Region');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Master\Vendor');
    }

    public function service_base()
    {
        return $this->belongsTo('App\Models\Master\ServiceBase');
    }

}
