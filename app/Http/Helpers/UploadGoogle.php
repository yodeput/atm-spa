<?php namespace App\Http\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use DateTime;

class UploadGoogle {

    public function uploadVisit($file, $filename, $wo)
    {
        $date = new DateTime();
        $folder = $date->format('Y-m');
        $findFolder = $this->findFolder($folder);
        $folder = $findFolder['basename'];

        $findFolderSub = $this->findSubFolder($folder, $wo);
        $subFolder = $findFolderSub['basename'] ?? null;

        $pathFolder = "$folder/";
        $path = "$folder/$filename";

        if($subFolder){
            $pathFolder = "$folder/$subFolder/";
            $path = "$folder/$subFolder/$filename";
        }

        Storage::cloud()->put($path, $file);

        $contents = collect(Storage::cloud()->listContents($pathFolder, false));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first();


        $service = Storage::cloud()->getAdapter()->getService();
        $permission = new \Google_Service_Drive_Permission();
        $permission->setRole('reader');
        $permission->setType('anyone');
        $permission->setAllowFileDiscovery(false);

        if($file && $file['basename']){
            $permissions = $service->permissions->create($file['basename'], $permission);
        }

        return Storage::cloud()->url($file['path']);
    }

    public function uploadAtm($file, $filename)
    {
        $date = new DateTime();
        $folder = "ATM";
        $findFolder = $this->findFolder($folder);
        $folder = $findFolder['basename'];

        $pathFolder = "$folder/";
        $path = "$folder/$filename";

        Storage::cloud()->put($path, $file);

        $contents = collect(Storage::cloud()->listContents($pathFolder, false));
        $file = $contents
            ->where('type', '=', 'file')
            ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
            ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
            ->first(); // there can be duplicate file names!


        $service = Storage::cloud()->getAdapter()->getService();
        $permission = new \Google_Service_Drive_Permission();
        $permission->setRole('reader');
        $permission->setType('anyone');
        $permission->setAllowFileDiscovery(false);
        if($file && $file['basename']){
            $permissions = $service->permissions->create($file['basename'], $permission);
        }


        return Storage::cloud()->url($file['path']);
    }

    public function findFolder($folderName)
    {
        $contents = collect(Storage::cloud()->listContents('/', false));
        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $folderName)
            ->first();

        if(!$dir){
            return $this->createFolder($folderName);
        }

        return $dir;
    }

    public function findSubFolder($parent, $folderName)
    {
        $contents = collect(Storage::cloud()->listContents("/$parent", false));
        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $folderName)
            ->first();

        if(!$dir){
            Storage::cloud()->makeDirectory("$parent/$folderName");
            $contents = collect(Storage::cloud()->listContents("/$parent", false));
            return $contents->where('type', '=', 'dir')
                ->where('filename', '=', $folderName)
                ->first();
        }

        return $dir;
    }

    public function createFolder($folderName){
        Storage::cloud()->makeDirectory($folderName);
        $contents = collect(Storage::cloud()->listContents('/', false));

        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $folderName)
            ->first();


        return $dir;
    }
}
