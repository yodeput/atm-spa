<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Closure;
use JWTAuth;

class CheckSingleSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = $request->headers->get('authorization');
        $previous_session = Auth::User()->session_id;
        if ($previous_session !== str_replace("Bearer ","",$token)) {
            JWTAuth::setToken(str_replace("Bearer ","",$token))->invalidate();
            return response()->json(['error' => 'session expired'], 403);
        }
        return $next($request);
    }
}
