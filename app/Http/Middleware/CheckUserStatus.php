<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        //If the status is not approved redirect to login
        if (Auth::check() && Auth::user()->is_active != true) {

            Auth::logout();

            $request->session()->flash('alert-danger', 'Akun belum diaktifkan oleh Administrator');

            return redirect('/login')->with('error_login', 'Akun belum diverifikasi oleh Admin');

        }

        return $response;

    }
}
