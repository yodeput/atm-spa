<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\Master\Region;
use App\Models\Task\TaskDetail;
use App\Models\Task\TaskUser;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class TaskUserController extends Controller
{
    public function filter(Request $request)
    {
        $query = TaskUser::query()->where('task_id', $request->task_id);


        if ($request->user_id) {
            $query->where('user_id', $request->user_id);
        }

        if ($request->search) {
            $search = $request->search;
            $query->whereHas('user', function ($query) use ($search) {
                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('username', 'LIKE', '%' . $search . '%')
                    ->orWhereHas('region', function ($query2) use ($search) {
                        $query2->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('code', 'LIKE', '%' . $search . '%');
                    })->orWhereHas('service_base', function ($query2) use ($search) {
                        $query2->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('code', 'LIKE', '%' . $search . '%');
                    });
            });
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));


        return $data;
    }

    public function getByTask($id)
    {
        return TaskUser::query()->where('task_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request)
    {
        $taskUser = TaskUser::create([
            'task_id' => $request->task_id,
            'user_id' => $request->user_id
        ]);

        return $taskUser;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\TaskUser $taskUser
     * @return \Illuminate\Http\Response
     */
    public
    function show(TaskUser $taskUser)
    {
        return TaskUser::findOrFail($taskUser);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\TaskUser $taskUser
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Request $request)
    {
        $taskUser = TaskUser::findOrFail($request->id);
        $taskUser->user_id = $request->user_id;
        $taskUser->task_id = $request->task_id;
        $taskUser->save();
        return $taskUser;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\TaskUser $taskUser
     * @return \Illuminate\Http\Response
     */
    public
    function update(Request $request, TaskUser $taskUser)
    {
        //
    }


    public function destroy($id)
    {
        try {
            TaskUser::query()->where('id', $id)->delete();
//                TaskDetail::query()->where('task_user_id', $id)->delete();
//                TaskUser::destroy($id);
            return response()->json([
                'error' => false,
                'msg' => "Delete Task User Sukses",
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'exc' => $e->getMessage(),
                'msg' => "Delete Task User gagal",
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }
}
