<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Imports\OpenWoImport;
use App\Imports\TaskImport;
use App\Models\Master\Location;
use App\Models\Master\Region;
use App\Models\Task\Task;
use App\Models\Task\TaskDetail;
use App\Models\Task\TaskUser;
use App\Models\Visit\OpenWo;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class TaskController extends Controller
{

    use ActivityTraits;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filter(Request $request)
    {

        $query = Task::query();
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];
        $query = $query->whereHas('taskUser', function ($query) use ($role, $user) {
            if ($role === 'engineer' || $role === 'vendor') {
                $query->where('user_id', $user->id);
            }
            $query->whereHas('taskDetail', function (Builder $query2) {
            });
        });
        if ($request->search) {
            $query->where('description', $request->search);
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|object
     */
    public function store(Request $request)
    {
        $data = json_decode(file_get_contents($request->data), true);
        $task = Task::create([
            'description' => $data['description'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],

        ]);

        try {
            if ($request->file) {
                $file = $request->file('file');
                $nama_file = rand() . $file->getClientOriginalName();
                $file->move('storage/file_import/', $nama_file);
                Excel::import(new TaskImport($task), public_path('/storage/file_import/' . $nama_file));
            }
            if ($request->task_user) {
                foreach ($request->task_user as $value) {
                    $user = $value->user;
                    $userId = null;
                    if ($user) {
                        $userId = $user->id;
                    }
                    $task_detail = $value->task_detail;
                    $taskUser = TaskUser::query()->where('task_id', $task->id)->where('user_id', $userId)->first();


                    foreach ($task_detail as $taskDetail) {
                        $location = $taskDetail->location;

                        if ($taskUser) {
                            TaskDetail::create([
                                'type' => $taskDetail->type,
                                'location_id' => $location->id ?: null,
                                'task_user_id' => $taskUser->id,
                            ]);
                        } else {
                            $taskUser = TaskUser::create([
                                'task_id' => $task->id,
                                'user_id' => $userId
                            ]);

                            TaskDetail::create([
                                'type' => $taskDetail->type,
                                'location_id' => $location->id ?: null,
                                'task_user_id' => $taskUser->id,
                            ]);
                        }

                    }

                }
            }

            return response()->json([
                'error' => false,
                'msg' => "Task Created",
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'msg' => $e,
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Task $task
     * @return Builder|\Illuminate\Database\Eloquent\Model|Response|object
     */
    public function show($task)
    {
        $result = Task::findOrFail($task);
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        /* if ($role === 'engineer' || $role === 'vendor') {
             $result->load('taskUserByRole');
         } else {
             $result->load('taskUser');
         }*/

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $task = Task::findOrFail($request->id);
            $task->description = $request->description;
            $task->save();

            /*foreach ($request->task_user as $value) {
                $user = $value['user'];
                $user = User::findOrFail($user['id']);
                $userId = null;
                if ($user) {
                    $userId = $user->id;
                }
                $taskUser = TaskUser::query()->where('task_id', $task->id)->where('user_id', $userId)->first();

                if (!$taskUser) {
                    $taskUser = TaskUser::create([
                        'task_id' => $task->id,
                        'user_id' => $userId
                    ]);
                }

                $task_detail = $value['task_detail'];
                foreach ($task_detail as $taskDetail) {
                    $location = $taskDetail['location'];
                    $location = Location::findOrFail($location['id']);
                    $taskDetailQuery = TaskDetail::query()->where('task_user_id', $taskUser->id)->where('location_id', $location->id)->first();

                    if ($taskDetailQuery) {
                        $taskDetailQuery->type = $taskDetail['type'];
                        $taskDetailQuery->location_id = $location->id;
                        $taskDetailQuery->task_user_id = $taskUser->id;
                        $taskDetailQuery->save();
                    } else {
                        TaskDetail::create([
                            'type' => $taskDetail['type'],
                            'location_id' => $location->id ?: null,
                            'task_user_id' => $taskUser->id,
                        ]);
                    }
                }
            }*/

            return response()->json([
                'error' => false,
                'msg' => "Sukses",
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);

        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'msg' => "Gagal",
            ])->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Task::destroy($id);
    }
}
