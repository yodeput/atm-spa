<?php

namespace App\Http\Controllers\Visits;

use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Models\Master\Checklist;
use App\Models\Master\Location;
use App\Models\Master\Region;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Sparepart;
use App\Models\Visit\Visit;
use App\Models\Visit\VisitChecklist;
use App\Models\Visit\VisitPhoto;
use App\Traits\ActivityTraits;
use App\Traits\CustomResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Http\Response;

class SparepartController extends Controller
{
    use ActivityTraits;
    use CustomResponse;

    public function filter(Request $request)
    {
        $query = Sparepart::query();
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];
        $search = $request->search;
        switch ($role) {
            case 'engineer':
                $query->whereHas('visit', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                });
                break;
            case 'vendor':
                $query->whereHas('visit', function ($query1) use ($user) {
                    $query1->whereHas('user', function ($query2) use ($user) {
                        $query2->where('vendor_id', $user->vendor_id);
                    });
                });
                break;
        }

        if($search){
            $query->whereHas('visit', function ($query) use ($search) {
                $query->where('work_order', 'LIKE', '%' . $search . '%');
            });
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));
        return $data;
    }

    public function show($data)
    {
        $visit = Sparepart::findOrFail($data);
        return $visit;
    }

    public function update(Request $request)
    {
        $spare = Sparepart::findOrFail($request->id);
        $spare->part_number = $request->part_number_data;
        $spare->description = $request->description;
        $spare->category = $request->category;
        $spare->type = $request->type;
        $spare->qty = $request->qty;
        $spare->serial_in = $request->serial_in;
        $spare->serial_out = $request->serial_out;
        $spare->problem = $request->problem;
        $spare->save();
        return $spare;
    }



    public function getReport(Request $request)
    {
        $query = Sparepart::query();
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];
        $search = $request->search;
        switch ($role) {
            case 'engineer':
                $query->whereHas('visit', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                });
                break;
            case 'vendor':
                $query->whereHas('visit', function ($query1) use ($user) {
                    $query1->whereHas('user', function ($query2) use ($user) {
                        $query2->where('vendor_id', $user->vendor_id);
                    });
                });
                break;
        }

        if($search){
            $query->whereHas('visit', function ($query) use ($search) {
                $query->where('work_order', 'LIKE', '%' . $search . '%');
            });
        }

        $data = $query->whereBetween(DB::raw('DATE(created_at)'), array($request->start, $request->end))->get();
        return $data;
    }

    public function destroy($data)
    {
        return Sparepart::destroy($data);
    }


}
