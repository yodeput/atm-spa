<?php

namespace App\Http\Controllers\Visits;

use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Models\Visit\history\VisitChecklistHistory;
use App\Models\Visit\history\VisitHistory;
use App\Models\Visit\OpenWo;
use App\Models\Visit\VisitChecklist;

use App\Traits\ActivityTraits;
use App\Traits\CustomResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class VisitHistoryController extends Controller
{
    use ActivityTraits;
    use CustomResponse;

    public function filter(Request $request)
    {
        $query = VisitHistory::query();
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        switch ($role) {
            case 'manager':
            case 'fsm':
            case 'account-support-manager':
            case 'admin-pm':
            case 'administrator':
                if ($request->search) {
                    $search = $request->search;
                    $query->Where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%');
                        })
                        ->orWhereHas('location', function ($query) use ($search) {
                            $query->where('serial_number', 'LIKE', '%' . $search . '%')
                                ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                                ->orWhere('address', 'LIKE', '%' . $search . '%')
                                ->whereHas('customer', function ($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                                });
                        });
                }
                break;
            case 'vendor':
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%');
                        })->whereHas('location', function ($query) use ($user) {
                            $query->where('customer_id', $user->customer_id);
                        });
                }
                break;
            case 'engineer':
                $query->where('user_id', $user->id);
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('location', function ($query) use ($search) {
                            $query->where('serial_number', 'LIKE', '%' . $search . '%')
                                ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                                ->orWhere('address', 'LIKE', '%' . $search . '%')
                                ->whereHas('customer', function ($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                                });
                        })
                        ->where('user_id', $user->id);
                }
                break;
            case 'supervisor':
                $query->where('user_id', $user->id);
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('location', function ($query) use ($search) {
                            $query->where('serial_number', 'LIKE', '%' . $search . '%')
                                ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                                ->orWhere('address', 'LIKE', '%' . $search . '%')
                                ->whereHas('customer', function ($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                                });
                        })
                        ->where('user_id', $user->id);
                }
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%');
                        })->whereHas('location', function ($query) use ($user) {
                            $query->where('customer_id', $user->customer_id);
                        });
                }
                break;
        }

        if ($request->status) {
            if ($request->status === 'open') {
                $query->where('checkout_time', null);
            } else if ($request->status === 'close') {
                $query->whereNotNull('checkout_time');
            }
        }

        if ($request->month != null) {
            $req = date_create($request->month);
            $now = date_format($req, "m");
            $query->where(DB::raw('MONTH(checkin_time)'), $now)->get();
        }

        if ($request->customer != null && $role != 'customer') {
            $cust = $request->customer;
            $query->whereHas('location', function ($query) use ($cust) {
                $query->whereIn('customer_id', $cust);
            });
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));


//        $data->load('user');
//        $data->load('location');
        $data->load('checklists');
        return $data;
    }

    public function update(Request $request)
    {


        $visit1 = DB::table('t_visit_history')->where('work_order', $request->work_order)->first();
        $visit = VisitHistory::findOrFail($request->id);
        if ($visit1) {
            if ($visit1->id !== $request->id) {
                $this->validate($request, [
                    'work_order' => 'required|string|unique:t_visit_history',
                ]);
            }
        }
            $visit->work_order = $request->work_order;
            $visit->tipe_kunjungan = $request->tipe_kunjungan;
            $visit->detail_kunjungan = $request->detail_kunjungan;
            $visit->pengelola = $request->pengelola;
            $visit->pengelola_pic = $request->pengelola_pic;
            $visit->pengelola_phone = $request->pengelola_phone;
            $visit->user_id = $request->user['id'];
            $visit->location_id = $request->location ? $request->location['id'] : null;
            $visit->checkin_time = $request->checkin_time;
            $visit->checkout_time = $request->checkout_time;

            $checklistData = $request->checklists;
            foreach ($checklistData as $attribute) {
                $id = $attribute['id'];
                $value = $attribute['value'] ? $attribute['value'] : '';
                $isPhoto = $attribute['is_photo'];
                $visitCheck = VisitChecklistHistory::findOrFail($id);
                $visitCheck->value = $value;
                $visitCheck->is_photo = $isPhoto;
                $visitCheck->save();
            }



            $today = date("mY", strtotime($visit->checkin_time));
            $now = date('m-d-Y-His');


            if ($request->photo) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    }

                    $visit->save();
                }
            }

            if ($request->photo_sn) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo_sn;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo_sn = $photo_url;
                    }

                    $visit->save();
                }
            }

            if ($request->photo_ar && $request->photo_ar !== $visit->photo_ar) {
                $baseUrl = config('filesystems.disks.azure.url');
                $pathAr = 'AR/' . $today;
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo_ar;

                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_AR_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_ar = $photo_url;
                }
            }

            $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
            if ($openWo != null) {
                $openWo->close_time = now();
                $openWo->save();
                $this->logGeneral($openWo, 'CLOSE-' . $request->user['name'] . ' close Oustanding WO');
            }
            $visit->save();
            return $visit;




    }

    public function show($data)
    {
        $visit = VisitHistory::findOrFail($data);
//        $visit->load('user');
//        $visit->load('location');
        $visit->load('checklists');
        return $visit;
    }


    public function destroy($data)
    {
        return VisitHistory::destroy($data);
    }

}
