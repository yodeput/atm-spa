<?php

namespace App\Http\Controllers\Visits;

use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Models\Visit\Visit;
use App\Models\Visit\VisitPhoto;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VisitPhotosController extends Controller
{
    use ActivityTraits;

    public function uploadPhoto(Request $request)
    {
        $visit = Visit::findOrFail($request->visit_id);
        $visitPhoto = VisitPhoto::findOrFail($request->id);
        $baseUrl = config('filesystems.disks.azure.url');
        $path = 'visit/' . $visit->work_order;
        if ($request->value) {
            $base64_image = $request->value;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $desc1 = preg_replace('/\s+/', '_', $visitPhoto->deskripsi);
                $desc2 = preg_replace('/[^A-Za-z0-9\-]/', '_', $desc1);
                $fileName = $visit->work_order . '-' . $desc2 . '.' . $extension;

                $data = base64_decode($data);

                if ($baseUrl != null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visitPhoto->value = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visitPhoto->value = $photo_url;
                }

                $visitPhoto->save();
            }
        }

        $this->logGeneral($visitPhoto, 'UPDATE-Upload ' . $visitPhoto->deskripsi);

        return $visitPhoto;
    }


}
