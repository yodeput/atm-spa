<?php

namespace App\Http\Controllers\Visits;

use App\Imports\LocationsImport;
use App\Imports\OpenWoImport;
use App\Models\Master\Location;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Visit;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use OneSignal;

class OpenWoController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = OpenWo::query();
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($request->search) {
            $search = $request->search;
            $query->Where('work_order', 'LIKE', '%' . $request->search . '%')
                ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                ->orWhereHas('user', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orWhere('username', 'LIKE', '%' . $search . '%');
                });
        }
        /*switch ($role) {
            case 'manager':
            case 'administrator':
                if ($request->search) {
                    $search = $request->search;
                    $query->Where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%')
                            });
                }
                break;
            case 'engineer':
                $query->where('user_id', $user->id);
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%')
                            });
                }
                break;
            case 'supervisor':
                $regionId = $user->region_id;
                $query->whereHas('user', function ($query) use ($regionId) {
                    $query->whereHas('region', function ($query) use ($regionId) {
                        $query->where('region_id', $regionId);
                    });
                });
                if ($request->search) {
                    $search = $request->search;
                    $regionId = $user->region_id;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->whereHas('user', function ($query) use ($regionId) {
                            $query->whereHas('region', function ($query) use ($regionId) {
                                $query->where('region_id', $regionId);
                            });
                        });
                }
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                if ($request->search) {
                    $search = $request->search;
                    $query->where('work_order', 'LIKE', '%' . $request->search . '%')
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('email', 'LIKE', '%' . $search . '%')
                                ->orWhere('username', 'LIKE', '%' . $search . '%');
                        });
                }
                break;
        }*/


        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));


//        $data->load('user');
//        $data->load('location');
//        $data->load('checklists');
//        $data->load('photos');
        return $data;
    }

    public function export(Request $request)
    {
        $query = OpenWo::query();

        if ($request->status !== null) {
            $query->where('status', $request->status);
        }
        $all = $query->get();

        return $all;
    }

    public function getByUser()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];
        $query = OpenWo::query();
        switch ($role) {
            default:
                $data = $query->where('status', '!=', 'close')->pluck('work_order');
                break;
            case 'engineer':
                $data = $query->where('user_id', $user->id)->where('status', '!=', 'close')->pluck('work_order');
                break;
        }
        return $data;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'work_order' => 'required|string',
            'user' => 'required'
        ]);

        $openWo = OpenWo::create([
            'work_order' => $request->work_order,
            'user_id' => $request->user['id'],
            'status' => 'Open'

        ]);

        $countOpen = OpenWo::query()->where('user_id', $request->user['id'])
            ->where('status', 'Open')->get()->count();

        if ($countOpen > 0) {
            $dataNotif = array(
                "ref" => 't_outstanding_wo'
            );
            $uid = $request->user['id'];
            OneSignal::sendNotificationToExternalUser(
                $countOpen . " Oustanding WO\nSegera ubah WO pada data Visit",
                array((string)$uid),
                $url = null,
                $data = $dataNotif,
                $buttons = null,
                $schedule = null
            );
        }

        $this->logCreatedActivity($openWo, Auth::user()->name . ' create a OUstanding WO', $request->toArray());


        return $openWo;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'work_order' => 'required|string',
            'user' => 'required',
            'status' => 'required',
        ]);

        $openWo = OpenWo::findOrFail($request->id);
        $beforeUpdateValues = $openWo->toArray();
        if ($openWo->user_id != $request->user['id']) {
            $openWo->user_id = $request->user['id'];

        }
        $openWo->close_time = $request->close_time;
        if ($request->close_time === null) {
            if ($request->status === 'Close') {
                $openWo->close_time = now();
            }
        }

        $openWo->status = $request->status;
        $openWo->work_order = $request->work_order;
        $openWo->save();


        $countOpen = OpenWo::query()->where('user_id', $openWo->user_id)
            ->where('status', 'Open')->count();

        if ($countOpen > 0) {
            $dataNotif = array(
                "ref" => 't_outstanding_wo'
            );
            $uid = $request->user['id'];
            OneSignal::sendNotificationToExternalUser(
                $countOpen . " Oustanding WO\nSegera ubah WO pada data Visit",
                array((string)$uid),
                $url = null,
                $data = $dataNotif,
                $buttons = null,
                $schedule = null
            );
        }

        $afterUpdateValues = $openWo->getChanges();
        $this->logUpdatedActivity($openWo, $beforeUpdateValues, $afterUpdateValues);
        return $openWo;
    }

    public function show($id)
    {
        $location = OpenWo::findOrFail($id);
        return $location;
    }

    public function destroy($location)
    {
        $data = OpenWo::find($location);
        $this->logDeletedActivity($data, 'Outstanding WO ' . $data->name);

        return OpenWo::destroy($location);
    }

    public function deleteClose()
    {
        try {
            $outandingWoClose = OpenWo::query()->where('status', 'Close')->get();
            foreach ($outandingWoClose as $data) {
                $data->delete();
            }
            /*$ids_to_delete = array_map(function($item){ return $item['id']; }, $outandingWoClose);
            dd($ids_to_delete);
            $data = OpenWo::query()->whereIn('status', $ids_to_delete)->delete();
            $this->logDeletedActivity([], 'Delete Outstanding status Close');*/

            return response()->json([
                'error' => false,
                'msg' => "Delete WO sukses",
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'exc' => $e->getMessage(),
                'msg' => "Delete WO gagal",
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function count()
    {
        return OpenWo::count();
    }


    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();


        $file->move('storage/file_import/', $nama_file);

        try {
            Excel::import(new OpenWoImport(), public_path('/storage/file_import/' . $nama_file));
            /* $outandingWoClose = OpenWo::query()->where('status','Close')->whereNotNull('close_time');
             foreach ($outandingWoClose as $row) {
                 OpenWo::destroy($row);
             }*/

            $query = DB::select(DB::raw("SELECT
	user_id,
	COUNT(t_open_wo.id) as count
FROM
	t_open_wo
WHERE t_open_wo.status = 'Open'
GROUP BY
	t_open_wo.user_id"));

            foreach ($query as $q) {
                $uid = $q->user_id;
                $count = $q->count;
                $dataNotif = array(
                    "ref" => 't_outstanding_wo'
                );

                OneSignal::sendNotificationToExternalUser(
                    $count . " Oustanding WO\nSegera ubah WO pada data Visit",
                    array((string)$uid),
                    $url = null,
                    $data = $dataNotif,
                    $buttons = null,
                    $schedule = null
                );
            }

            return response()->json([
                'error' => false,
                'msg' => "Import Sukses",
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'msg' => "Delete WO gagal",
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function countByEnginer(Request $request)
    {
        DB::enableQueryLog();
        $now = date('m');
        if ($request->month != null) {
            $now = $request->month;
        }
        $user = User::find(Auth::id());
        $count = OpenWo::select([DB::raw('DATE(created_at) as date'),
            DB::raw('count(id) as "open"')])
            ->where(DB::raw('user_id'), $user->id)
            //->where(DB::raw('MONTH(created_at)'),$now)
            ->whereNull('close_time')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->get();
        $count->load('user');
        //dd(DB::getQueryLog());
        return $count;
    }

    public function countPerEnginer(Request $request)
    {
        DB::enableQueryLog();
        $now = date('m');
        if ($request->month != null) {
            $now = $request->month;
        }
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $count = OpenWo::select([DB::raw('user_id as "user_id"'),
            DB::raw('count(id) as "open"')])
            //->where(DB::raw('MONTH(created_at)'),$now)
            ->whereNull('close_time')
            ->groupBy(DB::raw('user_id'))
            ->get();
        $count->load('user');
        return $count;
    }

    public function countPerRegion(Request $request)
    {
        DB::enableQueryLog();
        $now = date('m');
        if ($request->month != null) {
            $now = $request->month;
        }
        $user = User::find(Auth::id());
        $regionId = $user->region_id;
        $query = OpenWo::query();
        $query = $query->select([DB::raw('user_id as "user_id"'),
            DB::raw('count(id) as "open"')])
            //->where(DB::raw('MONTH(created_at)'),$now)
            ->groupBy(DB::raw('user_id'))
            ->where(DB::raw('user_id'), '!=', $user->id)
            ->whereNull('close_time')
            ->whereHas('user', function ($query) use ($regionId) {
                $query->whereHas('region', function ($query) use ($regionId) {
                    $query->where('region_id', $regionId);
                });
            })
            ->get();
        $query->load('user');
        //dd(DB::getQueryLog());
        return $query;


        /*
              */
    }

}
