<?php

namespace App\Http\Controllers\Visits;

use App\Exports\NotifyVisitExport;
use App\Exports\RegionsExport;
use App\Exports\VisitExport;
use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Models\Master\Checklist;
use App\Models\Master\Location;
use App\Models\Master\Region;
use App\Models\Task\TaskDetail;
use App\Models\TT\TroubleTicket;
use App\Models\Visit\history\VisitChecklistHistory;
use App\Models\Visit\history\VisitHistory;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Sparepart;
use App\Models\Visit\Visit;
use App\Models\Visit\VisitChecklist;
use App\Models\Visit\VisitPhoto;
use App\Traits\ActivityTraits;
use App\Traits\CustomResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use DateTime;
use DateInterval;
use DatePeriod;
use App\Transformers\VisitCollection;

class VisitController extends Controller
{
    use ActivityTraits;
    use CustomResponse;

    public function __construct()
    {
        set_time_limit(0);
    }

    public function filter(Request $request)
    {

        $query = Visit::query();
        $user = User::find(Auth::id());
        $roles = auth()->user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($request->search) {
            $search = $request->search;
            $query->Where('work_order', 'LIKE', '%' . $search . '%')
                ->orWhere('tipe_kunjungan', 'LIKE', '%' . $search . '%')
                ->orWhereHas('user', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orWhere('username', 'LIKE', '%' . $search . '%');
                })
                ->orWhereHas('location', function ($query) use ($search) {
                    $query->where('serial_number', 'LIKE', '%' . $search . '%')
                        ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('address', 'LIKE', '%' . $search . '%')
                        ->whereHas('customer', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('code', 'LIKE', '%' . $search . '%');
                        });
                });
        }

        switch ($role) {
            case 'vendor':
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
                break;
            case 'engineer':
            case 'supervisor':
                $query->where('user_id', $user->id);
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                break;
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $data;
    }

    public function filter2(Request $request)
    {

        $query = Visit::query();
        $user = User::find(Auth::id());
        $roles = auth()->user()->roles()->get()->pluck('name');
        $role = $roles[0];

        $advance = $request->advance;
        if (!$this->IsNullOrEmptyString($request->search)) {
            $query->Where('work_order', 'LIKE', '%' . $request->search . '%')
                ->orWhere('tipe_kunjungan', 'LIKE', '%' . $request->search . '%');
        }

        if (!$this->IsNullOrEmptyString($advance['customer_id'])) {
            $customer = $advance['customer_id'];
            $query->whereHas('location', function ($query) use ($customer) {
                $query->where('customer_id', $customer);
            });
        }

        $status = $advance['status'];
        if (count($status) < 2) {
            if (in_array('open', $status, true)) {
                $query->whereNull('photo_ar')->whereNull('checkout_time');
            } else {
                $query->whereNotNull('photo_ar')->whereNotNull('checkout_time');
            }
        }

        if($advance["start_end"] != null){
            $query->whereBetween('checkin_time',$advance["start_end"])
                ->orwhereBetween('checkout_time',$advance["start_end"]);
        }

        if ($request->search) {
            $search = $request->search;
            $query->Where('work_order', 'LIKE', '%' . $search . '%')
                ->orWhere('tipe_kunjungan', 'LIKE', '%' . $search . '%')
                ->orWhereHas('user', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orWhere('username', 'LIKE', '%' . $search . '%');
                })
                ->orWhereHas('location', function ($query) use ($search) {
                    $query->where('serial_number', 'LIKE', '%' . $search . '%')
                        ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('address', 'LIKE', '%' . $search . '%')
                        ->whereHas('customer', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('code', 'LIKE', '%' . $search . '%');
                        });
                });
        }

        switch ($role) {
            case 'vendor':
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
                break;
            case 'engineer':
            case 'supervisor':
                $query->where('user_id', $user->id);
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                break;
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $data;
    }

    public function show($data)
    {

        $visit = Visit::with(['checklists', 'spareparts'])->findOrFail($data);
        if ($visit === null) {
            $visit = Visit::with(['checklists', 'spareparts'])->where('work_order', $data)->first();
        }

        if (count($visit->checklists) === 0) {
            $checklistQuery = Checklist::query()->where('is_active', true);
            $checklistsData = $checklistQuery->where('is_general', true)
                ->orWhere('customer_id', $visit->location['customer_id'])
                ->get();

            if ($checklistsData) {
                foreach ($checklistsData as $attribute) {
                    $visit_id = $visit->id;
                    $deskripsi = $attribute['deskripsi'];
                    $tipe = $attribute['tipe'];
                    $option = $attribute['value'];
                    $is_photo = $attribute['is_photo'];
                    $value = '';
                    $visitPhoto = VisitChecklist::create([
                        'visit_id' => $visit_id,
                        'deskripsi' => $deskripsi,
                        'tipe' => $tipe,
                        'option' => $option,
                        'value' => $value,
                        'is_photo' => $is_photo
                    ]);
                }
            }
        }
        return $visit;
    }

    public function getByWo(Request $request)
    {

        $visit = Visit::query()->where('work_order', $request->wo)->first();
        return $visit;
    }

    public function store(Request $request)
    {

        try {
            /*$this->validate($request, [
                'work_order' => 'required|string|unique:t_visit',
                'user' => 'required',
                'atm' => 'required',
                //'checkin_time' => 'required',
                'tipe_kunjungan' => 'required|string',
                'detail_kunjungan' => 'required|string',
                'pengelola' => 'required|string',
                'pengelola_pic' => 'required|string',
                'pengelola_phone' => 'required|string',
                'photo' => 'required',
                'photo_sn' => 'required',
            ]);*/

            $date = new DateTime();
            $woNUll = $date->format('Ymdis');

            $visit = Visit::create([
                'work_order' => $request->work_order ? $request->work_order : $woNUll,
                'user_id' => $request->user ? $request->user['id'] : null,
                'location_id' => $request->atm ? $request->atm['id'] : null,
                'tipe_kunjungan' => $request->tipe_kunjungan,
                'detail_kunjungan' => $request->detail_kunjungan,
                'pengelola' => $request->pengelola,
                'pengelola_pic' => $request->pengelola_pic,
                'checkin_on_site' => !$request->checkin_on_site,
                'pengelola_phone' => $request->pengelola_phone,
                'checkin_time' => $request->checkin_time ?: now(),
                'longitude' => $request->longitude ?: null,
                'latitude' => $request->latitude ?: null,
            ]);

            $userId = $visit->user_id;

            $taskDetail = TaskDetail::query()->where('location_id', $visit->location_id)
                ->where('type', $visit->tipe_kunjungan)
                ->whereNull('visit_id')
                ->whereNull('close_date')
                ->orderBy('created_at', 'desc')
                ->first();

            if ($taskDetail) {
                $taskDetail->load('taskUser');

                if ($taskDetail->taskUser->user_id === $userId) {
                    $taskDetail->start_date = $visit->checkin_time;
                    $taskDetail->visit_id = $visit->id;
                } else {
                    $taskDetail->start_date = $visit->checkin_time;
                    $taskDetail->visit_id = $visit->id;
                    $taskDetail->user_action_id = $userId;
                }

                $taskDetail->save();
                $this->logGeneral($taskDetail, 'START-' . $request->user['name'] . ' start a Task');
            }

            $checklistQuery = Checklist::query()->where('is_active', true);
            $checklistsData = $checklistQuery->where('is_general', true)
                ->orWhere('customer_id', $request->atm["customer_id"])
                ->get();

            if ($checklistsData) {
                foreach ($checklistsData as $attribute) {
                    $visit_id = $visit->id;
                    $deskripsi = $attribute['deskripsi'];
                    $tipe = $attribute['tipe'];
                    $option = $attribute['value'];
                    $is_photo = $attribute['is_photo'];
                    $value = '';
                    $visitPhoto = VisitChecklist::create([
                        'visit_id' => $visit_id,
                        'deskripsi' => $deskripsi,
                        'tipe' => $tipe,
                        'option' => $option,
                        'value' => $value,
                        'is_photo' => $is_photo
                    ]);
                }
            }

            $today = date("mY", strtotime($visit->checkin_time));
            $now = date('m-d-Y-His');

            if ($request->photo) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    }

                    $visit->save();
                }
            }

            if ($request->photo_sn) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo_sn;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo_sn = $photo_url;
                    }

                    $visit->save();
                }
            }

            $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
            if ($openWo != null) {
                if ($visit->photo_ar === null) {
                    $openWo->status = 'On Progress';
                    $openWo->save();
                }
            }

            $this->logCreatedActivityNoModel(auth()->user()->name . ' create a Visit');

            /* return response()->json([
                 'error' => '$error',
                 'msg' => '$msg',
             ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);*/

            return $this->HttpOkWithData(false, 'Visit Created', $visit);

        } catch (\Exception $e) {

            return $this->HttpBadRequest(true, $e->getMessage());

        }
    }

    public function storeMobile(Request $request)
    {/*
        $this->validate($request, [
            'work_order' => 'required|string|unique:t_visit',
            'user' => 'required',
            'atm' => 'required',
            //'checkin_time' => 'required',
            'tipe_kunjungan' => 'required|string',
            'detail_kunjungan' => 'required|string',
            'pengelola' => 'required|string',
            'pengelola_pic' => 'required|string',
            'pengelola_phone' => 'required|string',
            'photo' => 'required',
            'photo_sn' => 'required',
        ]);*/

        $dataPost = json_decode(file_get_contents($request->data), true);

        $openWo = OpenWo::query()->where('work_order', $dataPost['work_order'])->first();
        if ($openWo != null) {
            $openWo->close_time = now();
            $openWo->save();
            $this->logGeneral($openWo, 'CLOSE-' . auth()->user()->name . ' close Oustanding WO');
        }

        $visit = Visit::create([
            'work_order' => $dataPost['work_order'],
            'user_id' => auth()->user()->id,
            'location_id' => $dataPost['atm']['id'],
            'tipe_kunjungan' => $dataPost['tipe_kunjungan'],
            'detail_kunjungan' => $dataPost['detail_kunjungan'],
            'pengelola' => $dataPost['pengelola'],
            'pengelola_pic' => $dataPost['pengelola_pic'],
            'pengelola_phone' => $dataPost['pengelola_phone'],
            'checkin_time' => $dataPost['checkin_time'] ? $dataPost['checkin_time'] : now(),
            'longitude' => $dataPost['longitude'],
            'latitude' => $dataPost['latitude'],
        ]);

        $userId = $visit->user_id;
        $taskDetail = TaskDetail::query()->where('location_id', $visit->location_id)
            ->where('type', $visit->tipe_kunjungan)
            ->whereNull('visit_id')
            ->whereNull('close_date')
            ->orderBy('created_at', 'desc')
            ->first();


        if ($taskDetail) {
            $taskDetail->load('taskUser');
            //dd($taskDetail->taskUser);
            if ($taskDetail->taskUser->user_id === $userId) {
                $taskDetail->start_date = $visit->checkin_time;
                $taskDetail->visit_id = $visit->id;
            } else {
                $taskDetail->start_date = $visit->checkin_time;
                $taskDetail->visit_id = $visit->id;
                $taskDetail->user_action_id = $userId;
            }
            $taskDetail->save();
            $this->logGeneral($taskDetail, 'START-' . auth()->user()->name . ' start a Task');
        }

        $checklistQuery = Checklist::query()->where('is_active', true);

        $checklistsData = $checklistQuery->where('is_general', true)->orWhere('customer_id', $dataPost['atm']["customer_id"])
            ->get();

        foreach ($checklistsData as $attribute) {
            $visit_id = $visit->id;
            $deskripsi = $attribute['deskripsi'];
            $tipe = $attribute['tipe'];
            $option = $attribute['value'];
            $is_photo = $attribute['is_photo'];
            $value = '';
            $visitPhoto = VisitChecklist::create([
                'visit_id' => $visit_id,
                'deskripsi' => $deskripsi,
                'tipe' => $tipe,
                'option' => $option,
                'value' => $value,
                'is_photo' => $is_photo
            ]);
        }

        $today = date("mY", strtotime($visit->checkin_time));
        $now = date('m-d-Y-His');
        $path = 'visit/' . $today . '/' . $visit->work_order;

        if ($request->photo) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                }

                $visit->save();
            }
        }

        if ($request->photo_sn) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo_sn;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_sn = $photo_url;
                }

                $visit->save();
            }
        }
        $openWo = OpenWo::query()->where('work_order', $visit->work_order)->first();
        if ($openWo != null) {
            if ($visit->photo_ar === null) {
                $openWo->status = 'On Progress';
                $openWo->save();
            }
        }

        $this->logCreatedActivityNoModel(auth()->user()->name . ' create a Visit');

        return $this->HttpOkWithData(false, 'Visit Created', $visit);
    }

    public function uploadPhotoAr(Request $request)
    {
        $visit = Visit::findOrFail($request->id);
        $baseUrl = config('filesystems.disks.azure.url');
        $path = 'AR';
        $index = File::get('images/index.php');
        $upload = new Upload();
        $upload->upload($index, $path . '/index.php');
        if ($request->photo_ar) {
            $base64_image = $request->photo_ar;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $desc1 = preg_replace('/\s+/', '_ACTIVITY_REPORT');
                $desc2 = preg_replace('/[^A-Za-z0-9\-]/', '_', $desc1);
                $fileName = $visit->work_order . '-' . $desc2 . '.' . $extension;

                $data = base64_decode($data);

                if ($baseUrl != null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo_ar = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_ar->photo = $photo_url;
                }

                $visit->save();
            }
        }

        $this->logGeneral($visit, 'UPDATE-Upload Activity Report');

        return $visit;
    }

    public function update(Request $request)
    {


        $visit1 = DB::table('t_visit')->where('work_order', $request->work_order)->first();
        $visit = Visit::findOrFail($request->id);
        if ($visit1) {
            if ($visit1->id !== $request->id) {
                $this->validate($request, [
                    'work_order' => 'required|string|unique:t_visit',
                ]);
            }
        }
        if ($visit->location_id == $request->location['id']) {
            $visit->work_order = $request->work_order;
            $visit->tipe_kunjungan = $request->tipe_kunjungan;
            $visit->detail_kunjungan = $request->detail_kunjungan;
            $visit->pengelola = $request->pengelola;
            $visit->pengelola_pic = $request->pengelola_pic;
            $visit->pengelola_phone = $request->pengelola_phone;
            $visit->user_id = $request->user['id'];
            $visit->location_id = $request->location ? $request->location['id'] : null;
            $visit->checkin_time = $request->checkin_time;
            $visit->checkout_time = $request->checkout_time;

            $checklistData = $request->checklists;
            foreach ($checklistData as $attribute) {
                $id = $attribute['id'];
                $value = $attribute['value'] ? $attribute['value'] : '';
                $isPhoto = $attribute['is_photo'];
                $visitCheck = VisitChecklist::findOrFail($id);
                $visitCheck->value = $value;
                $visitCheck->is_photo = $isPhoto;
                $visitCheck->save();
            }


            $today = date("mY", strtotime($visit->checkin_time));
            $now = date('m-d-Y-His');

            $sparepartsData = $request->spareparts;
            foreach ($sparepartsData as $sparepart) {
                $id = $sparepart['id'];
                if ($sparepart && $id) {
                    $createPart = Sparepart::query()->where('id', $sparepart['id'])->first();
                    $createPart->part_number = $sparepart['part_number_data'];
                    $createPart->description = $sparepart['description'];
                    $createPart->type = $sparepart['type'];
                    $createPart->category = $sparepart['category'] ?? "-";
                    $createPart->qty = $sparepart['qty'];
                    $createPart->serial_in = $sparepart['serial_in'];
                    $createPart->serial_out = $sparepart['serial_out'];
                    $createPart->problem = $sparepart['problem'];

                    $createPart->save();
                } else {
                    $createPart = Sparepart::create([
                        'visit_id' => $visit->id,
                        'part_number' => $sparepart['part_number_data'],
                        'description' => $sparepart['description'],
                        'type' => $sparepart['type'],
                        'category' => $sparepart['category'] ?? "-",
                        'qty' => $sparepart['qty'],
                        'serial_in' => $sparepart['serial_in'],
                        'serial_out' => $sparepart['serial_out'],
                        'problem' => $sparepart['problem'],
                    ]);
                }


                if ($sparepart['photo']) {
                    $baseUrl = config('filesystems.disks.azure.url');
                    $path = 'visit/' . $today . '/' . $visit->work_order . '/part';
                    $index = File::get('images/index.php');
                    $upload = new Upload();
                    $upload->upload($index, $path . '/index.php');
                    $base64_image = $sparepart['photo'];
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                        $data = substr($base64_image, strpos($base64_image, ',') + 1);
                        $extension = explode('/', mime_content_type($base64_image))[1];
                        $fileName = $visit->work_order . '_part_' . $createPart->id . '_' . $now . '.' . $extension;
                        $data = base64_decode($data);
                        if ($baseUrl !== null) {
                            $upload = new UploadAzure();
                            $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                            $photo_url = $baseUrl . $path . '/' . $fileName;
                            $createPart->photo = $photo_url;
                        } else {
                            $upload = new Upload();
                            $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                            $photo_url = '/storage/' . $path . '/' . $fileName;
                            $createPart->photo = $photo_url;
                        }
                        $createPart->save();
                    }
                }
            }


            if ($request->photo) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    }

                    $visit->save();
                }
            }

            if ($request->photo_sn) {
                $baseUrl = config('filesystems.disks.azure.url');
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo_sn;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $extension = explode('/', mime_content_type($base64_image))[1];
                    $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                    $data = base64_decode($data);
                    if ($baseUrl !== null) {
                        $upload = new UploadAzure();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = $baseUrl . $path . '/' . $fileName;
                        $visit->photo = $photo_url;
                    } else {
                        $upload = new Upload();
                        $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                        $photo_url = '/storage/' . $path . '/' . $fileName;
                        $visit->photo_sn = $photo_url;
                    }

                    $visit->save();
                }
            }

            if ($request->photo_ar && $request->photo_ar !== $visit->photo_ar) {
                $baseUrl = config('filesystems.disks.azure.url');
                $pathAr = 'AR/' . $today;
                $path = 'visit/' . $today . '/' . $visit->work_order;
                $index = File::get('images/index.php');
                $upload = new Upload();
                $upload->upload($index, $path . '/index.php');
                $base64_image = $request->photo_ar;

                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_AR_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_ar = $photo_url;
                }
            }

            $visit->save();

            $taskDetail = TaskDetail::query()->where('visit_id', $request->id)->first();
            if ($taskDetail) {
                $taskDetail->close_date = $visit->checkout_time;
                $taskDetail->save();
                $this->logGeneral($taskDetail, 'CLOSE-' . $request->user['name'] . ' close Task');
            } else {
                $taskDetail = TaskDetail::query()->where('location_id', $visit->location_id)
                    ->where('type', $visit->tipe_kunjungan)
                    /*->whereNull('visit_id')->whereNull('close_date')*/
                    ->orderBy('created_at', 'desc')->first();
                if ($taskDetail) {
                    $taskDetail->start_date = $visit->checkin_time;
                    $taskDetail->visit_id = $visit->id;
                    if ($visit->checkout_time) {
                        $taskDetail->close_date = $visit->checkout_time;
                        $this->logGeneral($taskDetail, 'CLOSE-' . $request->user['name'] . ' close Task');
                    } else {
                        $this->logGeneral($taskDetail, 'START-' . $request->user['name'] . ' start a Task');
                    }

                    $taskDetail->save();
                }
            }


            $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
            if ($openWo != null) {
                if ($visit->photo_ar === null) {
                    $openWo->status = 'On Progress';
                    //$this->logGeneral($openWo, 'PROGRESS-' . $request->user['name'] . ' Oustanding WO');
                } else {
                    $openWo->close_time = now();
                    $openWo->status = 'Close';

                    $this->logGeneral($openWo, 'CLOSE-' . $request->user['name'] . ' close Oustanding WO');
                }
                $openWo->save();
            }
            return $visit;

        } else {

            $visit = Visit::findOrFail($request->id);

            $visitHistory = VisitHistory::query()->where('work_order', $request->work_order)->first();
            if ($visitHistory) {
                $visitHistory->work_order = $request->work_order;
                $visitHistory->tipe_kunjungan = $request->tipe_kunjungan;
                $visitHistory->detail_kunjungan = $request->detail_kunjungan;
                $visitHistory->pengelola = $request->pengelola;
                $visitHistory->pengelola_pic = $request->pengelola_pic;
                $visitHistory->pengelola_phone = $request->pengelola_phone;
                $visitHistory->user_id = $request->user['id'];
                $visitHistory->location_id = $visit->location_id;
                $visitHistory->checkin_time = $request->checkin_time;
                $visitHistory->checkout_time = $request->checkin_time;
                $visitHistory->photo = $request->photo;
                $visitHistory->photo_sn = $request->photo_sn;
                $visitHistory->photo_ar = $request->photo_ar;

                $visitHistory->save();
            } else {

                $visit = Visit::findOrFail($request->id);
                $visitHistory = VisitHistory::create([
                    'work_order' => $visit->work_order,
                    'user_id' => $visit->user['id'],
                    'location_id' => $visit->location_id,
                    'tipe_kunjungan' => $visit->tipe_kunjungan,
                    'detail_kunjungan' => $visit->detail_kunjungan,
                    'pengelola' => $visit->pengelola,
                    'pengelola_pic' => $visit->pengelola_pic,
                    'pengelola_phone' => $visit->pengelola_phone,
                    'checkout_time' => $visit->checkout_time,
                    'checkin_time' => $visit->checkin_time,
                    'photo' => $visit->photo,
                    'photo_sn' => $visit->photo_sn,
                    'photo_ar' => $visit->photo_ar,
                    'longitude' => $visit->longitude,
                    'latitude' => $visit->latitude,
                ]);
            }


            $checklistHistory = $request->checklists;
            foreach ($checklistHistory as $attribute) {
                $id = $attribute['id'];
                $deskripsi = $attribute['deskripsi'];
                $tipe = $attribute['tipe'];
                $option = $attribute['option'];
                $photo = $attribute['photo'];
                $is_photo = $attribute['is_photo'];
                $value = $attribute['value'];
                $vch = VisitChecklistHistory::query()->where('visit_history_id', $visitHistory->id)->where('deskripsi', $deskripsi)->first();
                if ($vch) {
                    $vch->value = $value;
                    $vch->photo = $photo;
                    $vch->is_photo = $is_photo;
                    $vch->save();
                } else {
                    VisitChecklistHistory::create([
                        'visit_history_id' => $visitHistory->id,
                        'deskripsi' => $deskripsi,
                        'tipe' => $tipe,
                        'option' => $option,
                        'value' => $value ? $value : '',
                        'is_photo' => $is_photo
                    ]);
                }
                VisitChecklist::destroy($id);
            }

            $visit->work_order = $request->work_order;
            $visit->tipe_kunjungan = $request->tipe_kunjungan;
            $visit->detail_kunjungan = $request->detail_kunjungan;
            $visit->pengelola = $request->pengelola;
            $visit->pengelola_pic = $request->pengelola_pic;
            $visit->pengelola_phone = $request->pengelola_phone;
            $visit->user_id = $request->user['id'];
            $visit->location_id = $request->location ? $request->location['id'] : null;
            $visit->checkin_time = $request->checkin_time;
            $visit->photo_ar = NULL;
            $visit->checkout_time = NULL;
            $visit->history_id = $visitHistory->id;

            $checklistQuery = Checklist::query()->where('is_active', true);

            $checklistsData = $checklistQuery->where('is_general', true)->orWhere('customer_id', $request->atm["customer_id"])
                ->get();

            foreach ($checklistsData as $attribute) {

                $visit_id = $visit->id;
                $deskripsi = $attribute['deskripsi'];
                $tipe = $attribute['tipe'];
                $option = $attribute['value'];
                $is_photo = $attribute['is_photo'];
                $value = '';
                $exist = VisitChecklist::query()->where('deskripsi', $deskripsi)->where('visit_id', $visit_id)->first();
                if (!$exist) {
                    $visitPhoto = VisitChecklist::create([
                        'visit_id' => $visit_id,
                        'deskripsi' => $deskripsi,
                        'tipe' => $tipe,
                        'option' => $option,
                        'value' => $value,
                        'is_photo' => $is_photo
                    ]);
                }
            }

            $visit->save();

            $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
            if ($openWo != null) {
                if ($visit->photo_ar === null) {
                    $openWo->status = 'On Progress';
                    //$this->logGeneral($openWo, 'PROGRESS-' . $request->user['name'] . ' Oustanding WO');
                } else {
                    $openWo->close_time = now();
                    $openWo->status = 'Close';
                    $this->logGeneral($openWo, 'CLOSE-' . $request->user['name'] . ' close Oustanding WO');
                }
                $openWo->save();
            }
            return $visit;
        }


    }

    public function checkout(Request $request)
    {
        $this->validate($request, [
            'photo_ar' => 'required',
        ]);

        $visit1 = Visit::query()->where('work_order', $request->work_order)->first();
        $visit = Visit::findOrFail($request->id);
        if ($visit1) {
            if ($visit1->id !== $request->id) {
                $this->validate($request, [
                    'work_order' => 'required|string|unique:t_visit',
                ]);
            }
        }

        $visit->work_order = $request->work_order;
        $visit->tipe_kunjungan = $request->tipe_kunjungan;
        $visit->detail_kunjungan = $request->detail_kunjungan;
        $visit->pengelola = $request->pengelola;
        $visit->pengelola_pic = $request->pengelola_pic;
        $visit->pengelola_phone = $request->pengelola_phone;
        $visit->location_id = $request->location ? $request->location['id'] : null;
        $visit->checkout_time = $request->checkout_time ? $request->checkout_time : now();
        $visit->checkin_time = $request->checkin_time;

        $checklistData = $request->checklists;
        foreach ($checklistData as $attribute) {
            $id = $attribute['id'];
            $value = $attribute['value'] ? $attribute['value'] : '';
            $isPhoto = $attribute['is_photo'];
            $visitCheck = VisitChecklist::findOrFail($id);
            $visitCheck->value = $value;
            $visitCheck->is_photo = $isPhoto;
            $visitCheck->save();
        }

        $today = date("mY", strtotime($visit->checkin_time));
        $now = date('m-d-Y-His');

        $sparepartsData = $request->spareparts;
        foreach ($sparepartsData as $sparepart) {

            $findPart = Sparepart::query()->where('visit_id', $visit->id)->where('description', $sparepart['description'])->first();
            if ($findPart == null) {
                $createPart = Sparepart::create([
                    'visit_id' => $visit->id,
                    'part_number' => $sparepart['part_number_data'],
                    'description' => $sparepart['description'],
                    'type' => $sparepart['type'],
                    'category' => $sparepart['category'] ?? "-",
                    'qty' => $sparepart['qty'],
                    'serial_in' => $sparepart['serial_in'],
                    'serial_out' => $sparepart['serial_out'],
                    'problem' => $sparepart['problem'],
                ]);

                if ($sparepart['photo']) {
                    $baseUrl = config('filesystems.disks.azure.url');
                    $path = 'visit/' . $today . '/' . $visit->work_order . '/part';
                    $index = File::get('images/index.php');
                    $upload = new Upload();
                    $upload->upload($index, $path . '/index.php');
                    $base64_image = $sparepart['photo'];
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                        $data = substr($base64_image, strpos($base64_image, ',') + 1);
                        $extension = explode('/', mime_content_type($base64_image))[1];
                        $fileName = $visit->work_order . '_part_' . $createPart->id . '_' . $now . '.' . $extension;
                        $data = base64_decode($data);
                        if ($baseUrl !== null) {
                            $upload = new UploadAzure();
                            $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                            $photo_url = $baseUrl . $path . '/' . $fileName;
                            $createPart->photo = $photo_url;
                        } else {
                            $upload = new Upload();
                            $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                            $photo_url = '/storage/' . $path . '/' . $fileName;
                            $createPart->photo = $photo_url;
                        }
                        $createPart->save();
                    }
                }

            }
        }

        if ($request->photo && $request->photo !== $visit->photo) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                }
            }
        }

        if ($request->photo_sn && $request->photo_sn !== $visit->photo_sn) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo_sn;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_sn = $photo_url;
                }
            }
        }

        if ($request->photo_ar && $request->photo_ar !== $visit->photo_ar) {
            $baseUrl = config('filesystems.disks.azure.url');
            $pathAr = 'AR/' . $today;
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo_ar;

            $data = substr($base64_image, strpos($base64_image, ',') + 1);
            $extension = explode('/', mime_content_type($base64_image))[1];
            $now = date('m-d-Y-His');
            $fileName = $visit->work_order . '_AR_' . $now . '.' . $extension;
            $data = base64_decode($data);
            if ($baseUrl !== null) {
                $upload = new UploadAzure();
                $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                $photo_url = $baseUrl . $path . '/' . $fileName;
                $visit->photo = $photo_url;
            } else {
                $upload = new Upload();
                $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                $photoAr = $upload->upload($data, $pathAr . '/' . $fileName)->getData();
                $photo_url = '/storage/' . $path . '/' . $fileName;
                $visit->photo_ar = $photo_url;
            }
        }


        $visit->save();
        $taskDetail = TaskDetail::query()->where('visit_id', $request->id)->first();
        if ($taskDetail) {
            $taskDetail->close_date = $visit->checkout_time;
            $taskDetail->save();
            $this->logGeneral($taskDetail, 'CLOSE-' . $request->user['name'] . ' close Task');
        } else {
            $taskDetail = TaskDetail::query()->where('location_id', $visit->location_id)
                ->where('type', $visit->tipe_kunjungan)
                /*->whereNull('visit_id')->whereNull('close_date')*/
                ->orderBy('created_at', 'desc')->first();
            if ($taskDetail) {
                $taskDetail->start_date = $visit->checkin_time;
                $taskDetail->visit_id = $visit->id;
                if ($visit->checkout_time) {
                    $taskDetail->close_date = $visit->checkout_time;
                    $this->logGeneral($taskDetail, 'CLOSE-' . $request->user['name'] . ' close Task');

                } else {
                    $this->logGeneral($taskDetail, 'START-' . $request->user['name'] . ' start a Task');
                }

                $taskDetail->save();
            }
        }

        $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
        if ($openWo != null) {
            if ($visit->photo_ar === null) {
                $openWo->status = 'On Progress';
                //$this->logGeneral($openWo, 'PROGRESS-' . $request->user['name'] . ' Oustanding WO');
            } else {
                $openWo->close_time = now();
                $openWo->status = 'Close';
                $this->logGeneral($openWo, 'CLOSE-' . $request->user['name'] . ' close Oustanding WO');
            }
            $openWo->save();
        }

        $openTT = TroubleTicket::query()->where('visit_id', $visit->id)->first();
        if ($openTT) {
            $openTT->status = 'Close';
            $openTT->save();
            $this->logGeneral($openWo, 'CLOSE-' . $request->user['name'] . ' close Troubleticket');

        }
        return $visit;
    }

    public function destroy($id)
    {
        $visit = Visit::findOrFail($id);

        $visitHistory = VisitHistory::create([
            'work_order' => $visit->work_order,
            'user_id' => $visit->user['id'],
            'location_id' => $visit->location_id,
            'tipe_kunjungan' => $visit->tipe_kunjungan,
            'detail_kunjungan' => $visit->detail_kunjungan,
            'pengelola' => $visit->pengelola,
            'pengelola_pic' => $visit->pengelola_pic,
            'pengelola_phone' => $visit->pengelola_phone,
            'checkout_time' => $visit->checkout_time,
            'checkin_time' => $visit->checkin_time,
            'photo' => $visit->photo,
            'photo_sn' => $visit->photo_sn,
            'photo_ar' => $visit->photo_ar,
            'longitude' => $visit->longitude,
            'latitude' => $visit->latitude,
        ]);


        $checklistHistory = Checklist::query()->where('visit_id', $id);
        foreach ($checklistHistory as $attribute) {
            $idChecklist = $attribute['id'];
            $deskripsi = $attribute['deskripsi'];
            $tipe = $attribute['tipe'];
            $option = $attribute['option'];
            $photo = $attribute['photo'];
            $is_photo = $attribute['is_photo'];
            $value = $attribute['value'];
            $vch = VisitChecklistHistory::query()->where('visit_history_id', $visitHistory->id)->where('deskripsi', $deskripsi)->first();
            if ($vch) {
                $vch->value = $value;
                $vch->photo = $photo;
                $vch->is_photo = $is_photo;
                $vch->save();
            } else {
                VisitChecklistHistory::create([
                    'visit_history_id' => $visitHistory->id,
                    'deskripsi' => $deskripsi,
                    'tipe' => $tipe,
                    'option' => $option,
                    'value' => $value ? $value : '',
                    'is_photo' => $is_photo
                ]);
            }
            Checklist::destroy($idChecklist);
        }
        $visit->work_order = null;
        $visit->save();
        $this->logGeneral($visit, 'DELETE-' . auth()->user()->name . ' delete Visit');

        return Visit::destroy($id);
    }

    public function countOpen()
    {

        $user = User::find(Auth::id());
        $count = Visit::select([DB::raw('count(id) as "visits"')])
            ->where('user_id', $user->id)->where('close_time', null)
            ->get();
        return $count;
    }

    public function countPerDay(Request $request)
    {
        if ($request->month != null) {
            $req = date_create($request->month);
            $nowM = date_format($req, "m");
            $nowY = date_format($req, "Y");
        }
        $count = Visit::select([DB::raw('DATE(checkin_time) as date'),
            DB::raw('count(id) as "visits"')])
            ->whereMonth('checkin_time', $nowM)->whereYear('checkin_time', $nowY)
            ->groupBy(DB::raw('DATE(checkin_time)'))
            ->get();
        return $count;
    }

    public function countPerUser()
    {
        $user = User::find(Auth::id());
        $query = DB::select(DB::raw("SELECT
	count(id) as visit
            FROM
	        t_visit
            WHERE
	        checkout_time IS NULL
	        AND user_id = $user->id"));
        return $query;
    }

    public function countPerRegion(Request $request)
    {
        DB::enableQueryLog();
        $now = date('m');
        if ($request->month != null) {
            $now = $request->month;
        }
        $query = DB::select(DB::raw("SELECT
	m_region.`name`,
	COUNT(t_visit.id) as count
FROM
	m_region
	LEFT JOIN
	users
	ON
		m_region.id = users.region_id
	CROSS JOIN
	t_visit
WHERE
	m_region.id = users.region_id AND
	users.id = t_visit.user_id  AND
	MONTH(t_visit.checkin_time) = $now
GROUP BY
	m_region.`name`
		"));
        return $query;
    }

    public function counPerRegionPerDay(Request $request)
    {
        DB::enableQueryLog();
        $nowM = date('m');
        $nowY = date('Y');
        $daysCount = cal_days_in_month(CAL_GREGORIAN, $nowM, $nowY);
        if ($request->month != null) {
            $req = date_create($request->month);
            $nowM = date_format($req, "m");
            $nowY = date_format($req, "Y");
            $daysCount = cal_days_in_month(CAL_GREGORIAN, $nowM, $nowY);
        }
        for ($i = 1; $i <= $daysCount; $i++) {
            $dates[] = $nowY . "-" . $nowM . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
        }
        $regions = Region::all();
        $result = array();
        foreach ($regions as $reg) {
            $id = $reg->id;
            $name = $reg->name;

            $listDate = [];
            foreach ($dates as $tanggal) {
                $count = Visit::query()
                    ->whereDate('checkin_time', $tanggal)
                    ->whereHas('location', function ($query) use ($id){
                        $query->whereHas('service_base', function ($query) use ($id){
                            $query->where('region_id',$id);
                        });
                    })->count();
                $listReg = array();

                $listDate[] = array(
                    'date' => $tanggal,
                    'count' => $count
                );
            }

            $result[] = array(
                'id' => $id,
                'region' => $name,
                'data' => $listDate,
            );
        }
        return $result;

        /*foreach ($query as $region) {
            $id = $region->id;
            $name = $region->name;

            $end_time = strtotime('+1 days', strtotime($request->end));
            $start_time = strtotime($request->start);


            $listDate = null;
            for ($i = $start_time; $i < $end_time; $i += 86400) {
                $date = date('Y-m-d', $i);
                $query = DB::select(DB::raw("SELECT
                                        COUNT(t_visit.id) AS count
                                    FROM
                                        m_region
                                        LEFT JOIN
                                        users
                                        ON
                                            m_region.id = users.region_id
                                        CROSS JOIN
                                        t_visit
                                    WHERE
                                        m_region.id = users.region_id AND
                                        users.id = t_visit.user_id AND
                                        m_region.id = $id AND
                                        DATE(t_visit.checkin_time) = '$date'"
                ));
                $listLabel[] = array('date' => date('Y-m-d', $i));
                $listDate[] = array(
                    'date' => date('Y-m-d', $i),
                    'count' => $query[0]->count
                );
            }

            $obj[] = array(
                'region' => $name,
                'data' => $listDate,
            );
        }*/

        return $obj;
    }

    public function getReport(Request $request)
    {
        try {
            DB::enableQueryLog();
            $query = Visit::with(['checklists']);

            if (!$this->IsNullOrEmptyString($request->customer_id)) {
                $customer = $request->customer_id;
                $query->whereHas('location', function ($query) use ($customer) {
                    $query->where('customer_id', $customer);
                });
            }

            if (!$this->IsNullOrEmptyString($request->location_id)) {
                $query->where('location_id', $request->location_id);
            }

            if (!$this->IsNullOrEmptyString($request->user_id)) {
                $user = $request->user_id;
                $query->where('user_id', $user);

            }
            if($request->start_end){
                $query->whereBetween('checkin_time',$request->start_end)
                    ->whereBetween('checkout_time',$request->start_end);
            }


            $user = User::find(Auth::id());
            $role = $request->role;

            switch ($role) {
                case 'vendor':
                    $query->whereHas('user', function ($query) use ($user) {
                        $query->where('vendor_id', $user->vendor_id);
                    });
                    break;
                case 'engineer':
                case 'supervisor':
                    $query->where('user_id', $user->id);
                    break;
                case 'customer':
                    $query->whereHas('location', function ($query) use ($user) {
                        $query->where('customer_id', $user->customer_id);
                    });
                    break;
            }
            return $this->HttpOkWithData(false, '', VisitCollection::collection($query->orderBy('created_at', 'asc')->get()));
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace(),

            ])->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
        }
    }


    public function getReport2(Request $request)
    {

        $now = date('m-d-Y-His');
        $user = User::find(Auth::id());
        $filename = 'export/nmapp_visit_log_' . $request->startDate . '-' . $request->endDate . '.xlsx';
        //return (new RegionsExport)->download('nmapp_region_data_'.$now.'.xlsx', \Maatwebsite\Excel\Excel::XLSX,  ['Filename' => 'nmapp_region_data_'.$now.'.xlsx']);
        //Excel::store(new VisitExport($request->startDate, $request->endDate), 'export/nmapp_visit_data_'.$now.'.xlsx','public');
        $testDispatch = (new VisitExport($request->startDate, $request->endDate))
            ->store($filename, 'public')
            ->chain([
                new NotifyVisitExport($filename)
            ]);


        return back()->withSuccess('export started');
    }


    public function countPerDayByRole(Request $request)
    {
        $query = Visit::query();
        if ($request->month != null) {
            $req = date_create($request->month);
            $nowM = date_format($req, "m");
            $nowY = date_format($req, "Y");
            $query->whereMonth('checkin_time', $nowM)->whereYear('checkin_time', $nowY);
        }

        $user = User::find(Auth::id());
        $roles = auth()->user()->roles()->get()->pluck('name');
        $role = $roles[0];

        switch ($role) {
            case 'engineer':
                $query->select([DB::raw('DATE(checkin_time) as date'),
                    DB::raw('count(user_id) as "visits"')])
                    ->groupBy(DB::raw('DATE(checkin_time)'))->where('user_id', $user->id)
                    ->orderBy('date', 'ASC')->get();
                break;
            case 'vendor':
                $query->select([DB::raw('DATE(checkin_time) as date'),
                    DB::raw('count(user_id) as "visits"')])
                    ->groupBy(DB::raw('DATE(checkin_time)'))->whereHas('vendor', function ($query) use ($user) {
                        $query->where('vendor_id', $user->vendor_id);
                    })->orderBy('date', 'ASC')->get();
            case 'customer':
                $query->select([DB::raw('DATE(checkin_time) as date'),
                    DB::raw('count(user_id) as "visits"')])
                    ->groupBy(DB::raw('DATE(checkin_time)'))->whereHas('location', function ($query) use ($user) {
                        $query->where('customer_id', $user->customer_id);
                    })->orderBy('date', 'ASC')->get();
                break;
            default:
                $query->select([DB::raw('DATE(checkin_time) as date'),
                    DB::raw('count(user_id) as "visits"')])
                    ->orderBy('date', 'ASC')->get();
                break;
        }

        return $this->HttpOkWithData(false, '', $query);
    }


    public function countTotalByRole(Request $request)
    {
        DB::enableQueryLog();
        $query = Visit::query();
        $req = now();
        $nowM = date_format($req, "m");
        $nowY = date_format($req, "Y");
        if ($request->month != null) {
            $req = date_create($request->month);
            $nowM = date_format($req, "m");
            $nowY = date_format($req, "Y");
        }
        $user = User::find(Auth::id());
        $roles = auth()->user()->roles()->get()->pluck('name');
        $role = $roles[0];
        $query = $query->whereMonth('checkin_time', $nowM)->whereYear('checkin_time', $nowY);
        $taskQuery = TaskDetail::query()->whereMonth('created_at', $nowM)->whereYear('created_at', $nowY)->whereNull('close_date');
        switch ($role) {
            case 'engineer':
                $query->where('user_id', $user->id);
                $taskQuery = $taskQuery->whereHas('taskUser', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                })->count();
                break;
            case 'vendor':
                $query->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
                $taskQuery = null;
                break;
            case 'customer':
                $query->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                $taskQuery = null;
                break;
            default:
                $taskQuery = $taskQuery->count();
        }
        $data = $query->get();
        $total = $data->count();
        $open = $data->whereNull('photo_ar')->count();
        $close = $data->whereNotNull('photo_ar')->count();

        $result = [
            'task' => $taskQuery,
            'total' => $total,
            'open' => $open,
            'close' => $close,
        ];


        return $this->HttpOkWithData(false, '', $result);
    }


}
