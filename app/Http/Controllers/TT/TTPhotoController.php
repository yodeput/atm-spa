<?php

namespace App\Http\Controllers\TT;


use App\Models\TT\TTPhoto;
use Illuminate\Http\Request;

class TTPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TTPhoto  $tTPhoto
     * @return \Illuminate\Http\Response
     */
    public function show(TTPhoto $tTPhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TTPhoto  $tTPhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(TTPhoto $tTPhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TT\TTPhoto  $tTPhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TTPhoto $tTPhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TTPhoto  $tTPhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(TTPhoto $tTPhoto)
    {
        //
    }
}
