<?php

namespace App\Http\Controllers\TT;


use App\Models\TT\TTMessage;
use App\Models\TT\TTMessageStatus;
use App\Traits\CustomResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TTMessageController extends Controller
{


    public function findByTT($id)
    {
        return TTMessage::query()->where('tt_id', $id);
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $data = TTMessage::create([
            'message' => $request->message,
            'trouble_ticket_id' => $request->tt_id,
            'sender_id' => $user->id,
        ]);

        $statusData = TTMessageStatus::create([
            'is_read' => true,
            'is_deleted' => false,
            'tt_message_id' => $data->id,
            'sender_id' => $user->id,
        ]);

        return  $this->HttpCreated(false,'Edit message success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TTMessage  $tTMessage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return TTMessage::findOrFail($id);
    }


    public function update(Request $request, TTMessage $tTMessage)
    {
        TTMessage::find($request->id);
        return  $this->HttpOk(false,'Edit message success');
    }

    public function delete($id)
    {
        $data = TTMessage::findOrFail($id);
        $data->is_deleted = true;
        $data->save();
        return  $this->HttpOk(false,'Delete message success');
    }
}
