<?php

namespace App\Http\Controllers\TT;


use App\Models\TT\TTMessageStatus;
use Illuminate\Http\Request;

class TTMessageStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TTMessageStatus  $tTMessageStatus
     * @return \Illuminate\Http\Response
     */
    public function show(TTMessageStatus $tTMessageStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TTMessageStatus  $tTMessageStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(TTMessageStatus $tTMessageStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TTMessageStatus  $tTMessageStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TTMessageStatus $tTMessageStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TTMessageStatus  $tTMessageStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(TTMessageStatus $tTMessageStatus)
    {
        //
    }
}
