<?php

namespace App\Http\Controllers\TT;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Models\Master\Checklist;
use App\Models\TT\TroubleTicket;
use App\Models\TT\TTPhoto;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Visit;
use App\Models\Visit\VisitChecklist;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TroubleTicketController extends Controller
{
    use ActivityTraits;

    public function filter (Request $request)
    {
        $query = TroubleTicket::query();

        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];
        switch ($role) {
            case 'customer':
                $query->where('user_id', $user->id);
        }
        if ($request->search) {
            $search = $request->search;
            $query->where('ticket_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('problem_reported', 'LIKE', '%' . $request->search . '%')
                ->orWhereHas('location', function ($query) use ($search) {
                    $query->where('serial_number', 'LIKE', '%' . $search . '%')
                        ->orWhere('mc_id', 'LIKE', '%' . $search . '%')
                        ->orWhere('address', 'LIKE', '%' . $search . '%')
                        ->whereHas('customer', function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                                ->orWhere('code', 'LIKE', '%' . $search . '%');
                        });
                });
        }
        if ($request->status) {
            $query->where('ticket_number', 'LIKE', '%' . $request->status . '%');
        }


        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $data;
    }

    public function getTTOpen ()
    {

        return TroubleTicket::query()->where('status','!=','CLOSE');
    }

    public function show ($id)
    {
        return TroubleTicket::findOrFail($id);
    }

    public function store (Request $request)
    {
        $this->validate($request, [
            'problem_reported' => 'required|string',
            'location_id' => 'required'
        ]);

        $data = TroubleTicket::create([
            'problem_reported' => $request->problem_reported,
            'location_id' =>  $request->location ? $request->location['id'] : null,
            'user_id' =>  Auth::user()->id,
        ]);



        if ($request->photos) {
            foreach ($request->photos as $photo){
                $file = $photo;
                $nama_file = rand() . $file->getClientOriginalName();
                $url = '/storage/problem_report/'. $nama_file;
                $file->move('storage/problem_report/', $nama_file);
                TTPhoto::create([
                    'photo' => $url,
                    'tt_id' => $data->id,
                    'description' => $data->problem_reported
                ]);
            }
        }

        $this->logCreatedActivity($data, Auth::user()->name.' create a TroubleTicket', $request->toArray());

        return $data;
    }

    public function update (Request $request)
    {

        $data = TroubleTicket::find($request->id);
        $beforeUpdateValues = $data->toArray();

        $data->ticket_number = $request->ticket_number;
        $data->location_id = $request->location ? $request->location['id'] : null;
        $data->problem_reported = $request->problem_reported;

        if($request->work_order){
            $visit = Visit::create([
                'work_order' => $request->work_order,
                'user_id' => $request->ce ? $request->ce['id'] : null,
                'location_id' => $data->location_id,
                'tipe_kunjungan' => $request->tipe_kunjungan,
                'detail_kunjungan' => $request->problem_reported,
            ]);

            $data->status = 'Assigned';
            $data->visit_id =  $visit->id;
        }

        $data->save();
        $afterUpdateValues = $data->getChanges();
        $this->logUpdatedActivity($data, $beforeUpdateValues, $afterUpdateValues);
        return $data;
    }

    public function updateCheckin (Request $request)
    {
        $data = TroubleTicket::find($request->id);
        $visit = Visit::find($data->visit_id);
        $visit->checkin_time = now();
        $visit->pengelola = $request->pengelola;
        $visit->pengelola_pic = $request->pengelola_pic;
        $visit->pengelola_phone = $request->pengelola_phone;
        $visit->longitude = $request->longitude;
        $visit->latitude = $request->latitude;

        $checklistQuery = Checklist::query()->where('is_active', true);

        $checklistsData = $checklistQuery->where('is_general', true)->orWhere('customer_id', $request->atm["customer_id"])
            ->get();

        foreach ($checklistsData as $attribute) {
            $visit_id = $visit->id;
            $deskripsi = $attribute['deskripsi'];
            $tipe = $attribute['tipe'];
            $option = $attribute['value'];
            $is_photo = $attribute['is_photo'];
            $value = '';
            $visitPhoto = VisitChecklist::create([
                'visit_id' => $visit_id,
                'deskripsi' => $deskripsi,
                'tipe' => $tipe,
                'option' => $option,
                'value' => $value,
                'is_photo' => $is_photo
            ]);
        }
        $today = date("mY", strtotime($visit->checkin_time));
        $now = date('m-d-Y-His');
        if ($request->photo) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_selfie' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                }

            }
        }

        if ($request->photo_sn) {
            $baseUrl = config('filesystems.disks.azure.url');
            $path = 'visit/' . $today . '/' . $visit->work_order;
            $index = File::get('images/index.php');
            $upload = new Upload();
            $upload->upload($index, $path . '/index.php');
            $base64_image = $request->photo_sn;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $fileName = $visit->work_order . '_serial_number' . '_' . $now . '.' . $extension;
                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $visit->photo = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $visit->photo_sn = $photo_url;
                }
            }
        }
        $openWo = OpenWo::query()->where('work_order', $request->work_order)->first();
        if ($openWo != null) {
            if( $visit->photo_ar === null){
                $openWo->status = 'On Progress';
                $openWo->save();
            }
        }

        $data->status = 'On Progress';

        $visit->save();
        $data->save();
        return $data;
    }

    public function destroy ($id)
    {
        $data = TroubleTicket::find($id);
        $this->logDeletedActivity($data, 'TroubleTicket ' . $data->name);
        return TroubleTicket::destroy($id);
    }

    public function count ()
    {
        return TroubleTicket::count();
    }

}
