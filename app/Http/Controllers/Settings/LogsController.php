<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function MongoDB\BSON\toJSON;

class LogsController extends Controller
{
    public function filter(Request $request)
    {

        $query = DB::table('activity_log');


        if ($request->search) {
            $query->where('causer_id', $request->search );
        }

        $logs = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));


        foreach ($logs as $attribute) {
            $user_id = $attribute->causer_id;
            $queryUser = User::query()
                ->where('id',  $user_id )
                ->get();
            $attribute->users= $queryUser;
        }


        return $logs;
    }


}
