<?php

namespace App\Http\Controllers\Settings;

use App\Models\Setting;
use App\Traits\ActivityTraits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class SettingsController extends Controller
{
    use ActivityTraits;

    public function filter()
    {

        return Setting::All();
    }

    public function update(Request $request)
    {

        $i = 0;
        $setting = null;
        $beforeUpdateValues = null;
        $count = count($request->all());
        $array = $request->all();
        foreach ($array as $input) {
            $id = $input['id'];
            $value = $input['value'];
            $setting = Setting::findOrFail($id);

            $beforeUpdateValues = $setting->toArray();

            if ($setting->slug_name === 'app_logo') {
                if ($setting->value != $value) {
                    $base64_image = $value;
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                        $data = substr($base64_image, strpos($base64_image, ',') + 1);
                        $extension = explode('/', mime_content_type($base64_image))[1];
                        $data = base64_decode($data);
                        $path = 'settings/' . $setting->slug_name . '.' . $extension;
                        Storage::disk('public')->put($path, $data);
                        $setting->value = $setting->slug_name . '.' . $extension;
                        $setting->save();
                    }
                }
            } else {
                $setting->value = $value;
                $setting->save();
            }

        }
        $afterUpdateValues = $setting->getChanges();
        $this->logUpdatedActivity($setting, $beforeUpdateValues, $afterUpdateValues);
        return $array;
    }
}
