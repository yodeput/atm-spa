<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Upload;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Avatar;

class ProfileController extends Controller
{
    use ActivityTraits;

    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'username' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);

        return tap($user)->update($request->only('username','name', 'email'));
    }

    public function updateAuthUser (Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.Auth::id()
        ]);

        $user = User::find(Auth::id());
        $beforeUpdateValues = $user->toArray();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->telepon = $request->telepon;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->player_id = $request->player_id;
        $user->current_latitude = $request->current_latitude;
        $user->current_longitude = $request->current_longitude;
        $user->alamat = $request->alamat;
        $user->region_id = $request->region ? $request->region['id'] : null;
        $user->service_base_id = $request->service_base ? $request->service_base['id'] : null;
        $user->customer_id = $request->customer ? $request->customer['id'] : null;
        if ($user->name != $request->name) {
            $user->name = $request->name;
        }
        $user->save();
        $afterUpdateValues = $user->getChanges();

//        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
//        Storage::disk('azure')->put('avatars/'.$user->id.'/avatar.png', (string) $avatar);

        $this->logUpdatedActivity($user, $beforeUpdateValues, $afterUpdateValues);
        return $user;
    }

    public function updatePasswordAuthUser(Request $request)
    {
        $this->validate($request, [
            'current' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string'
        ]);

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Password Lama tidak sesuai']]], 422);
        }

        $user->password = Hash::make($request->password);
        $user->save();
        $this->logGeneral($user,'UPDATE-'.$user->name.' Change Password');

        return $user;
    }

    public function uploadAvatarAuthUser(Request $request)
    {
        $upload = new Upload();
        $path = 'avatars/'.Auth::id();
        $avatar = $upload->upload($request->file, $path)->getData();
        $user = User::find(Auth::id());
        $beforeUpdateValues = $user->toArray();
        $user->avatar = '/storage/'.$avatar['path'];
        $user->save();
        $afterUpdateValues = $user->getChanges();
        $this->logGeneral($user,'UPDATE-'.$user->name.' upload Profile Picture');

        return $user;
    }

    public function removeAvatarAuthUser()
    {
        $user = User::find(Auth::id());
        $user->avatar = '/images/user.png';
        $user->save();
        $this->logGeneral($user,'DELETE-'.$user->name.' reset Profile Picture');
        return $user;
    }
}
