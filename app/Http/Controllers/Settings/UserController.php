<?php

namespace App\Http\Controllers\Settings;

use App\Imports\ServicebasesImport;
use App\Imports\UsersImport;
use App\Models\Master\Location;
use App\Models\Visit\Visit;
use App\Traits\ActivityTraits;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Avatar;
use Illuminate\Support\Facades\Auth;

use App\User;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    use ActivityTraits;

    public function current(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * @OA\Get(
     *     path="/api/users/filter",
     *     description="Get All User",
     *     @OA\Response(response="default", description="Welcome page")
     * )
     */
    public function filter(Request $request)
    {
        $query = User::query()->withTrashed();

        if ($request->search) {
            $search = $request->search;
            $query->where('name', 'LIKE', '%' . $request->search . '%')
                ->orWhere('email', 'LIKE', '%' . $request->search . '%')
                ->orWhere('username', 'LIKE', '%' . $request->search . '%')
                ->orWhere('ce_code',  $request->search)
                ->orWhere('qlid',  $request->search)
                ->orwhereHas('region', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                })
                ->orwhereHas('service_base', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                })
                ->orwhereHas('customer', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                })
                ->orwhereHas('vendor', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                });
        }

        $users = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        $users->load('roles');
        return $users;
    }

    public function getAll(){
        /*return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);*/
        return User::orderBy('username')->whereHas('roles', function ($query) {
            $query->where('name', 'engineer');
        })->get();
    }

    public function show($user)
    {
        $user = User::withTrashed()->findOrFail($user);
        $user->roles = $user->getRoleNames();
        return $user;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
            'roles' => 'required|array'
        ]);
        $rolesNames = array_pluck($request->roles, ['name']);
        switch ($rolesNames[0]) {
            case 'customer':
                $this->validate($request, [
                    'name' => 'required|string',
                    'username' => 'required|string|unique:users',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|string',
                    'roles' => 'required|array',
                    'customer' => 'required',
                ]);

                break;
            case 'engineer':
                $this->validate($request, [
                    'name' => 'required|string',
                    'username' => 'required|string|unique:users',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|string',
                    'roles' => 'required|array',
                    'service_base' => 'required',
                    'ce_code' => 'required',
                    'qlid' => 'required'
                ]);
                break;
            case 'vendor':
                $this->validate($request, [
                    'name' => 'required|string',
                    'username' => 'required|string|unique:users',
                    'email' => 'required|email|unique:users',
                    'password' => 'required|string',
                    'roles' => 'required|array',
                    'vendor' => 'required',
                ]);

                break;
            default:
        }

        $this->create($request);

    }

    function create(Request $request){
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'password' => Hash::make($request->password),
            'ce_code' => $request->ce_code,
            'qlid' => $request->qlid,
            'region_id' => $request->region ? $request->region['id'] : null,
            'service_base_id' => $request->service_base ? $request->service_base['id'] : null,
            'customer_id' => $request->customer ? $request->customer['id'] : null,
            'vendor_id' => $request->vendor ? $request->vendor['id'] : null,
            'is_active' => true,
            'email_verified_at' => now()
        ]);



        $rolesNames = array_pluck($request->roles, ['name']);
        $user->assignRole($rolesNames);
        $this->logCreatedActivity($user, Auth::user()->name . ' create a User', $request->toArray());
        $user->avatar = '/images/user.png';
        $user->save();
        return $user;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'username' => 'required|string|unique:users,username,' . $request->id,
            'password' => 'string|nullable',
            'roles' => 'required|array'
        ]);

        $user = User::find($request->id);
        $beforeUpdateValues = $user->toArray();
        if ($user->name != $request->name) {
            $user->name = $request->name;
        }
        if ($user->email != $request->email) {
            $user->email = $request->email;
        }

        if ($user->username != $request->username) {
            $user->username = $request->username;
        }

        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }

        $user->ce_code = $request->ce_code;
        $user->qlid = $request->qlid;
        $user->telepon = $request->telepon;
        $user->alamat = $request->alamat;
        $user->service_base_id = $request->service_base ? $request->service_base['id'] : null;
        $user->customer_id = $request->customer ? $request->customer['id'] : null;
        $user->vendor_id = $request->vendor ? $request->vendor['id'] : null;
        $user->is_active = $request->is_active;
        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;

        $user->save();
        $afterUpdateValues = $user->getChanges();
        $this->logUpdatedActivity($user, $beforeUpdateValues, $afterUpdateValues);

        $rolesNames = array_pluck($request->roles, ['name']);
        $user->syncRoles($rolesNames);

        return $user;
    }

    public function destroy($user)
    {
        $data = User::find($user);
        $data->is_active = false;
        $data->is_delete = true;
        $data->save();
        $this->logDeletedActivity($data, 'User ' . $data->name);
        return User::destroy($user);
    }

    public function recover($user)
    {
        $user = User::withTrashed()->findOrFail($user);
        $user->is_active = true;
        $user->is_delete = false;
        $user->save();
        $user->restore();
        $this->logRecoveredActivity($user, 'User ' . $user->name);
        return $user;
    }

    public function count()
    {
        return User::count();
    }

    public function getUserRoles($user)
    {
        $user = User::findOrFail($user);
        $user->getRoleNames();

        return $user;
    }

    public function getUserMe()
    {
        $user = auth()->user();
        $user->roles = $user->getRoleNames();
        return $user;
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();

        $file->move('storage/file_import', $nama_file);

        Excel::import(new UsersImport(), public_path('/storage/file_import/' . $nama_file));


        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }

    public function filterMap(Request $request)
    {
        $query = User::query()->whereNotNull('current_latitude');
        $users = $query->get();

        foreach ($users as $u) {

            $id = $u->id;
            $visit = Visit::query()->where('user_id', $id)->whereNull('checkout_time')->first();
            $u['visit'] = $visit;
        }

        return response($users)
            ->header('Content-Type', 'application/json')
            ->header('X-Total-Count', $users->count());
    }

}
