<?php

namespace App\Http\Controllers\Master;

use App\Imports\ServicebasesImport;
use App\Models\Master\Location;
use App\Models\Master\Region;
use App\Models\Master\ServiceBase;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

use Avatar;


class ServicebasesController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = ServiceBase::query()->withTrashed()->with('region');

        if ($request->search) {
            $search = $request->search;
            $query->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('code', 'LIKE', '%'.$request->search.'%')
                ->orWhere('region_id', 'LIKE', '%'.$request->search.'%')
                ->orwhereHas('region',  function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                });
        }

        $servicebase = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        /*foreach ($servicebase as $attribute) {
            $attribute['total_user']=$attribute->getTotalUserAttribute();
            $attribute['total_atm']=$attribute->getTotalAtmAttribute();
        }*/

        return $servicebase;
    }

    public function show($user)
    {
        return ServiceBase::findOrFail($user);
    }

    public function all()
    {
        return ServiceBase::orderBy('name')->get();
    }

    public function byRegion(Request $request)
    {
        if($request->search) {
            $query = ServiceBase::orderBy('name')->where('region_id', 'LIKE', '%' . $request->search['id'] . '%')->get();
            $query->load('region');
            return $query;
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string',
            'region' => 'required'
        ]);

        $customer = ServiceBase::create([
            'code' => $request->code,
            'name' => $request->name,
            'region_id' => $request->region['id']
        ]);
        $this->logCreatedActivity($customer, Auth::user()->name.' create a Region', $request->toArray());

        return $customer;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string',
            'region' => 'required'
        ]);

        $customer = ServiceBase::find($request->id);
        $beforeUpdateValues = $customer->toArray();

        if ($customer->name != $request->name) {
            $customer->name = $request->name;
        }
        if ($customer->code != $request->code) {
            $customer->code = $request->code;
        }
        $customer->region_id = $request->region['id'];


        $customer->save();
        $afterUpdateValues = $customer->getChanges();
        $this->logUpdatedActivity($customer, $beforeUpdateValues, $afterUpdateValues);

        return $customer;
    }

    public function destroy ($customer)
    {
        $data = ServiceBase::find($customer);
        $this->logDeletedActivity($data, 'Service Base ' . $data->name);
        return ServiceBase::destroy($customer);
    }

    public function recover($location)
    {
        $data = ServiceBase::withTrashed()->find($location);
        $data->restore();
        $this->logRecoveredActivity($data, 'Service Base ' . $data->name);
        return $data;
    }

    public function count()
    {
        return ServiceBase::count();
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();

        $file->move('storage/file_import', $nama_file);

        Excel::import(new ServicebasesImport(), public_path('/storage/file_import/' . $nama_file));


        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }
}
