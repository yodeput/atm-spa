<?php

namespace App\Http\Controllers\Master;

use App\Exports\WoTypeExport;
use App\Http\Controllers\Controller;
use App\Imports\WoTypeImport;
use App\Models\Master\WoType;
use App\Traits\ActivityTraits;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Maatwebsite\Excel\Facades\Excel;

class WoTypeController extends Controller
{
    use ActivityTraits;

    public function filter (Request $request)
    {
        $query = WoType::query();

        if($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('description', 'LIKE', '%'.$request->search.'%');
        }

        $regions = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $regions;
    }

    public function all()
    {
        return WoType::orderBy('id')->get()->pluck('name');
    }

    public function show ($user)
    {
        return WoType::findOrFail($user);
    }

    public function store (Request $request)
    {
        $this->validate($request, [
            'description' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = WoType::create([
            'description' => $request->description,
            'name' => $request->name
        ]);
        $this->logCreatedActivity($customer, Auth::user()->name.' create a WO Type', $request->toArray());


        return $customer;
    }

    public function update (Request $request)
    {
        $this->validate($request, [
            'description' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = WoType::find($request->id);
        $beforeUpdateValues = $customer->toArray();

        if ($customer->name != $request->name) {
            $customer->name = $request->name;
        }
        if ($customer->description != $request->description) {
            $customer->description = $request->description;
        }

        $customer->save();
        $afterUpdateValues = $customer->getChanges();
        $this->logUpdatedActivity($customer, $beforeUpdateValues, $afterUpdateValues);
        return $customer;
    }

    public function destroy ($customer)
    {
        $data = WoType::find($customer);
        $this->logDeletedActivity($data, 'WO Type ' . $data->name);
        return WoType::destroy($customer);
    }

    public function count ()
    {
        return WoType::count();
    }

    public function exportData()
    {
        try {
            $now = date('m-d-Y-His');
            //return (new RegionsExport)->download('nmapp_region_data_'.$now.'.xlsx', \Maatwebsite\Excel\Excel::XLSX,  ['Filename' => 'nmapp_region_data_'.$now.'.xlsx']);
            Excel::store(new WoTypeExport(), 'export/nmapp_wo_type_data_'.$now.'.xlsx','public');
            return response()->json([
                'error' => false,
                'msg' => 'export/nmapp_wo_type_data_'.$now.'.xlsx',
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);

        } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'exc' =>$e->getMessage(),
                'msg' => "Export WO Type gagal",
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand().$file->getClientOriginalName();


        $file->move('storage/file_import',$nama_file);


        Excel::import(new WoTypeImport, public_path('/storage/file_import/'.$nama_file));

        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }



}

