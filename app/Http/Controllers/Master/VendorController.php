<?php

namespace App\Http\Controllers\Master;

use App\Models\Master\Vendor;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

use Maatwebsite\Excel\Facades\Excel;


class VendorController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = Vendor::query()->withTrashed();

        if ($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('code', 'LIKE', '%'.$request->search.'%');
        }

        $customer = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        foreach ($customer as $attribute) {
            $attribute['total_atm']=$attribute->getTotalUserAttribute();
        }
        return $customer;
    }

    public function getDeleted()
    {
        return Vendor::onlyTrashed()->get();

    }

    public function restore($id)
    {
        $customer = Vendor::onlyTrashed()->where('id', $id);
        $customer->restore();
        return $customer;
    }

    public function all()
    {
        return Vendor::orderBy('name')->get();
    }

    public function show($customer)
    {
        return Vendor::findOrFail($customer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Vendor::create([
            'code' => $request->code,
            'name' => $request->name
        ]);
        $this->logCreatedActivity($customer, Auth::user()->name.' create a Vendor', $request->toArray());

        return $customer;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Vendor::find($request->id);
        $beforeUpdateValues = $customer->toArray();

        if ($customer->name != $request->name) {
            $customer->name = $request->name;
        }
        if ($customer->code != $request->code) {
            $customer->code = $request->code;
        }

        $customer->save();
        $afterUpdateValues = $customer->getChanges();
        $this->logUpdatedActivity($customer, $beforeUpdateValues, $afterUpdateValues);

        return $customer;
    }

    public function destroy($customer)
    {
        $data = Vendor::find($customer);
        $this-> logDeletedActivity($data, 'Vendor '.$data->name);
        return Vendor::destroy($customer);
    }

    public function recover($customer)
    {
        $data = Vendor::withTrashed()->find($customer);
        $data->restore();
        $this-> logRecoveredActivity($data, 'Vendor '.$data->name);
        return $data;
    }

    public function count()
    {
        return Vendor::count();
    }
}
