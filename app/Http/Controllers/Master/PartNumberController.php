<?php

namespace App\Http\Controllers\Master;

use App\Exports\PartNumberExport;
use App\Imports\PartNumberImport;
use App\Models\Master\Location;
use App\Models\Master\PartNumber;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Avatar;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;


class PartNumberController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = PartNumber::query()->withTrashed();

        if ($request->search) {
            $query->where('part_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('description', 'LIKE', '%' . $request->search . '%');
        }

        if ($request->type) {
            $query->where('type', $request->type);
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $data;
    }

    public function all()
    {
        return PartNumber::orderBy('part_number')->get();
    }

    public function byType(Request $request)
    {
        $query = PartNumber::select(['id', 'part_number', 'type', 'description'])->where('type', $request->type);

        if ($request->search) {
            $query->where('part_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('description', 'LIKE', '%' . $request->search . '%');
        }

        return $query->orderBy('part_number', 'asc')->limit(20)
            ->get();
    }

    public function show($id)
    {
        return PartNumber::findOrFail($id);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string',
            'part_number' => 'required|string',
            'description' => 'required|string'
        ]);

        $data = PartNumber::create([
            'type' => $request->type,
            'part_number' => $request->part_number,
            'description' => $request->description,
        ]);
        $this->logCreatedActivity($data, Auth::user()->name . ' create a Part Number', $request->toArray());


        return $data;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string',
            'part_number' => 'required|string',
            'description' => 'required|string'
        ]);

        $data = PartNumber::find($request->id);
        $beforeUpdateValues = $data->toArray();

        $data->type = $request->type;
        $data->part_number = $request->part_number;
        $data->description = $request->description;
        $data->save();

        $afterUpdateValues = $data->getChanges();
        $this->logUpdatedActivity($data, $beforeUpdateValues, $afterUpdateValues);
        return $data;
    }

    public function destroy($customer)
    {
        $data = PartNumber::find($customer);
        $this->logDeletedActivity($data, 'Part Number ' . $data->name);
        return PartNumber::destroy($customer);
    }

    public function recover($location)
    {
        $data = PartNumber::withTrashed()->find($location);
        $data->restore();
        $this->logRecoveredActivity($data, 'Part Number ' . $data->name);
        return $data;
    }

    public function count()
    {
        return PartNumber::count();
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();


        $file->move('storage/file_import', $nama_file);


        Excel::import(new PartNumberImport, public_path('/storage/file_import/' . $nama_file));

        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }


}
