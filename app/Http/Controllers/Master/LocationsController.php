<?php

namespace App\Http\Controllers\Master;

use App\Exports\LocationsExport;
use App\Http\Helpers\Upload;
use App\Http\Helpers\UploadAzure;
use App\Imports\LocationsImport;
use App\Models\Master\Location;
use App\Models\Master\ServiceBase;
use App\Models\Visit\Visit;
use App\Traits\ActivityTraits;
use App\Traits\CustomResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Exception;
use Spatie\Permission\Models\Role;


class LocationsController extends Controller
{
    use ActivityTraits;
    use CustomResponse;

    public function filter(Request $request)
    {
        $query = Location::with(['region','customer','service_base'])->withTrashed();

        if ($request->search) {
            $search = $request->search;
            $query->where('serial_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('mc_id', 'LIKE', '%' . $request->search . '%')
                ->orWhere('model', 'LIKE', '%' . $request->search . '%')
                ->orWhere('site_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                ->orWhere('location_name', 'LIKE', '%' . $request->search . '%')
                ->orWhere('address', 'LIKE', '%' . $request->search . '%')
                ->orWhere('city', 'LIKE', '%' . $request->search . '%')
                ->orWhere('postal', 'LIKE', '%' . $request->search . '%')
                ->orwhereHas('service_base', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                })
                ->orwhereHas('customer', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                });
        }

        if($request->status && $request->status !== 'ALL'){
            $query->where('status', $request->status);
        }

        $location = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $location;
    }

    public function filterCustomer(Request $request)
    {
        $query = Location::with(['region','customer','service_base'])->withTrashed();

        if ($request->search) {
            $search = $request->search;
            $query->where('serial_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('mc_id', 'LIKE', '%' . $request->search . '%')
                ->orWhere('model', 'LIKE', '%' . $request->search . '%')
                ->orWhere('site_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                ->orWhere('location_name', 'LIKE', '%' . $request->search . '%')
                ->orWhere('address', 'LIKE', '%' . $request->search . '%')
                ->orWhere('city', 'LIKE', '%' . $request->search . '%')
                ->orWhere('postal', 'LIKE', '%' . $request->search . '%')
                ->orwhereHas('service_base', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                })
                ->orwhereHas('customer', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                });
        }

        if ($request->customer) {
            $customer = $request->customer;
            $query->orwhereHas('customer', function ($query) use ($customer) {
                $query->where('id',$customer);
            });
        }

        $location = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        return $location;
    }

    public function filterMap(Request $request)
    {
        $query = Location::query()->whereNotNull('latitude');
        $roles = Auth::user()->roles()->get()->pluck('name');
        $user = User::find(Auth::id());
        $role = $roles[0];

        switch ($role) {
            case 'customer':
                $query->where('customer_id', $user->customer_id);
                break;
        }

        $location = $query->get();


        foreach ($location as $loc) {

            $id = $loc->id;
            $visit = Visit::query()->where('location_id', $id)->whereNull('checkout_time')->first();
            $loc['visit'] = $visit;
            /* if ($visit==null){
                 array_splice($location,$index, $index);
             }*/
        }

        $location->load('region');
        $location->load('customer');
        $location->load('service_base');

        return response($location)
            ->header('Content-Type', 'application/json')
            ->header('X-Total-Count', $location->count());
    }

    public function filterPosition(Request $request)
    {
        DB::enableQueryLog();
        $query = Location::geofence($request->latitude, $request->longitude, 0, 1);
        $all = $query->get();
        //dd(DB::getQueryLog());
        //dd($request);
        return $all;
    }

    public function filterExport(Request $request)
    {
        $query = Location::query()->withTrashed();

        if ($request->filter === true) {
            $region = $request->region;
            $service_base = $request->service_base;
            $customer = $request->customer;
            if ($region) {
                $query->where('region_id', $region['id']);
            }
            if ($service_base) {
                $query->where('service_base_id', $service_base['id']);
            }
            if ($customer) {
                $query->where('customer_id', $customer['id']);
            }
        }
        $all = $query->get();

        return $all;
    }

    public function all()
    {
        return Location::all();
    }

    public function byCustomer(Request $request)
    {
        $query = Location::with(['customer'])->orderBy('serial_number')->Where('customer_id', $request->customer_id);
        if ($request->search) {
            $query->where('serial_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('mc_id', 'LIKE', '%' . $request->search . '%')
                ->orWhere('model', 'LIKE', '%' . $request->search . '%')
                ->orWhere('site_number', 'LIKE', '%' . $request->search . '%')
                ->orWhere('status', 'LIKE', '%' . $request->search . '%')
                ->orWhere('location_name', 'LIKE', '%' . $request->search . '%');
        }
        return $query->limit(20)->get();
    }

    public function show($id)
    {
        $location = Location::withTrashed()->findOrFail($id);
        $location->load('region');
        $location->load('customer');
        $location->load('service_base');
        return $location;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'mc_id' => 'required|string',
            'serial_number' => 'required|string|unique:m_location',
            'customer' => 'required',
            'address' => 'required|string',
            'city' => 'required|string',
            'postal' => 'required|string',
            /*   'service_base' => 'required',
               'region' => 'required',*/
        ]);


        $redId = $request->service_base ? $request->service_base['id'] : null;
        if($redId){
            $redId = ServiceBase::findOrFail($redId);
            $redId = $redId->region_id;
        }

        $location = Location::create([
            'mc_id' => $request->mc_id,
            'serial_number' => $request->serial_number,
            'region_id' => $redId ?: null,
            'service_base_id' => $request->service_base ? $request->service_base['id'] : null,
            'customer_id' => $request->customer ? $request->customer['id'] : null,
            'address' => $request->address,
            'city' => $request->city,
            'model' => $request->model,
            'install_date' => $request->install_date,
            'site_number' => $request->site_number,
            'location_name' => $request->location_name,
            'status' => $request->status,
            'postal' => $request->postal,
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
        ]);

        $baseUrl = config('filesystems.disks.azure.url');
        $upload = new Upload();
        $path = 'atm/' . $location->id;
        if ($request->image) {
            $base64_image = $request->image;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $now = date('m-d-Y-His');
                $fileName = $location->id . '_' . $now . '.' . $extension;

                $data = base64_decode($data);
                $upload = new Upload();
                if ($baseUrl != null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                }

            }
        }


//        if($request->image) {
//            $base64_image = $request->image;
//            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
//                $data = substr($base64_image, strpos($base64_image, ',') + 1);
//                $extension = explode('/', mime_content_type($base64_image))[1];
//                $data = base64_decode($data);
//                $path = 'atm/' . $location->id . '/' . $request->mc_id.'.'.$extension;
//                Storage::disk('public')->put($path, $data);
//                $find = Location::find($location->id);
//                $find->image = $request->mc_id.'.'.$extension;
//                $find->save();
//            }
//        }

        $this->logCreatedActivity($location, Auth::user()->name . ' create a Location', $request->toArray());

        return $location;
    }

    public function update(Request $request)
    {

        $location1 = Location::query()->where('serial_number', $request->serial_number)->first();
        $location = Location::find($request->id);
        if ($location1 && $location1->id !== $request->id) {
            $this->validate($request, [
                'serial_number' => 'required|string|unique:t_visit',
            ]);
        }

        $this->validate($request, [
            'mc_id' => 'required|string',
            'customer' => 'required',
            /*'address' => 'required|string',
            'city' => 'required|string',
            'postal' => 'required|string',
               'service_base' => 'required',
               'region' => 'required',*/

        ]);

        $location = Location::find($request->id);
        $beforeUpdateValues = $location->toArray();

        $redId = $request->service_base ? $request->service_base['id'] : null;
        if($redId){
            $redId = ServiceBase::findOrFail($redId);
            $redId = $redId->region_id;
        }


        $location->mc_id = $request->mc_id;
        $location->serial_number = $request->serial_number;
        $location->region_id = $redId ?: null;
        $location->service_base_id = $request->service_base ? $request->service_base['id'] : null;
        $location->customer_id = $request->customer ? $request->customer['id'] : null;
        $location->address = $request->address;
        $location->city = $request->city;
        $location->postal = $request->postal;
        $location->longitude = $request->longitude;
        $location->latitude = $request->latitude;
        $location->model = $request->model;
        $location->install_date = $request->install_date;
        $location->site_number = $request->site_number;
        $location->location_name = $request->location_name;
        $location->status = $request->status;

        $baseUrl = config('filesystems.disks.azure.url');
        $upload = new Upload();
        $path = 'atm/' . $location->id;
        if ($request->image) {
            $base64_image = $request->image;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $now = date('m-d-Y-His');
                $fileName = $location->id . '_' . $now . '.' . $extension;

                $data = base64_decode($data);
                $upload = new Upload();
                if ($baseUrl != null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                }

            }
        }

        $baseUrl = config('filesystems.disks.azure.url');
        $upload = new Upload();
        $path = 'atm/' . $location->id;
        if ($request->imageNew) {
            $base64_image = $request->imageNew;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $now = date('m-d-Y-His');
                $fileName = $location->id . '_' . $now . '.' . $extension;

                $data = base64_decode($data);
                $upload = new Upload();
                if ($baseUrl != null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $location->image = $photo_url;
                    $location->save();

                }

            }
        }

        $location->save();

        $afterUpdateValues = $location->getChanges();
        $this->logUpdatedActivity($location, $beforeUpdateValues, $afterUpdateValues);
        return $location;
    }

    public function destroy($location)
    {
        $data = Location::find($location);
        $this->logDeletedActivity($data, 'Location ' . $data->name);

        return Location::destroy($location);
    }

    public function recover($location)
    {
        $data = Location::withTrashed()->find($location);
        $data->restore();
        $this->logRecoveredActivity($data, 'Location ' . $data->name);
        return $data;
    }


    public function uploadPhoto(Request $request)
    {
        $location = Location::find($request->id);
        $beforeUpdateValues = $location->toArray();
        $baseUrl = config('filesystems.disks.azure.url');
        $path = 'atm/' . $location->id;
        if ($request->image) {
            $base64_image = $request->image;
            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);
                $extension = explode('/', mime_content_type($base64_image))[1];
                $now = date('m-d-Y-His');
                $fileName = $location->id . '_' . $now . '.' . $extension;

                $data = base64_decode($data);
                if ($baseUrl !== null) {
                    $upload = new UploadAzure();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = $baseUrl . $path . '/' . $fileName;
                    $location->image = $photo_url;
                } else {
                    $upload = new Upload();
                    $photo = $upload->upload($data, $path . '/' . $fileName)->getData();
                    $photo_url = '/storage/' . $path . '/' . $fileName;
                    $location->image = $photo_url;
                }
            }
        }
        $location->save();

        $afterUpdateValues = $location->getChanges();
        $this->logUpdatedActivity($location, $beforeUpdateValues, $afterUpdateValues);
        return $location;
    }


    public function count()
    {
        return Location::query()->where('status', 'Active')->count();
    }

    public function countMap()
    {

        $roles = Auth::user()->roles()->get()->pluck('name');
        $user = User::find(Auth::id());
        $role = $roles[0];
        $active = Location::query()->where('status', 'Active')->count();
        $nonActive = Location::query()->where('status', 'Non-Active')->count();
        $koordinat = Location::query()->whereNotNull('latitude')->count();
        $koordinatNull = Location::query()->whereNull('latitude')->count();

        switch ($role) {
            case 'customer':
                $active = Location::query()->where('status', 'Active')->where('customer_id', $user->customer_id)->count();
                $nonActive = Location::query()->where('status', 'Non-Active')->where('customer_id', $user->customer_id)->count();
                $koordinat = Location::query()->where('customer_id', $user->customer_id)->whereNotNull('latitude')->count();
                $koordinatNull = Location::query()->where('customer_id', $user->customer_id)->whereNull('latitude')->count();
                break;
        }

        $data = [
            'active' => $active,
            'nonActive' => $nonActive,
            'koordinat' => $koordinat,
            'koordinatNull' => $koordinatNull,
        ];
        return $this->HttpOkWithData(false, '', $data);
    }


    public function import(Request $request)
    {
        try {
            $this->validate($request, [
                'file' => 'required|mimes:csv,xls,xlsx'
            ]);

            $file = $request->file('file');

            $nama_file = rand() . $file->getClientOriginalName();


            $file->move('storage/file_import/', $nama_file);


            Excel::import(new LocationsImport, public_path('/storage/file_import/' . $nama_file));

            return $this->HttpCreated(false, "Import data berhasil");
        } catch (\Exception $e){

            return response()->json([
                'error' => true,
                'msg' => $e->getMessage(),
                'trace' => $e->getTrace(),

            ])->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
        }



    }


}
