<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Imports\ChecklistsImport;
use App\Models\Master\Checklist;
use App\Traits\ActivityTraits;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChecklistsController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = Checklist::query();

        if ($request->search) {
            $search = $request->search;
            $query->where('deskripsi', 'LIKE', '%' . $request->search . '%')
                ->orWhere('tipe', 'LIKE', '%' . $request->search . '%')
                ->orwhereHas('customer', function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('code', 'LIKE', '%' . $search . '%');
                });
        }

        $data = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));


        return $data;
    }

    public function all()
    {
        return Checklist::orderBy('id')->get();
    }

    public function allByCustomer(Request $request)
    {
        if ($request->customer_id) {
            $query = Checklist::where('customer_id', $request->customer_id)
                ->orWhere('is_general', true)->get()
                ->where('is_active', true);

            return $query;
        }
    }


    public function show($user)
    {
        return Checklist::findOrFail($user);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'deskripsi' => 'required|string',
            'tipe' => 'required|string',
            'is_general' => 'required|boolean'
        ]);

        $data = Checklist::create([
            'deskripsi' => $request->deskripsi,
            'tipe' => $request->tipe,
            'value' => $request->value,
            'is_general' => $request->is_general,
            'is_photo' => $request->is_photo,
            'customer_id' => $request->customer ? $request->customer['id'] : null,
        ]);

        $this->logCreatedActivity($data, Auth::user()->name . ' create a Checklist', $request->toArray());

        return $data;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'deskripsi' => 'required|string',
            'tipe' => 'required|string',
            'is_general' => 'required|boolean',
            'is_active' => 'required|boolean'
        ]);

        $data = Checklist::find($request->id);
        $beforeUpdateValues = $data->toArray();

        $data->deskripsi = $request->deskripsi;
        $data->tipe = $request->tipe;
        $data->value = $request->value;
        $data->is_active = $request->is_active;
        $data->is_general = $request->is_general;
        $data->is_photo = $request->is_photo;
        $data->customer_id = $request->customer ? $request->customer['id'] : null;
        $data->save();
        $afterUpdateValues = $data->getChanges();
        $this->logUpdatedActivity($data, $beforeUpdateValues, $afterUpdateValues);

        return $data;
    }

    public function updateStatus(Request $request)
    {
        $this->validate($request, [
            'is_active' => 'required|boolean'
        ]);

        $data = Checklist::find($request->id);
        $beforeUpdateValues = $data->toArray();

        $data->is_active = $request->is_active;

        $data->save();
        $afterUpdateValues = $data->getChanges();
        $this->logUpdatedActivity($data, $beforeUpdateValues, $afterUpdateValues);

        return $data;
    }

    public function destroy($data)
    {
        $checklist = Checklist::find($data);
        $checklist->is_active = false;
        $checklist->save();
        $this->logDeletedActivity($checklist, 'Checklist ' . $checklist->name);
        return $checklist;
    }

    public function recover($data)
    {
        $checklist = Checklist::find($data);
        $checklist->is_active = true;
        $checklist->save();
        $this->logRecoveredActivity($checklist, 'Checklist ' . $checklist->name);
        return $checklist;
    }

    public function count()
    {
        return Checklist::count();
    }


    public function export()
    {
        //return Excel::download(new ChecklistsExport, 'file_area.xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();


        $file->move('storage/file_import', $nama_file);


        Excel::import(new ChecklistsImport, public_path('/storage/file_import/' . $nama_file));

        return redirect('/master/checklists');
    }
}
