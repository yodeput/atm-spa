<?php

namespace App\Http\Controllers\Master;

use App\Imports\CustomersImport;
use App\Models\Master\Customer;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

use Avatar;
use Maatwebsite\Excel\Facades\Excel;


class CustomersController extends Controller
{
    use ActivityTraits;

    public function filter(Request $request)
    {
        $query = Customer::query()->withTrashed();

        if ($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('code', 'LIKE', '%'.$request->search.'%');
        }

        $customer = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
            ->paginate($request->input('pagination.per_page'));

        foreach ($customer as $attribute) {
            $attribute['total_atm']=$attribute->getTotalAtmAttribute();
        }
        return $customer;
    }

    public function getDeleted()
    {
        return Customer::onlyTrashed()->get();

    }

    public function restore($id)
    {
        $customer = Customer::onlyTrashed()->where('id', $id);
        $customer->restore();
        return $customer;
    }

    public function all()
    {
        return Customer::orderBy('name')->get();
    }

    public function show($customer)
    {
        return Customer::findOrFail($customer);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Customer::create([
            'code' => $request->code,
            'name' => $request->name
        ]);
        $this->logCreatedActivity($customer, Auth::user()->name.' create a Customer', $request->toArray());

        return $customer;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Customer::find($request->id);
        $beforeUpdateValues = $customer->toArray();

        if ($customer->name != $request->name) {
            $customer->name = $request->name;
        }
        if ($customer->code != $request->code) {
            $customer->code = $request->code;
        }

        $customer->save();
        $afterUpdateValues = $customer->getChanges();
        $this->logUpdatedActivity($customer, $beforeUpdateValues, $afterUpdateValues);

        return $customer;
    }

    public function destroy($customer)
    {
        $data = Customer::find($customer);
        $this-> logDeletedActivity($data, 'Customer '.$data->name);
        return Customer::destroy($customer);
    }

    public function recover($customer)
    {
        $data = Customer::withTrashed()->find($customer);
        $data->restore();
        $this-> logRecoveredActivity($data, 'Customer '.$data->name);
        return $data;
    }

    public function count()
    {
        return Customer::count();
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand() . $file->getClientOriginalName();


        $file->move('storage/file_import', $nama_file);


        Excel::import(new CustomersImport(), public_path('/storage/file_import/' . $nama_file));

        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }
}
