<?php

namespace App\Http\Controllers\Master;

use App\Exports\RegionsExport;
use App\Imports\RegionsImport;
use App\Models\Master\Location;
use App\Models\Master\Region;
use App\Traits\ActivityTraits;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Avatar;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;


class RegionsController extends Controller
{
    use ActivityTraits;

    public function filter (Request $request)
    {
        $query = Region::query()->withTrashed();

        if($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%')
                ->orWhere('code', 'LIKE', '%'.$request->search.'%');
        }

        $regions = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
                    ->paginate($request->input('pagination.per_page'));


        /*foreach ($regions as $attribute) {
            $id  = $attribute->id;
            $queryUser = User::query()
                ->where('region_id', 'LIKE', '%' .$id. '%')
                ->get();

            $queryLoc = Location::query()
                ->where('region_id', 'LIKE', '%' .$id. '%')
                ->get();

            $attribute['users']=$queryUser;
            $attribute['locations']=$queryLoc;
        }*/


        return $regions;
    }

    public function all()
    {
        return Region::orderBy('name')->get();
    }

    public function byServiceBase(Request $request)
    {
        return Region::orderBy('name')->where('name', 'LIKE', '%'.$request->search.'%')->pluck('name');
    }


    public function show ($user)
    {
        return Region::findOrFail($user);
    }

    public function store (Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Region::create([
            'code' => $request->code,
            'name' => $request->name
        ]);
        $this->logCreatedActivity($customer, Auth::user()->name.' create a Region', $request->toArray());


        return $customer;
    }

    public function update (Request $request)
    {
        $this->validate($request, [
            'code' => 'required|string',
            'name' => 'required|string'
        ]);

        $customer = Region::find($request->id);
        $beforeUpdateValues = $customer->toArray();

        if ($customer->name != $request->name) {
            $customer->name = $request->name;
        }
        if ($customer->code != $request->code) {
            $customer->code = $request->code;
        }

        $customer->save();
        $afterUpdateValues = $customer->getChanges();
        $this->logUpdatedActivity($customer, $beforeUpdateValues, $afterUpdateValues);
        return $customer;
    }

    public function destroy ($customer)
    {
        $data = Region::find($customer);
        $this->logDeletedActivity($data, 'Region ' . $data->name);
        return Region::destroy($customer);
    }

    public function recover($location)
    {
        $data = Region::withTrashed()->find($location);
        $data->restore();
        $this->logRecoveredActivity($data, 'Region ' . $data->name);
        return $data;
    }

    public function count ()
    {
        return Region::count();
    }


    public function exportData()
    {
        try {
        $now = date('m-d-Y-His');
        //return (new RegionsExport)->download('nmapp_region_data_'.$now.'.xlsx', \Maatwebsite\Excel\Excel::XLSX,  ['Filename' => 'nmapp_region_data_'.$now.'.xlsx']);
        Excel::store(new RegionsExport, 'export/nmapp_region_data_'.$now.'.xlsx','public');
            return response()->json([
                'error' => false,
                'msg' => 'export/nmapp_region_data_'.$now.'.xlsx',
            ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);

         } catch (\Exception $e) {
            return response()->json([
                'error' => false,
                'exc' =>$e->getMessage(),
                'msg' => "Delete WO gagal",
            ])->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        $file = $request->file('file');

        $nama_file = rand().$file->getClientOriginalName();


        $file->move('storage/file_import',$nama_file);


        Excel::import(new RegionsImport, public_path('/storage/file_import/'.$nama_file));

        return response()->json([
            'error' => false,
            'msg' => "Import Sukses",
        ])->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
    }



}
