<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Task\Task;
use App\Models\Task\TaskUser;
use App\Models\TT\TroubleTicket;
use App\Models\Visit\OpenWo;
use App\Models\Visit\Visit;
use App\Traits\CustomResponse;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use function Psy\debug;

class NotificationController extends Controller {

    use CustomResponse;

    public function notification()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        $notifArray = [];

        $queryVisit = Visit::query();
        switch ($role) {
            case 'engineer':
                $queryVisit->where('user_id', $user->id);
                break;
            case 'vendor':
                $queryVisit->whereHas('user', function ($query) use ($user) {
                    $query->where('vendor_id', $user->vendor_id);
                });
            case 'customer':
                $queryVisit->whereHas('location', function ($query) use ($user) {
                    $query->where('customer_id', $user->customer_id);
                });
                break;
        }

        $countVisit = $queryVisit->whereNull('photo_ar')->count();
        if($countVisit !== 0) {
            $notifArray[] = (object)[
                'ref' => 't_visit',
                'ref_id' => 0,
                'count' => $countVisit,
                'message' => 'Open Visit',
                'category' => 'blue'
            ];
        }

        if($role !== 'customer'){
            $queryOpenWo = OpenWo::query();
            $queryOpenTT = TroubleTicket::query();
            switch ($role) {
                default:
                    $dataOpenWo = $queryOpenWo->where('status', '!=', 'Close')->count();
                    $dataOpenTT = $queryOpenTT->where('status', '!=', 'Close')->count();
                    break;
                case 'engineer':
                    $dataOpenWo = $queryOpenWo->where('user_id', $user->id)->where('status', '!=', 'Close')->count();
                    $dataOpenTT = $queryOpenTT->where('status', '!=', 'Close')->whereHas('visit', function ($query) use ($user) {
                        $query->where('user_id',  $user->id);
                    })->count();
                    break;
            }

            if($dataOpenWo !== 0){
                $notifArray[] = (object)[
                    'ref' => 't_open_wo',
                    'ref_id' => 0,
                    'count' => $dataOpenWo,
                    'message' => 'Outstanding WO',
                    'category' => 'red'
                ];
            }

            if($dataOpenTT !== 0){
                $notifArray[] = (object)[
                    'ref' => 't_troubleticket',
                    'ref_id' => 0,
                    'count' => $dataOpenTT,
                    'message' => 'Troubleticket Open',
                    'category' => 'red'
                ];
            }

            $taskUser = Task::query()->get();
            if(count($taskUser) > 0) {
                foreach ($taskUser as $task){
                    if($task['open'] > 0){
                        $notifArray[] = (object)[
                            'ref' => 't_task',
                            'ref_id' => $task['id'],
                            'count' => $task['open'],
                            'message' => 'Task: ' . $task['description'],
                            'category' => 'orange'
                        ];
                    }
                }

            }
        } else {
            $queryOpenTT = TroubleTicket::query();
            $dataOpenTT = $queryOpenTT->where('status', '!=', 'Close')->where('user_id', $user->id)->count();
            if($dataOpenTT !== 0){
                $notifArray[] = (object)[
                    'ref' => 't_troubleticket',
                    'ref_id' => 0,
                    'count' => $dataOpenTT,
                    'message' => 'Troubleticket Open',
                    'category' => 'red'
                ];
            }
        }

        return $this->HttpOkWithData(false, 'Ok', $notifArray);
    }

}
