<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VisitExportMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $filename;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($filename, $user)
    {
        $this->filename = $filename;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Data Visit berhasil diexport')
            ->from('admin@nmapp.id')
            ->view('visit-email')
            ->with(
                [
                    'nama' => $this->user->name,
                    'filePath' => config('app.url').'/storage/'.$this->filename,
                ]);
    }
}
