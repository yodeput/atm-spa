<?php

namespace App\Jobs;

use App\Http\Helpers\UploadGoogle;
use App\Models\Visit\Visit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SnUploadToGoogle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dataFile;
    protected $fileName;
    protected $workorder;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataFile, $fileName, $workorder)
    {
        $this->dataFile = $dataFile;
        $this->fileName = $fileName;
        $this->workorder = $workorder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $visit = Visit::query()->where('work_order', $this->workorder)->first();
        $google = new UploadGoogle();
        $data = base64_decode($this->dataFile);
        $photo = $google->uploadVisit($data, $this->fileName, $this->workorder);
        $visit->photo_sn = $photo;
        $visit->save();
    }
}
