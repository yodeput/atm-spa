<?php

namespace App\Jobs;

use App\Http\Helpers\UploadGoogle;
use App\Models\Master\Location;
use App\Models\Visit\Visit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AtmUploadToGoogle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dataFile;
    protected $fileName;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataFile, $fileName, $id)
    {
        $this->dataFile = $dataFile;
        $this->fileName = $fileName;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $location =  Location::withTrashed()->findOrFail($this->id);
        $data = base64_decode($this->dataFile);
        $google = new UploadGoogle();
        $photo = $google->uploadAtm($data,  $this->fileName);
        $location->image = $photo;
        $location->save();
    }
}
