<?php

namespace App\Jobs;

use App\Http\Helpers\UploadGoogle;
use App\Models\Visit\Sparepart;
use App\Models\Visit\Visit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PartUploadToGoogle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dataFile;
    protected $fileName;
    protected $workorder;
    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dataFile, $fileName, $workorder, $id)
    {
        $this->dataFile = $dataFile;
        $this->fileName = $fileName;
        $this->workorder = $workorder;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $part = Sparepart::findOrFail($this->id);
        $google = new UploadGoogle();
        $data = base64_decode($this->dataFile);
        $photo = $google->uploadVisit($data, $this->fileName, $this->workorder);
        $part->photo = $photo;
        $part->save();
    }
}
