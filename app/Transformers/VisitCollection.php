<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class VisitCollection extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'workorder' => $this->work_order,
            'sn' => $this->location->serial_number,
            'mc_id' => $this->location->mc_id,
            'customer' => $this->location->customer['name'],
            'model' => $this->location->model,
            'type' => $this->tipe_kunjungan,
            'problem' => $this->detail_kunjungan,
            'ce' => $this->user->ce_code,
            'name' => $this->location->name,
            'servicebase' => $this->location->servicebase_name,
            'region' => $this->location->region_name,
            'pengelola' => $this->pengelola,
            'pic' => $this->pengelola_pic,
            'phone' => $this->pengelola_phone,
            'input' => $this->created_at,
            'checkin' => $this->checkin_time,
            'checkout' => $this->checkout_time,
            'Selfie' => $this->photo,
            'SN' => $this->photo_sn,
            'AR' => $this->photo_ar,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'address' => $this->location->address,
            'city' => $this->location->city,
            'postal' => $this->location->postal,
            'checklists' => $this->checklists,
            'start_onsite' => $this->checkin_on_site,
         ];
    }
}
