<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Malhal\Geographical\Geographical;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class PicLocation extends Model
{
    use Notifiable;
    use Userstamps;
    use Geographical;

    protected $table = 't_pic_location';
    protected $with = ['user'];

    protected $fillable = [
        'detail',
        'longitude',
        'latitude'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
