<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $table = 'm_checklist';
    protected $with = ['customer'];

    protected $fillable = [
        'deskripsi',
        'tipe',
        'value',
        'is_general',
        'is_photo',
        'customer_id'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Master\Customer');
    }
}
