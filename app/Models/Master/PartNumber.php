<?php

namespace App\Models\Master;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class PartNumber extends Model
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Timestamp;
    use Userstamps;

    protected $table = 'm_part_number';
    protected $fillable = [
        'type', 'part_number', 'description'
    ];


}
