<?php

namespace App\Models\Master;

use App\Models\Visit\Visit;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Malhal\Geographical\Geographical;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class Location extends Model
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Userstamps;
    use Timestamp;
    use Geographical;

    protected static $kilometers = true;

    protected $table = 'm_location';
    protected $with = ['customer', 'region', 'service_base'];

    protected $fillable = [
        'mc_id',
        'serial_number',
        'customer_id',
        'address',
        'city',
        'postal',
        'service_base_id',
        'region_id',
        'longitude',
        'latitude',
        'model',
        'install_date',

        'site_number',
        'location_name',
        'status',
        'image'
        ];

    protected $appends = [
        'image_url','name', 'customer_name', 'servicebase_name', 'region_name', 'photo_sn'
    ];

    public function getImageUrlAttribute()
    {
        return Storage::url('atm/' . $this->id . '/' . $this->image);
    }

    public function getCustomerNameAttribute()
    {
        $data = $this->customer()->first();
        return $data->name ?: '-';
    }

    public function getServicebaseNameAttribute()
    {
        $data = $this->service_base()->first();
        return $data ? $data->name ?: '-' : '-';
    }

    public function getRegionNameAttribute()
    {
        $data = $this->service_base()->first();
        $region = $data ? $data['region'] : null ;
        return $region ? $data['region']['code'] : '-';
    }

    public function getNameAttribute()
    {
        $name = $this->location_name ? $this->location_name :  $this->serial_number;
        return $this->mc_id . ' | ' . $name;
    }

    public function getPhotoSnAttribute()
    {
        $data = $this->visit()->orderBy('created_at', 'desc')->first();
        return $data->photo_sn ?? null;
    }

    public function visit(){
        return $this->hasMany(Visit::class,'location_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class)->select(array('id', 'code', 'name'));
    }

    public function region()
    {
        return $this->belongsTo(Region::class)->select(array('id', 'code', 'name'));
    }

    public function service_base()
    {
        return $this->belongsTo(ServiceBase::class)->select(array('id', 'code', 'name','region_id'));
    }
}
