<?php

namespace App\Models\Master;

use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class ServiceBase extends Model
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Timestamp;
    use Userstamps;

    protected $table = 'm_servicebase';
    protected $with = ['region'];
    protected $fillable = [
        'code', 'name', 'region_id'
    ];


    public function region()
    {
        return $this->belongsTo(Region::class)->select(array('id', 'code', 'name'));
    }

}
