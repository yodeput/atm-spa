<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class Customer extends Model
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Userstamps;

    protected $table = 'm_customer';
    protected $fillable = [
        'code', 'name'
    ];

    public function getTotalAtmAttribute()
    {
        return $this->hasMany(Location::class)->count();
    }

}
