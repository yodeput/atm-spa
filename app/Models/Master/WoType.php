<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use Carbon\Traits\Timestamp;

class WoType extends Model
{
    use Timestamp;
    use Userstamps;

    protected $table = 'm_wo_type';

    protected $fillable = [
        'name', 'description'
    ];
}
