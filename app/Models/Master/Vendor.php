<?php

namespace App\Models\Master;

use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class Vendor extends Model
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
    use Timestamp;
    use Userstamps;

    protected $table = 'm_vendor';
    protected $fillable = [
        'code', 'name'
    ];

    public function getTotalUserAttribute()
    {
        return $this->hasMany(User::class)->count();
    }

}
