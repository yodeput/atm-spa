<?php

namespace App\Models\TT;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class TTMessage extends Model
{
    use Userstamps;

    protected $table = 't_tt_message';

    protected $fillable = [
        'message',
        'trouble_ticket_id',
        'sender_id',
    ];

    protected $appends = [
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tt()
    {
        return $this->belongsTo(TroubleTicket::class);
    }

    public function getStatusAttribute()
    {
        return $this->statusMessage()->where('user_id', $this->user_id)->where('tt_message_id', $this->id)->first();
    }

    public function statusMessage()
    {
        return $this->hasMany(TTMessageStatus::class);
    }

}
