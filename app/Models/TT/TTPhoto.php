<?php

namespace App\Models\TT;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class TTPhoto extends Model
{
    use Userstamps;

    protected $table = 't_tt_photo';

    protected $fillable = [
        'description',
        'photo',
        'trouble_ticket_id',
    ];

    public function tt()
    {
        return $this->belongsTo(TroubleTicket::class);
    }



}
