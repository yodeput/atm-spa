<?php

namespace App\Models\TT;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class TTMessageStatus extends Model
{
    use Userstamps;

    protected $table = 't_tt_message_status';
    protected $fillable = [
        'is_read',
        'is_deleted',
        'tt_message_id',
        'user_id',
    ];


}
