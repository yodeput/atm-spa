<?php

namespace App\Models\TT;

use App\Models\Visit\Visit;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;
use App\User;
use App\Models\Master\Location;

class TroubleTicket extends Model
{
    use Userstamps;

    protected $table = 't_troubleticket';
    protected $with = ['user', 'location', 'photo' ];
    protected $fillable = [
        'ticket_number',
        'problem_reported',
        'user_id',
        'visit_id',
        'location_id',
    ];

    /*protected $appends = [
        'unread_message_count'
    ];*/

   /* public function getUnreadMessageCountAttribute(): int
    {
        return $this->messageStatus()->where('user_id', '!=', $this->user_id)->where('is_read', false)->count();
    }*/

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function visit()
    {
        return $this->belongsTo(Visit::class);
    }

    public function photo()
    {
        return $this->hasMany(TTPhoto::class);
    }

    public function message()
    {
        return $this->hasMany(TTMessage::class);
    }

    public function messageStatus()
    {
        return $this->hasManyThrough(TTMessageStatus::class, TTMessage::class);
    }
}
