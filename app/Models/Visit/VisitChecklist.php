<?php

namespace App\Models\Visit;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

class VisitChecklist extends Model
{
    use Timestamp;
    protected $table = 't_visit_checklist';

    protected $fillable = [
        'visit_id',
        'deskripsi',
        'tipe',
        'value',
        'option',
        'photo',
        'is_photo'
    ];

    public function visit(){
        return $this->belongsTo(Visit::class);
    }
}
