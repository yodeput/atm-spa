<?php

namespace App\Models\Visit\history;

use App\Models\Visit\Sparepart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class VisitHistory extends Model
{
    use Notifiable;
    use Userstamps;

    protected $table = 't_visit_history';
    protected $with = ['user', 'location'];
    protected $fillable = [
        'work_order',
        'user_id',
        'location_id',
        'tipe_kunjungan',
        'detail_kunjungan',
        'pengelola',
        'pengelola_pic',
        'pengelola_phone',
        'longitude',
        'latitude',
        'photo',
        'checkin_time',
        'checkout_time',
        'photo_sn',
        'photo_ar',
    ];


    public function checklists()
    {
        return $this->hasMany(VisitChecklistHistory::class);
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Master\Location');
    }

}
