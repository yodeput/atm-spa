<?php

namespace App\Models\Visit\history;

use Illuminate\Database\Eloquent\Model;

class VisitChecklistHistory extends Model
{

    protected $table = 't_visit_checklist_history';

    protected $fillable = [
        'visit_history_id',
        'deskripsi',
        'tipe',
        'value',
        'option',
        'photo',
        'is_photo'
    ];

    public function visit(){
        return $this->belongsTo(VisitHistory::class);
    }
}
