<?php

namespace App\Models\Visit;

use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Wildside\Userstamps\Userstamps;

class OpenWo extends Model
{
    use Notifiable;
    use Userstamps;
    use Timestamp;

    protected $table = 't_open_wo';
    protected $with = ['user'];

    protected $fillable = [
        'work_order',
        'user_id',
        'close_time',
        'status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->select(array(
            'id',
            'name',
            'username',
            'email',
            'ce_code',
            'qlid',
            'avatar',
        ));
    }
}
