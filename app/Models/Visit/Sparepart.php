<?php

namespace App\Models\Visit;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Sparepart extends Model
{
    use Userstamps;
    use Timestamp;
    protected $table = 't_sparepart';
    protected $with = ['visit'];
    protected $fillable = [
        'visit_id',
        'part_number',
        'description',
        'type',
        'category',
        'qty',
        'serial_in',
        'serial_out',
        'photo',
        'problem',
    ];

    protected $appends = [
        'part_number_data'
    ];

    public function getPartNumberDataAttribute()
    {
        return $this->part_number;
    }

    public function visit(){
        return $this->belongsTo(Visit::class);
    }
}
