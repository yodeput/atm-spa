<?php

namespace App\Models\Visit;

use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;

class VisitPhoto extends Model
{
    use Timestamp;
    protected $table = 't_visit_photo';

    protected $fillable = [
        'visit_id',
        'deskripsi',
        'tipe',
        'value'
    ];

    public function visit(){
        return $this->belongsTo(Visit::class);
    }
}
