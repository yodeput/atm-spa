<?php

namespace App\Models\Visit;

use App\Models\Master\Location;
use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;

class Visit extends Model
{
    use Notifiable;
    use SoftDeletes;
    use Userstamps;
    use Timestamp;

    protected $table = 't_visit';
    protected $with = ['user', 'location'];
    protected $fillable = [
        'work_order',
        'user_id',
        'location_id',
        'tipe_kunjungan',
        'detail_kunjungan',
        'pengelola',
        'pengelola_pic',
        'pengelola_phone',
        'longitude',
        'latitude',
        'photo',
        'checkin_time',
        'checkin_on_site'
    ];

    protected $casts = [
        'checkin_on_site' => 'boolean',
    ];

    public function checklists()
    {
        return $this->hasMany(VisitChecklist::class)->select([
                'id',
                'deskripsi',
                'value',
                'photo',
                'visit_id',
                'tipe',
                'option',
                'is_photo'
            ]);
    }

    public function spareparts()
    {
        return $this->hasMany(Sparepart::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select(array(
            'id',
            'name',
            'username',
            'email',
            'ce_code',
            'qlid',
            'avatar',
        ));
    }

    public function location()
    {
        return $this->belongsTo(Location::class)->withTrashed()
            ->select(array(
                'id',
                'mc_id',
                'serial_number',
                'customer_id',
                'model',
                'location_name',
                'region_id',
                'service_base_id',
                'longitude',
                'latitude',
                'city',
                'address',
                'postal',
            ));
    }

}
