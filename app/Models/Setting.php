<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Facades\Storage;

class Setting extends Model
{
    use Notifiable;

    protected $table = 'settings';

    protected $fillable = [
        'name', 'slug_name', 'value'
    ];

    protected $appends = [
        'image_url',
    ];

    public function getImageUrlAttribute()
    {
        if ($this->slug_name == 'app_logo')
            return Storage::url('settings/'. $this->value);
    }

}
