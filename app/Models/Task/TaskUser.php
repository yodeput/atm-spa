<?php

namespace App\Models\Task;

use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class TaskUser extends Model
{
    use Userstamps;
    use Timestamp;
    protected $table = 't_task_user';

    protected $with = ['user','taskDetail'];

    protected $fillable = [
        'task_id',
        'user_id',
    ];

    protected $appends = [
        'total', 'open', 'progress', 'close'
    ];

    public function getTotalAttribute()
    {
        return $this->taskDetail()->count();
    }

    public function getOpenAttribute()
    {
        return $this->taskDetail()->whereNull('close_date')->whereNull('visit_id')->count();
    }

    public function getProgressAttribute()
    {
        return $this->taskDetail()->whereNotNull('visit_id')->whereNull('close_date')->count();
    }

    public function getCloseAttribute()
    {
        return $this->taskDetail()->whereNotNull('close_date')->whereNotNull('visit_id')->count();
    }

    public function taskDetail()
    {
        return $this->hasMany(TaskDetail::class)->orderBy('close_date','asc')->orderBy('visit_id','asc');
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select(array(
            'id',
            'name',
            'username',
            'email',
            'ce_code',
            'qlid',
            'avatar',
            'region_id',
            'service_base_id',
        ));
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function delete()
    {

        $this->taskDetail()->delete();
        return parent::delete();
    }

}
