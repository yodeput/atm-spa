<?php

namespace App\Models\Task;

use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Wildside\Userstamps\Userstamps;

class Task extends Model
{
    use Userstamps;
    use Timestamp;

    protected $table = 't_task';

    protected $fillable = [
        'description',
        'start_date',
        'end_date',
    ];

    protected $appends = [
        'total', 'open', 'progress', 'close'
    ];

    public function getTotalAttribute()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($role === 'engineer' || $role === 'vendor') {
            $tu = $this->taskUser()->where('user_id', $user->id)->first();
            if ($tu) {
                return $this->taskUserDetail()->where('task_user_id', $tu->id)->count();
            } else {
                return 0;
            }
        }
        return $this->taskUserDetail()->count();
    }

    public function getOpenAttribute()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($role === 'engineer' || $role === 'vendor') {
            $tu = $this->taskUser()->where('user_id', $user->id)->first();
            if ($tu) {
                return $this->taskUserDetail()->whereNull('close_date')->where('task_user_id', $tu->id)->count();
            } else {
                return 0;
            }
        }
        return $this->taskUserDetail()->whereNull('close_date')->count();
    }

    public function getProgressAttribute()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($role === 'engineer' || $role === 'vendor') {
            $tu = $this->taskUser()->where('user_id', $user->id)->first();
            if ($tu) {
                return $this->taskUserDetail()->whereNotNull('visit_id')->whereNull('close_date')->where('task_user_id', $tu->id)->count();
            } else {
                return 0;
            }
        }
        return $this->taskUserDetail()->whereNotNull('visit_id')->whereNull('close_date')->count();
    }

    public function getCloseAttribute()
    {
        $user = User::find(Auth::id());
        $roles = Auth::user()->roles()->get()->pluck('name');
        $role = $roles[0];

        if ($role === 'engineer' || $role === 'vendor') {
            $tu = $this->taskUser()->where('user_id', $user->id)->first();
            if ($tu) {
                return $this->taskUserDetail()->whereNotNull('visit_id')->whereNotNull('close_date')->where('task_user_id', $tu->id)->count();
            } else {
                return 0;
            }
        }
        return $this->taskUserDetail()->whereNotNull('visit_id')->whereNotNull('close_date')->count();
    }

    public function taskUser()
    {
        return $this->hasMany(TaskUser::class);
    }

    public function taskUserDetail()
    {
        return $this->hasManyThrough(TaskDetail::class, TaskUser::class);
    }

    public function delete()
    {
        $this->taskUser()->delete();
        return parent::delete();
    }
}
