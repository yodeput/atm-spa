<?php

namespace App\Models\Task;

use App\Models\Master\Location;
use App\Models\Visit\Visit;
use App\User;
use Carbon\Traits\Timestamp;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class TaskDetail extends Model
{
    use Userstamps;
    use Timestamp;
    protected $table = 't_task_detail';

    protected $with = ['location','visit','userAction'];

    protected $fillable = [
        'type',
        'status',
        'task_user_id',
        'location_id',
        'visit_id',
        'start_date',
        'close_date',
    ];


    public function location()
    {
        return $this->belongsTo(Location::class)
            ->select(array(
                'id',
                'mc_id',
                'serial_number',
                'customer_id',
                'region_id',
                'service_base_id',
                'model',
                'location_name',
            ));
    }

    public function taskUser()
    {
        return $this->belongsTo(TaskUser::class);
    }

    public function userAction()
    {
        return $this->belongsTo(User::class);
    }

    public function visit()
    {
        return $this->belongsTo(Visit::class)->select(array(
            'id',
            'work_order',
            'user_id',
            'location_id',
            'tipe_kunjungan',
            'checkin_time',
        ));
    }
}
