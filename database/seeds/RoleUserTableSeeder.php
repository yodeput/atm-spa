<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::findByName('administrator');
        $admin->givePermissionTo(Permission::all());

        $engineer = Role::findByName('engineer');
        $engineer->givePermissionTo('read-location');
        $engineer->givePermissionTo('read-open-wo','update-open-wo','delete-open-wo');
        $engineer->givePermissionTo('read-region');
        $engineer->givePermissionTo('read-checklist');
        $engineer->givePermissionTo('read-customer');
        $engineer->givePermissionTo('read-profile', 'update-profile', 'read-profile-password', 'update-profile-password');
        $engineer->givePermissionTo('read-servicebase');
        $engineer->givePermissionTo('read-visit');

        $supervisor = Role::findByName('supervisor');
        $supervisor->givePermissionTo('read-location');
        $supervisor->givePermissionTo('read-region');
        $supervisor->givePermissionTo('read-open-wo','update-open-wo','delete-open-wo');
        $supervisor->givePermissionTo('read-checklist');
        $supervisor->givePermissionTo('read-customer');
        $supervisor->givePermissionTo('read-profile', 'update-profile', 'read-profile-password', 'update-profile-password');
        $supervisor->givePermissionTo('read-servicebase');
        $supervisor->givePermissionTo('read-visit');

        $manager = Role::findByName('manager');
        $manager->givePermissionTo('read-location');
        $manager->givePermissionTo('read-region');
        $manager->givePermissionTo('read-open-wo','update-open-wo','delete-open-wo');
        $manager->givePermissionTo('read-checklist');
        $manager->givePermissionTo('read-customer');
        $manager->givePermissionTo('read-profile', 'update-profile', 'read-profile-password', 'update-profile-password');
        $manager->givePermissionTo('read-servicebase');
        $manager->givePermissionTo('read-visit');

        $supervisor = Role::findByName('supervisor');
        $supervisor->givePermissionTo('read-location');
        $supervisor->givePermissionTo('read-region');
        $supervisor->givePermissionTo('read-checklist');
        $supervisor->givePermissionTo('read-customer');
        $supervisor->givePermissionTo('read-profile', 'update-profile', 'read-profile-password', 'update-profile-password');
        $supervisor->givePermissionTo('read-servicebase');
        $supervisor->givePermissionTo('read-visit');

        $customer = Role::findByName('customer');
        $customer->givePermissionTo('read-location');
        $customer->givePermissionTo('read-region');
    }
}
