<?php

use Illuminate\Database\Seeder;

class AppRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'aplikasi',
            'display_name' => 'Aplikasi',
            'icon' => 'icon-user',
            'active' => false
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'login-web',
                'display_name' => 'Login Web',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'login-mobile',
                'display_name' => 'Login Mobile',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);
    }
}
