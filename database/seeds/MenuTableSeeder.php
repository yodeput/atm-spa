<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'master',
            'display_name' => 'Menu Master',
            'icon' => 'icon-key'
        ]);

        $moduleId2 = DB::table('modules')->insertGetId([
            'name' => 'pengaturan',
            'display_name' => 'Menu Pengaturan',
            'icon' => 'icon-key'
        ]);

        $moduleId3 = DB::table('modules')->insertGetId([
            'name' => 'tutorial',
            'display_name' => 'Menu Tutorial',
            'icon' => 'icon-key'
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'read-master',
                'display_name' => 'Tampilkan',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'read-pengaturan',
                'display_name' => 'Tampilkan',
                'guard_name' => 'api',
                'module_id' => $moduleId2
            ],
            [
                'name' => 'read-tutorial',
                'display_name' => 'Tampilkan',
                'guard_name' => 'api',
                'module_id' => $moduleId3
            ],
        ]);
    }
}
