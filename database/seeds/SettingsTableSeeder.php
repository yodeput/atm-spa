<?php


use App\Models\Setting;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'settings',
            'display_name' => 'Settings',
            'icon' => 'icon-key'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-setting',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-setting',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-setting',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-setting',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);

        $setting = \App\Models\Setting::create([
            'name' => 'Nama Aplikasi',
            'slug_name' => 'app_name',
            'value' => 'Maintenance ATM',
        ]);

        $setting2 = \App\Models\Setting::create([
            'name' => 'Logo Aplikasi',
            'slug_name' => 'app_logo',
            'value' => 'app_logo.png',
        ]);

    }
}
