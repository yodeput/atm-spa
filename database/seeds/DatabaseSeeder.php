<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(DashboardTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(ServicebaseTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(VisitTableSeeder::class);
        $this->call(ChecklistTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SettingsAzureTableSeeder::class);
        $this->call(OpenWoTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(TTTableSeeder::class);
    }
}
