<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class VisitTableSeeder extends Seeder
{
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'visit',
            'display_name' => 'Data Visit',
            'icon' => 'icon-user',
            'active' => false
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-visit',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-visit',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-visit',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-visit',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);
    }
}
