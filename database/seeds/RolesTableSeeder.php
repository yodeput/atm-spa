<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'roles',
            'display_name' => 'Roles',
            'icon' => 'icon-key'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-roles',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-roles',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-roles',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-roles',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);

        $admin = Role::create([
            'name' => 'administrator',
            'display_name' => 'Administrator'
        ]);

        $engineer = Role::create([
            'name' => 'engineer',
            'display_name' => 'Engineer'
        ]);

        $supervisor = Role::create([
            'name' => 'supervisor',
            'display_name' => 'Supervisor'
        ]);

        $manager = Role::create([
            'name' => 'manager',
            'display_name' => 'Manager'
        ]);

        $customer = Role::create([
            'name' => 'customer',
            'display_name' => 'Customer'
        ]);


    }
}
