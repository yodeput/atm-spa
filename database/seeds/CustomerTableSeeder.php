<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'customer',
            'display_name' => 'Master Customer',
            'icon' => 'icon-user',
            'active' => false
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-customer',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-customer',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-customer',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-customer',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);


    }
}
