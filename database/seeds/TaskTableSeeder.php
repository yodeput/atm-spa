<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class TaskTableSeeder extends Seeder
{
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'task',
            'display_name' => 'Task',
            'icon' => 'icon-user',
            'active' => false
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-task',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-task',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-task',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-task',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);
    }
}
