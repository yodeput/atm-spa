<?php


use App\Models\Setting;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SettingsAzureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $setting = \App\Models\Setting::create([
            'name' => 'AZURE_STORAGE_NAME',
            'slug_name' => 'AZURE_STORAGE_NAME',
            'value' => null,
        ]);

        $setting2 = \App\Models\Setting::create([
            'name' => 'AZURE_STORAGE_NAME',
            'slug_name' => 'AZURE_STORAGE_NAME',
            'value' => null,
        ]);

        $setting3 = \App\Models\Setting::create([
            'name' => 'AZURE_STORAGE_CONTAINER',
            'slug_name' => 'AZURE_STORAGE_CONTAINER',
            'value' => null,
        ]);

        $setting4 = \App\Models\Setting::create([
            'name' => 'AZURE_STORAGE_URL',
            'slug_name' => 'AZURE_STORAGE_URL',
            'value' => null,
        ]);

    }
}
