<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'user',
            'display_name' => 'Data User',
            'icon' => 'icon-people'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-user',
                'display_name' => 'Read',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'create-user',
                'display_name' => 'Create',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-user',
                'display_name' => 'Update',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-user',
                'display_name' => 'Delete',
                'guard_name' => 'api',
                'module_id' => $moduleId
            ]
        ]);


        $userAdmin = \App\User::create([
            'username' => 'administrator',
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
            'avatar' => 'avatar.png',
            'is_active' => true,
            'email_verified_at' => now()

        ]);
        // Assign admin role to default user
        $userAdmin->assignRole('administrator');
        // Generate avatar to defautl user
        $avatar = Avatar::create($userAdmin->name)->getImageObject()->encode('png');
        Storage::disk('public')->put('avatars/'.$userAdmin->id.'/avatar.png', (string) $avatar);
        $userAdmin = \App\User::find($userAdmin->id);
        $userAdmin->avatar = '/storage/avatars/'.$userAdmin->id.'/avatar.png';
        $userAdmin->save();

        $userEng = \App\User::create([
            'username' => 'yodeput',
            'name' => 'Yogi Dewansyah',
            'email' => 'yodeput@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'avatar.png',
            'is_active' => true,
            'email_verified_at' => now()

        ]);
        // Assign admin role to default user
        $userEng->assignRole('engineer');
        // Generate avatar to defautl user
        $avatar = Avatar::create($userEng->name)->getImageObject()->encode('png');
        Storage::disk('public')->put('avatars/'.$userEng->id.'/avatar.png', (string) $avatar);
        $userEng = \App\User::find($userEng->id);
        $userEng->avatar = '/storage/avatars/'.$userEng->id.'/avatar.png';
        $userEng->save();
    }
}
