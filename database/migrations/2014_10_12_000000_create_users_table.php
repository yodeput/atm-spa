<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('avatar')->default('avatar.png');
            $table->string('telepon')->nullable();
            $table->string('alamat')->nullable();
            $table->unsignedBigInteger('region_id')->unsigned()->nullable();
            $table->foreign('region_id')
                ->references('id')->on('m_region')
                ->onDelete('cascade');
            $table->unsignedBigInteger('service_base_id')->unsigned()->nullable();
            $table->foreign('service_base_id')
                ->references('id')->on('m_servicebase')
                ->onDelete('cascade');
            $table->unsignedBigInteger('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')
                ->references('id')->on('m_customer')
                ->onDelete('cascade');
            $table->boolean('is_active')->default(FALSE);
            $table->boolean('is_delete')->default(FALSE);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('users');
        Schema::enableForeignKeyConstraints();

    }
}
