<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTVisitHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_visit_history', function (Blueprint $table) {
            $table->id();
            $table->string('work_order')->nullable();
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('location_id')->unsigned()->nullable();
            $table->foreign('location_id')
                ->references('id')->on('m_location')
                ->onDelete('cascade');
            $table->string('tipe_kunjungan')->nullable();
            $table->string('detail_kunjungan')->nullable();
            $table->string('pengelola')->nullable();
            $table->string('pengelola_pic')->nullable();
            $table->string('pengelola_phone')->nullable();
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->dateTime('checkin_time', 0)->nullable()->default(null);
            $table->dateTime('checkout_time', 0)->nullable()->default(null);
            $table->string('photo')->nullable();
            $table->string('photo_sn')->nullable();
            $table->string('photo_ar')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_visit');
    }
}
