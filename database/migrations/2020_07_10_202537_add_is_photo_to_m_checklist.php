<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsPhotoToMChecklist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_checklist', function (Blueprint $table) {
            $table->boolean('is_photo')->default(FALSE);
        });
        Schema::table('t_visit_checklist', function (Blueprint $table) {
            $table->string('photo')->nullable();
            $table->boolean('is_photo')->default(FALSE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_checklist', function (Blueprint $table) {
            //
        });
    }
}
