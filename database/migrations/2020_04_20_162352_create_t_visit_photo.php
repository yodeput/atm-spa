<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTVisitPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_visit_photo', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('visit_id')->unsigned();
            $table->foreign('visit_id')
                ->references('id')
                ->on('t_visit')
                ->onDelete('cascade');
            $table->string('deskripsi');
            $table->string('tipe');
            $table->string('value');
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_visit_photo', function (Blueprint $table) {
            $table->dropForeign(['visit_id']); // drop the foreign key.
            $table->dropColumn('visit_id'); // drop the column
        });
    }
}
