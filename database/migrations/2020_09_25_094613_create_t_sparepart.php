<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTSparepart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_sparepart', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('visit_id')->unsigned();
            $table->foreign('visit_id')
                ->references('id')->on('t_visit')
                ->onDelete('cascade');
            $table->string('type');
            $table->string('description');
            $table->integer('qty');
            $table->string('serial_in');
            $table->string('serial_out');
            $table->string('photo');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sparepart');
    }
}
