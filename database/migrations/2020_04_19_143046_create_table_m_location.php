<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_location', function (Blueprint $table) {
            $table->id();
            $table->string('mc_id');
            $table->string('serial_number');
            $table->unsignedBigInteger('customer_id')->unsigned();
            $table->foreign('customer_id')
                ->references('id')->on('m_customer')
                ->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->string('city')->nullable();
            $table->string('postal')->nullable();
            $table->unsignedBigInteger('region_id')->unsigned()->nullable();
            $table->foreign('region_id')
                ->references('id')->on('m_region')
                ->onDelete('cascade');
            $table->unsignedBigInteger('service_base_id')->unsigned()->nullable();
            $table->foreign('service_base_id')
                ->references('id')->on('m_servicebase')
                ->onDelete('cascade');
            $table->double('longitude')->nullable();
            $table->double('latitude')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_location', function (Blueprint $table) {
            $table->dropForeign(['customer_id']); // drop the foreign key.
            $table->dropColumn('customer_id'); // drop the column
        });
    }
}
