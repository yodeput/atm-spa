<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_task_detail', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->foreignId('task_user_id')->nullable()->references('id')->on('t_task_user')->nullOnDelete();
            $table->foreignId('location_id')->nullable()->references('id')->on('m_location')->nullOnDelete();
            $table->foreignId('visit_id')->nullable()->references('id')->on('t_visit')->nullOnDelete();
            $table->dateTime('start_date', 0)->nullable()->default(null);
            $table->dateTime('close_date', 0)->nullable()->default(null);
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_task_details');
    }
}
