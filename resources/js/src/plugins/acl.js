const Acl = {
  install (Vue, options) {
    Vue.directive('can', {
      bind: function (el, binding, vnode) {
        if (!JSON.parse(localStorage.getItem('permission'))
          .includes(binding.expression
            .replace(/'/g, '')
            .replace(/"/g, ''))) {
          vnode.elm.style.display = 'none'
        }
      }
    })
  }
}

module.exports = Acl
