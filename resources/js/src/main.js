/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import App from './App.vue'

import Vuesax from 'vuesax'
Vue.use(Vuesax)

import VeeValidate from 'vee-validate'
import validationMessages from 'vee-validate/dist/locale/id';
const config = {
    errorBagName: 'errors',
    events: 'input|blur',
    fieldsBagName: 'fields',
    inject: true,
    dictionary: {
        id: validationMessages
    },
    locale: 'id',
    validity: false,
    useConstraintAttrs: true
};

Vue.use(VeeValidate, config);

import {VueHammer} from 'vue2-hammer'
Vue.use(VueHammer)

import VueClip from 'vue-clip'
Vue.use(VueClip)

import VueExpandableImage from 'vue-expandable-image'
Vue.use(VueExpandableImage)

Vue.use(require('vue-moment'));

Vue.use(require('./plugins/acl'));
// Theme Configurations
import '../themeConfig.js'

// Globally Registered Components
import './globalComponents.js'

// Vue Router
import router from './router'

import i18n from './i18n/i18n'

// Vuex Store
import store from './store/store'

import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'

// Google Maps
import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
    load: {
        //key: 'AIzaSyB4DDathvvwuwlwnUu7F4Sow3oU22y5T1Y',
        key: 'AIzaSyC7vwhIs2N_cOmIxQMrMWjWxT_0tltW0Go',
        libraries: 'places'
    }
})
//AIzaSyB4DDathvvwuwlwnUu7F4Sow3oU22y5T1Y
//AIzaSyDsSMja6TYiq9Qi2x7H3KJeUrXXCD4F8e4
//AIzaSyBSggebKukH2xgqQ6Kxk-zhynPyR0qi4jw

import 'prismjs'

require('@assets/css/iconfont.css')

Vue.config.productionTip = false

import IdleVue from "idle-vue";
const eventsHub = new Vue();
Vue.use(IdleVue, {
    eventEmitter: eventsHub,
    store,
    idleTime: 1000*60*5,
    startAtIdle: false
});

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);
import elementLangId from 'element-ui/lib/locale/lang/en';
import elementLocale from 'element-ui/lib/locale';
elementLocale.use(elementLangId);

import BootstrapVue from "bootstrap-vue"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
Vue.use(BootstrapVue)

new Vue({
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app')
