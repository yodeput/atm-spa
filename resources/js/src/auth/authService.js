
import EventEmitter from 'events'
import store from '../store/store.js'

const localStorageKey = 'loggedIn'
const tokenExpiryKey = 'tokenExpiry'
const loginEvent = 'loginEvent'


class AuthService extends EventEmitter {
    token = null
    profile = null
    tokenExpiry = null

    // Starts the user login flow
    login (customState) {
        webAuth.authorize({
            appState: customState
        })
    }


    localLogin (authResult) {
        this.token = authResult.token
        this.profile = authResult.userData

        this.tokenExpiry = new Date(authResult.expires_in * 1000)
        localStorage.setItem(tokenExpiryKey, this.tokenExpiry)
        localStorage.setItem(localStorageKey, 'true')

        store.commit('UPDATE_USER_INFO', {
            name: this.profile.name,
            email: this.profile.email,
            avatar_url: this.profile.avatar,
            username: this.profile.username
        })

        this.emit(loginEvent, {
            loggedIn: true,
            profile: authResult.tokenPayload,
            state: authResult.appState || {}
        })
    }

    renewTokens () {
        // reject can be used as parameter in promise for using reject
        return new Promise((resolve) => {
            if (localStorage.getItem(localStorageKey) !== 'true') {
                // return reject("Not logged in");
            }

            webAuth.checkSession({}, (err, authResult) => {
                if (err) {
                    // reject(err);
                } else {
                    this.localLogin(authResult)
                    resolve(authResult)
                }
            })
        })
    }

    logOut () {
        localStorage.removeItem(localStorageKey)
        localStorage.removeItem(tokenExpiryKey)
        localStorage.removeItem('userInfo')

        this.token = null
        this.tokenExpiry = null
        this.profile = null

        webAuth.logout({
            returnTo: window.location.origin + process.env.BASE_URL
        })

        this.emit(loginEvent, {loggedIn: false})
    }

    isAuthenticated () {
        return (
            new Date(Date.now()) < new Date(localStorage.getItem(tokenExpiryKey))
        )
    }
}

export default new AuthService()
