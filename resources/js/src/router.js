/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  Object Strucutre:
                    path => router path
                    name => router name
                    component(lazy loading) => component to load
                    meta : {
                      rule => which user can have access (ACL)
                      breadcrumb => Add breadcrumb to specific page
                      pageTitle => Display title besides breadcrumb
                    }
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth/authService'

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
};

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: '/',
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [

        {
            path: '/app',
            redirect: '/app/dashboard',
            component: () => import('./layouts/main/Main'),
            children: [
                {
                    path: '/app/dashboard',
                    name: 'admin-dashboard',
                    meta: {
                        authRequired: true,
                       // admin: true,
                        pageTitle: 'Admin Dashboard'
                    },
                    component: () => import('./views/DashboardAdmin')
                }, {
                    path: '/app/peta-lokasi',
                    name: 'admin-peta',
                    component: () => import('./views/map/MapView'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {
                                title: 'Location Mapping',
                                active: true
                            }
                        ],
                        pageTitle: 'Location Mapping'
                    }
                },
                {
                    path: '/app/visit',
                    name: 'admin-visit',
                    component: () => import('./views/visit/VisitList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {title: 'Visit'},
                            {
                                title: 'Log',
                                active: true
                            }
                        ],
                        pageTitle: 'Visit Log'
                    }
                },
                {
                    path: '/app/visit/history',
                    name: 'admin-visit-history',
                    component: () => import('./views/visit/VisitHistory'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {title: 'Visit'},
                            {
                                title: 'History',
                                active: true
                            }
                        ],
                        pageTitle: 'Visit History'
                    }
                },
                {
                    path: '/app/visit/add',
                    name: 'app-visit-add',
                    component: () => import('./views/visit/VisitAdd'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {title: 'Site Visit'},
                            {
                                title: 'Add Data',
                                active: true
                            }
                        ],
                        pageTitle: 'Add Site Visit'
                    }
                },
                {
                    path: '/app/visit/view/:id',
                    name: 'app-visit-view',
                    component: () => import('./views/visit/VisitView'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Detail Site Visit'
                    }
                },
                {
                    path: '/app/visit/ubah/:id',
                    name: 'app-visit-ubah',
                    component: () => import('./views/visit/VisitEditStep'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Ubah',
                                active: true
                            }
                        ],
                        pageTitle: 'Ubah Site Visit'
                    }
                },
                {
                    path: '/app/visit/detail/:id',
                    name: 'admin-visit-detail',
                    component: () => import('./views/visit/VisitEdit2'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {title: 'Site Visit'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Detail Site Visit'
                    }
                },
                {
                    path: '/app/log/visit/revised',
                    name: 'admin-visit-revised',
                    component: () => import('./views/visit/history/VisitHistoryList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Log'},
                            {title: 'Visit'},
                            {
                                title: 'Revised',
                                active: true
                            }
                        ],
                        pageTitle: 'Visit Revised'
                    }
                },
                {
                    path: '/app/visit/history/detail/:id',
                    name: 'admin-visit-history-detail',
                    component: () => import('./views/visit/history/VisitHistoryEdit'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {title: 'History Site Visit'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Detail History Site Visit'
                    }
                },
                {
                    path: '/app/tutorial',
                    name: 'tutorial',
                    meta: {
                        authRequired: true,
                        pageTitle: 'Tutorial'
                    },
                    component: () => import('./views/tutor/KnowledgeBase')
                },
                {
                    path: '/app/tutorial/master',
                    name: 'tutorial-detail-master',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Master Data`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseMaster')
                },
                {
                    path: '/app/tutorial/user',
                    name: 'tutorial-detail-user',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Pengaturan User`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseUser')
                },
                {
                    path: '/app/tutorial/visit',
                    name: 'tutorial-detail-visit',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Site Visit`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseVisit')
                },
                {
                    path: '/app/part',
                    name: 'admin-part',
                    component: () => import('./views/visit/part/PartList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {
                                title: 'Part Usage',
                                active: true
                            }
                        ],
                        pageTitle: 'Part Usage'
                    }
                },
                {
                    path: '/app/outstanding',
                    name: 'admin-outstanding',
                    component: () => import('./views/visit/OpenWoList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {
                                title: 'Outstanding WO',
                                active: true
                            }
                        ],
                        pageTitle: 'Outstanding WO'
                    }
                },
                {
                    path: '/app/task',
                    name: 'admin-task-list',
                    component: () => import('./views/task/TaskList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {
                                title: 'Task',
                                active: true
                            }
                        ],
                        pageTitle: 'Task'
                    }
                },
                {
                    path: '/app/task/add',
                    name: 'admin-task-add',
                    component: () => import('./views/task/TaskAdd'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Task'},
                            {
                                title: 'Add',
                                active: true
                            }
                        ],
                        pageTitle: 'Add Task'
                    }
                },
                {
                    path: '/app/task/detail/:id',
                    name: 'admin-task-detail',
                    component: () => import('./views/task/TaskDetail'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Task'},
                            {
                                title: 'Task Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Task Detail'
                    }
                },
                {
                    path: '/app/troubleticket',
                    name: 'admin-tt-list',
                    component: () => import('./views/tt/TroubleticketList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {
                                title: 'Troubleticket',
                                active: true
                            }
                        ],
                        pageTitle: 'Troubleticket'
                    }
                },
                {
                    path: '/app/troubleticket/add',
                    name: 'admin-tt-add',
                    component: () => import('./views/tt/TTAdd'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Troubleticket'},
                            {
                                title: 'Add Report',
                                active: true
                            }
                        ],
                        pageTitle: 'Add Report'
                    }
                },
                {
                    path: '/app/master/region',
                    name: 'admin-master-region',
                    component: () => import('./views/master/region/RegionList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Regions',
                                active: true
                            }
                        ],
                        pageTitle: 'Regions'
                    }
                },
                {
                    path: '/app/master/parts',
                    name: 'admin-master-parts',
                    component: () => import('./views/master/part/PartList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Parts',
                                active: true
                            }
                        ],
                        pageTitle: 'Parts'
                    }
                },
                {
                    path: '/app/master/vendors',
                    name: 'admin-master-vendors',
                    component: () => import('./views/master/vendor/VendorList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Vendors',
                                active: true
                            }
                        ],
                        pageTitle: 'Vendors'
                    }
                },
                {
                    path: '/app/master/service-base',
                    name: 'admin-master-service-base',
                    component: () => import('./views/master/servicebase/ServicebaseList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Service Bases',
                                active: true
                            }
                        ],
                        pageTitle: 'Service Bases'
                    }
                },
                {
                    path: '/app/master/customer',
                    name: 'admin-master-customer',
                    component: () => import('./views/master/customer/CustomerList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Customers',
                                active: true
                            }
                        ],
                        pageTitle: 'Customers'
                    }
                },
                {
                    path: '/app/master/wo-type',
                    name: 'admin-master-wo-type',
                    component: () => import('./views/master/wo-type/WoTypeList'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'WO Types',
                                active: true
                            }
                        ],
                        pageTitle: 'WO Types'
                    }
                },
                {
                    path: '/app/master/atm',
                    name: 'admin-master-atm',
                    component: () => import('./views/master/atm/AtmList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'ATM Locations',
                                active: true
                            }
                        ],
                        pageTitle: 'ATM Locations'
                    }
                },
                {
                    path: '/app/master/checklist',
                    name: 'admin-master-checklist',
                    component: () => import('./views/master/checklist/ChecklistList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Master'},
                            {
                                title: 'Checklists',
                                active: true
                            }
                        ],
                        pageTitle: 'Checklists'
                    }
                },
                {
                    path: '/app/profile',
                    name: 'admin-user-profile',
                    component: () => import('./views/user-settings/UserSettings.vue'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Profile',
                                active: true
                            }
                        ],
                        pageTitle: 'Profile'
                    }
                },
                {
                    path: '/app/setting/users',
                    name: 'admin-users-management',
                    component: () => import('./views/setting/users/UserList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Users',
                                active: true
                            }
                        ],
                        pageTitle: 'Users Management'
                    }
                },
                {
                    path: '/app/setting/users/edit',
                    name: 'admin-user-edit',
                    component: () => import('./views/setting/users/UserEdit'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {title: 'Users'},
                            {
                                title: 'Edit',
                                active: true
                            }
                        ],
                        pageTitle: 'Edit User'
                    }
                },
                {
                    path: '/app/setting/users/detail',
                    name: 'admin-user-detail',
                    component: () => import('./views/setting/users/UserEdit'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {title: 'Users'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'User Detail'
                    }
                },
                {
                    path: '/app/setting/users/add',
                    name: 'admin-user-add',
                    component: () => import('./views/setting/users/UserAdd'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Users',
                                url: '/app/setting/users'
                            },
                            {
                                title: 'Add',
                                active: true
                            }
                        ],
                        pageTitle: 'Add User'
                    }
                },
                {
                    path: '/app/setting/roles',
                    name: 'admin-roles-management',
                    component: () => import('./views/setting/roles/RoleList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Roles',
                                active: true
                            }
                        ],
                        pageTitle: 'Roles & Permissions'
                    }
                },
                {
                    path: '/app/setting/roles/edit',
                    name: 'admin-role-edit',
                    component: () => import('./views/setting/roles/RoleEdit'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Roles',
                                url: '/app/setting/roles'
                            },
                            {
                                title: 'Edit',
                                active: true
                            }
                        ],
                        pageTitle: 'Edit Role'
                    }
                },
                {
                    path: '/app/setting/roles/add',
                    name: 'admin-role-add',
                    component: () => import('./views/setting/roles/RoleAdd'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Roles',
                                url: '/app/setting/roles'
                            },
                            {
                                title: 'Add',
                                active: true
                            }
                        ],
                        pageTitle: 'Add Role'
                    }
                },
                {
                    path: '/app/log/user',
                    name: 'admin-log-user',
                    component: () => import('./views/setting/logs/LogList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Log'},
                            {
                                title: 'Lokasi User',
                                active: true
                            }
                        ],
                        pageTitle: 'Lokasi User'
                    }
                },
                {
                    path: '/app/log/data',
                    name: 'admin-log-data',
                    component: () => import('./views/setting/logs/LogList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Log'},
                            {
                                title: 'Data',
                                active: true
                            }
                        ],
                        pageTitle: 'Log Aplikasi'
                    }
                },
                {
                    path: '/app/setting/config',
                    name: 'admin-config',
                    component: () => import('./views/setting/ConfigEdit'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Settings'},
                            {
                                title: 'Config',
                                active: true
                            }
                        ],
                        pageTitle: 'Application Config'
                    }
                }
            ]
        },

        /*{
            path: '/app',
            redirect: '/app/dashboard',
            component: () => import('./layouts/main/Main'),
            children: [
                {
                    path: '/app/dashboard',
                    name: 'app-dashboard',
                    meta: {
                        authRequired: true,
                        admin: false,
                        pageTitle: ' Dashboard',
                    },

                    component: () => import('./views/Dashboard')
                },
                {
                    path: '/app/visit',
                    name: 'app-visit',
                    meta: {
                        authRequired: true,
                        admin: false,
                        pageTitle: ' Site Visit',
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {
                                title: 'Site Visit',
                                active: true
                            }
                        ],
                    },
                    component: () => import('./views/visit/VisitList')
                },
                {
                    path: '/app/visit/add',
                    name: 'app-visit-add',
                    component: () => import('./views/visit/VisitAdd'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Add Data',
                                active: true
                            }
                        ],
                        pageTitle: 'Add Site Visit'
                    }
                },
                {
                    path: '/app/visit/detail/:id',
                    name: 'app-visit-detail',
                    component: () => import('./views/visit/VisitEdit2'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Detail Site Visit'
                    }
                },
                {
                    path: '/app/visit/view/:id',
                    name: 'app-visit-view',
                    component: () => import('./views/visit/VisitView'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Detail',
                                active: true
                            }
                        ],
                        pageTitle: 'Detail Site Visit'
                    }
                },
                {
                    path: '/app/visit/ubah/:id',
                    name: 'app-visit-ubah',
                    component: () => import('./views/visit/VisitEditStep'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Site Visit'},
                            {
                                title: 'Ubah',
                                active: true
                            }
                        ],
                        pageTitle: 'Ubah Site Visit'
                    }
                },
                {
                    path: '/app/outstanding',
                    name: 'app-outstanding',
                    component: () => import('./views/visit/OpenWoList'),
                    meta: {
                        authRequired: true,
                       // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {
                                title: 'Outstanding WO',
                                active: true
                            }
                        ],
                        pageTitle: 'Outstanding WO'
                    }
                },
                {
                    path: '/app/profile',
                    name: 'app-user-profile',
                    component: () => import('./views/user-settings/UserSettings.vue'),
                    meta: {
                        authRequired: true,
                        admin: false,
                        breadcrumb: [
                            {title: 'Home'},
                            {title: 'Settings'},
                            {
                                title: 'Profile',
                                active: true
                            }
                        ],
                        pageTitle: 'Profile'
                    }
                }
            ]
        },*/
        /*{
            path: '/app/tutorial',
            redirect: '/tutorial',
            component: () => import('./layouts/main/Main'),
            children: [
                {
                    path: '/tutorial',
                    name: 'tutorial',
                    meta: {
                        authRequired: true,
                        pageTitle: 'Tutorial'
                    },
                    component: () => import('./views/tutor/KnowledgeBase')
                },
                {
                    path: '/tutorial/master',
                    name: 'tutorial-detail-master',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Master Data`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseMaster')
                },
                {
                    path: '/tutorial/user',
                    name: 'tutorial-detail-user',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Pengaturan User`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseUser')
                },
                {
                    path: '/tutorial/visit',
                    name: 'tutorial-detail-visit',
                    meta: {
                        authRequired: true,
                        pageTitle: `Tutorial Site Visit`
                    },
                    component: () => import('./views/tutor/KnowledgeBaseSite Visit')
                },
            ]
        },*/


        // =============================================================================
        // FULL PAGE LAYOUTS
        // =============================================================================
        {
            path: '',
            redirect: '/app',
            component: () => import('./layouts/full-page/FullPage'),
            children: [
                {
                    path: '/app/peta-lokasi/full',
                    name: 'admin-peta-full',
                    component: () => import('./views/map/MapViewFull'),
                    meta: {
                        authRequired: true,
                        // admin: true,
                        breadcrumb: [
                            {title: 'Dashboard'},
                            {title: 'Data'},
                            {
                                title: 'Location Mapping',
                                active: true
                            }
                        ],
                        pageTitle: 'Location Mapping'
                    }
                },
                {
                    path: '/lock',
                    name: 'page-lock',
                    meta: {authRequired: false},
                    component: () => import('./views/auth/LockScreen')
                },
                {
                    path: '/login',
                    name: 'page-login',
                    meta: {authRequired: false},
                    component: () => import('./views/auth/Login')
                },
                {
                    path: '/register',
                    name: 'page-register',
                    meta: {authRequired: false},
                    component: () => import('./views/auth/Register')
                },
                {
                    path: '/forgot-password',
                    name: 'page-forgot-password',
                    meta: {authRequired: false},
                    component: () => import('./views/auth/ForgotPassword')
                },
                {
                    path: '/reset-password',
                    name: 'page-reset-password',
                    meta: {authRequired: false},
                    component: () => import('./views/auth/ResetPassword')
                },
                {
                    path: '/error-404',
                    name: 'page-404',
                    meta: {authRequired: false},
                    component: () => import('./views/pages/Error404')
                }
            ]
        },
        {
            path: '*',
            redirect: '/error-404'
        }
    ]
})

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('con-vs-loading')
    if (appLoading) {
        appLoading.style.display = 'none'
    }
})

router.beforeEach((to, from, next) => {
    const userInfo = localStorage.getItem('userInfo')
    const isAuthenticated = auth.isAuthenticated()
    const authRequired = to.meta.authRequired
  /*  const isAdmin = localStorage.getItem('roles') === 'administrator';
    const isManager = localStorage.getItem('roles') === 'manager';*/

    if (authRequired) {
        if (!isAuthenticated) {
                router.push({
                    path: '/login',
                    query: {to: to.path}
                })
        }
    }
   /* if (to.meta.admin) {
        if (!isAdmin && !isManager) {
            router.push({
                path: '/app'
            })
        }
    }*/

    return next()
})

export default router
