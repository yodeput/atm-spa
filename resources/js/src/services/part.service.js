import axios from '@/http/axios'
import store from "../store/store";

let isAlreadyFetchingAccessToken = false
let subscribers = []

function onAccessTokenFetched (access_token) {
    subscribers = subscribers.filter(callback => callback(access_token))
}

function addSubscriber (callback) {
    subscribers.push(callback)
}

export default {
    init() {
        axios.interceptors.response.use(function (response) {
            return response
        }, function (error) {
            // const { config, response: { status } } = error
            const {config, response} = error
            const originalRequest = config

            // if (status === 401) {
            if (response && response.status === 401) {
                if (!isAlreadyFetchingAccessToken) {
                    isAlreadyFetchingAccessToken = true
                    store.dispatch('auth/fetchAccessToken')
                        .then((access_token) => {
                            isAlreadyFetchingAccessToken = false
                            onAccessTokenFetched(access_token)
                        })
                }

                const retryOriginalRequest = new Promise((resolve) => {
                    addSubscriber(access_token => {
                        originalRequest.headers.Authorization = `Bearer ${access_token}`
                        resolve(axios(originalRequest))
                    })
                })
                return retryOriginalRequest
            }
            return Promise.reject(error)
        })
    },
    get(filter) {
        return axios.post(`/api/parts/filter?page=${filter.pagination.current_page}`, filter);
    },
    getReport(param) {
        return axios.get(`/api/parts/report?${param}`);
    },
    show(data){
        return  axios.get(`/api/parts/${data.id}`)
    },
    create(data){
        return axios.post(`/api/parts/store/`, data);
    },
    edit(data){
        return axios.put(`/api/parts/update/${data.id}`,data)
    },
    delete(data){
        return  axios.delete(`/api/parts/${data.id}`)
    },
    restore(data){
        return   axios.post(`/api/parts/recover/${data.id}`)
    }
}
