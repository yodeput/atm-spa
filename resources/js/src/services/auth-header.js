export default function authHeader () {
    let token = localStorage.getItem('accessToken')

    return {Authorization: 'Bearer ' + token}

}
