import axios from 'axios';
import authHeader from './auth-header';

const API_URL = '/api/users/';

class UserService {
    getAll(filters) {
        return axios.post(`filter?page=${filters.pagination.current_page}`, filters );
    }

    getData(id) {
        return axios.get(API_URL + id);
    }

    save(user) {
        return axios.post(API_URL + 'store', { headers: authHeader()},
            user);
    }

    edit(id) {
        return axios.put(API_URL + 'update/'+ id, { headers: authHeader() });
    }

    delete(id) {
        return axios.delete(API_URL + id, { headers: authHeader() });
    }
}

export default new UserService();
