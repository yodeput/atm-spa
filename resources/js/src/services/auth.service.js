import axios from 'axios';

const API_URL = 'api/auth/';

class AuthService {
    login(user) {
        return axios
            .post(API_URL + 'login', {
                login: user.login,
                password: user.password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem('userInfo', JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {
        axios.post(API_URL + 'logout');
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            username: user.username,
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();
