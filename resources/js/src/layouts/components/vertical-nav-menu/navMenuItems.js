/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
    {
        url: '/app/dashboard',
        name: 'Dashboard',
        roles : ['supervisor','engineer', 'customer'],
        slug: 'dashboard',
        icon: 'HomeIcon'
    },
    {
        url: '/tutorial',
        name: 'Tutorial',
        roles:  ['supervisor','engineer'],
        slug: 'tutorialInfo',
        icon: 'InfoIcon'
    },

    {
        url: '/app/visit',
        name: 'Kunjungan',
        roles : ['supervisor','engineer', 'customer'],
        slug: 'kunjungan',
        icon: 'ListIcon'
    },
    {
        url: '/app/outstanding',
        name: 'Outstanding WO',
        roles : ['supervisor','engineer'],
        slug: 'outstanding',
        icon: 'ListIcon'
    },

]
