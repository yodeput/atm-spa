/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [

    {
        url: '/app/dashboard',
        name: 'Dashboard',
        roles: 'read-visit',
        slug: 'dashboard',
        icon: 'HomeIcon'
    },
    {
        url: '/app/tutorial',
        name: 'Tutorial',
        roles: 'read-turorial',
        slug: 'tutorial',
        icon: 'InfoIcon'
    },
    {
        url: '/app/peta-lokasi',
        name: 'Location Mapping',
        roles: 'read-map',
        slug: 'peta',
        icon: 'MapIcon'
    },
    {
        url: null,
        name: 'Site Visit',
        rolesParent: 'read-visit',
        roles: 'read-visit',
        icon: 'ListIcon',
        isActive: true,
        submenu: [
            {
                url: '/app/visit',
                name: 'Log',
                roles: 'read-visit',
                slug: 'kunjungan',
                icon: 'ListIcon'
            },
            {
                url: '/app/visit/history',
                name: 'History',
                roles: 'read-visit',
                slug: 'kunjungan',
                icon: 'ListIcon'
            },
        ]
    },
    {
        url: '/app/part',
        name: 'Part Usage',
        roles: 'create-visit',
        slug: 'part',
        icon: 'HexagonIcon'
    },
    {
        url: '/app/outstanding',
        name: 'Outstanding WO',
        roles: 'read-open-wo',
        slug: 'outstanding',
        icon: 'BellIcon'
    },

    {
        header: 'Task',
        icon: 'PackageIcon',
        roles: 'read-task',
        i18n: 'Task',
        items: [
            {
                url: '/app/task',
                name: 'Task',
                rolesParent: 'read-task',
                roles: 'read-task',
                slug: 'task',
                icon: 'SquareIcon'
            },
          /*  {
                url: '/app/task-detail',
                name: 'Task Detail',
                rolesParent: 'read-task',
                roles: 'read-task',
                slug: 'task-detail',
                icon: 'XSquareIcon'
            }*/
        ]
    },
    {
        header: 'Troubleticket',
        icon: 'PackageIcon',
        roles: 'read-tt',
        i18n: 'Troubleticket',
        items: [
            {
                url: '/app/troubleticket',
                name: 'TroubleTicket',
                rolesParent: 'read-tt',
                roles: 'read-tt',
                slug: 'tt',
                icon: 'SquareIcon'
            },
        ]
    },
    {
        header: 'Master',
        icon: 'PackageIcon',
        roles: 'read-master',
        i18n: 'Apps',
        items: [
            {
                url: '/app/master/wo-type',
                name: 'WO Types',
                rolesParent: 'read-master',
                roles: 'read-wo-type',
                slug: 'master-wo-type',
                icon: 'SquareIcon'
            },
            {
                url: '/app/master/region',
                name: 'Regions',
                rolesParent: 'read-master',
                roles: 'read-region',
                slug: 'master-region',
                icon: 'SquareIcon'
            },
            {
                url: '/app/master/service-base',
                name: 'Service Bases',
                rolesParent: 'read-master',
                roles: 'read-servicebase',
                slug: 'master-service-base',
                icon: 'XSquareIcon'
            },
            {
                url: '/app/master/atm',
                name: 'ATM Locations',
                rolesParent: 'read-master',
                roles: 'read-location',
                slug: 'master-atm',
                icon: 'MapPinIcon'
            },
            {
                url: '/app/master/checklist',
                name: 'Checklists',
                rolesParent: 'read-master',
                roles: 'read-checklist',
                slug: 'master-checklist',
                icon: 'CheckCircleIcon'
            },
            {
                url: '/app/master/customer',
                name: 'Customers',
                rolesParent: 'read-master',
                roles: 'read-customer',
                slug: 'master-customer',
                icon: 'BriefcaseIcon'
            },
            {
                url: '/app/master/vendors',
                name: 'Vendors',
                rolesParent: 'read-master',
                roles: 'read-vendor',
                slug: 'master-vendor',
                icon: 'BriefcaseIcon'
            },
            {
                url: '/app/master/parts',
                name: 'Part Numbers',
                rolesParent: 'read-master',
                roles: 'read-part_number',
                slug: 'master-part',
                icon: 'HexagonIcon'
            }
        ]
    },

    {
        header: 'Pengaturan',
        icon: 'GearIcon',
        roles: 'read-pengaturan',
        i18n: 'Apps',
        items: [
            {
                url: '/app/setting/config',
                name: 'Aplikasi',
                rolesParent: 'read-pengaturan',
                roles: 'read-setting',
                slug: 'config',
                icon: 'SettingsIcon'
            },
            {
                url: '/app/setting/users',
                name: 'Users',
                rolesParent: 'read-pengaturan',
                roles: ['read-user'],
                slug: 'user',
                icon: 'UserIcon'
            },
            {
                url: '/app/setting/roles',
                name: 'Roles & Permissions',
                rolesParent: 'read-pengaturan',
                roles: 'read-roles',
                slug: 'roles',
                icon: 'KeyIcon'
            },
            {
                url: null,
                name: 'Logs',
                rolesParent: 'read-pengaturan',
                roles: 'read-pengaturan',
                icon: 'HashIcon',
                submenu: [
                    {
                        url: '/app/log/visit/revised',
                        name: 'Visit Revised',
                        roles: 'read-visit-history',
                        slug: 'kunjungan',
                        icon: 'ClockIcon'
                    },
                    {
                        url: '/app/log/user',
                        name: 'Lokasi User',
                        rolesParent: 'read-pengaturan',
                        roles: 'read-log',
                        slug: 'user_logs',
                        icon: 'MapIcon'
                    },
                    {
                        url: '/app/log/data',
                        name: 'Logs',
                        rolesParent: 'read-pengaturan',
                        roles: 'read-log',
                        slug: 'logs',
                        icon: 'HashIcon'
                    }
                ]
            }


        ]
    }

]
