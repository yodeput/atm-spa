/*=========================================================================================
  File Name: actions.js
  Description: Vuex Store - actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
import axios from '@/http/axios/index'

let total = 0;
function getdataListAdmin(commit) {
    axios.get(`/api/outstanding/countPerEnginer`)
        .then(response => {
            let responseData = response.data;
            let notifUser = {
                total: 0,
                notif: []
            }
            notifUser.total += responseData.length
            if (responseData.length > 0) {
                for (let i = 0; i < responseData.length; i++) {
                    total += responseData[i].open
                    let notifItem = {
                        page: 'outstandingWO',
                        isUser: true,
                        count: responseData[i].open,
                        index: i,
                        title: responseData[i].user == null ? '-' : `${responseData[i].user.display_name}`,
                        msg: `Mempunyai Outstanding Workorder`,
                        icon: 'AlertOctagonIcon',
                        time: `${responseData[i].date} 00:00:00`,
                        category: 'danger'
                    }
                    notifUser.notif.push(notifItem);
                }
                commit('UPDATE_NOTIF_USER', notifUser)
            }
        })
}
function getdataListPIC(commit) {
    axios.get(`/api/outstanding/countByEnginer`)
        .then(response => {
            let responseData = response.data;
            let notifUser = {
                total: 0,
                notif: []
            }
            notifUser.total += responseData.length
            if (responseData.length > 0) {
                for (let i = 0; i < responseData.length; i++) {
                    let notifItem = {
                        page: 'outstandingWO',
                        index: i,
                        title: `${responseData[i].open} Oustanding WO`,
                        msg: `Pada tanggal ${responseData[i].date}`,
                        icon: 'AlertOctagonIcon',
                        time: `${responseData[i].date} 00:00:00`,
                        category: 'danger'
                    }
                    notifUser.notif.push(notifItem);
                }
                commit('UPDATE_NOTIF_USER', notifUser)
            }
        })
}
function getdataListSPV(commit) {
    axios.get(`/api/outstanding/countPerRegion`)
        .then(response => {
            let responseData = response.data;
            let notifUser = {
                total: 0,
                notif: []
            }
            notifUser.total += responseData.length
            if ( responseData.length > 0) {
                for (let i = 0; i <  responseData.length; i++) {
                    let notifItem = {
                        page: 'outstandingWO',
                        isUser: true,
                        count:  responseData[i].open,
                        index: i,
                      title: responseData[i].user == null ? '-' : `${responseData[i].user.display_name}`,
                        msg: `Mempunyai Outstanding Workorder`,
                        icon: 'AlertOctagonIcon',
                        time: `${responseData[i].date} 00:00:00`,
                        category: 'danger'
                    }
                    notifUser.notif.push(notifItem);
                }
                commit('UPDATE_NOTIF_USER', notifUser)
            }
        })
}
function getOpenVisit(commit) {
    axios.get(`/api/visits/countOpenPerUser`)
        .then(response => {
            let responseData = response.data;
            let notifUser = {
                total: 0,
                notif: []
            }

            if ( responseData.length > 0) {
                responseData = responseData[0]
                notifUser.total += responseData.visit

                let notifItem = {
                    page: 'kunjungan',
                    isUser: true,
                    count: responseData.visit,
                    index: 0,
                    title: `Data Kunjungan`,
                    msg: `Data masih belum CLOSE`,
                    icon: 'AlertOctagonIcon',
                    time: ``,
                    category: 'danger'
                }
                notifUser.notif.push(notifItem);
            }
            commit('UPDATE_NOTIF_USER', notifUser)
        })
}
function getNotification (commit) {
    axios.get(`/api/notification`)
        .then(response => {
            let responseData = response.data.data;
            let notifUser = {
                total: 0,
                notif: []
            }

            if ( responseData.length > 0) {
                for (let i = 0; i < responseData.length; i++){
                    let data = responseData[i]
                    let notifItem = {
                        index: i,
                        ref: data.ref,
                        refId: data.ref_id,
                        count: data.count,
                        message: data.message,
                        icon: 'AlertOctagonIcon',
                        time: ``,
                        category: data.category
                    }
                    notifUser.notif.push(notifItem);
                }

                notifUser.total = responseData.length

            }
            commit('UPDATE_NOTIF_USER', notifUser)
        }).catch(error => {

    })
}
const actions = {

  // /////////////////////////////////////////////
  // COMPONENTS
  // /////////////////////////////////////////////

  // Vertical NavMenu
  updateVerticalNavMenuWidth ({ commit }, width) {
    commit('UPDATE_VERTICAL_NAV_MENU_WIDTH', width)
  },

  // VxAutoSuggest
  updateStarredPage ({ commit }, payload) {
    commit('UPDATE_STARRED_PAGE', payload)
  },

  // The Navbar
  arrangeStarredPagesLimited ({ commit }, list) {
    commit('ARRANGE_STARRED_PAGES_LIMITED', list)
  },
  arrangeStarredPagesMore ({ commit }, list) {
    commit('ARRANGE_STARRED_PAGES_MORE', list)
  },

  // /////////////////////////////////////////////
  // UI
  // /////////////////////////////////////////////

  toggleContentOverlay ({ commit }) {
    commit('TOGGLE_CONTENT_OVERLAY')
  },
  updateTheme ({ commit }, val) {
    commit('UPDATE_THEME', val)
  },

  updateNotifUser ({ commit }, val) {
    commit('UPDATE_NOTIF_USER', val)
  },

  getNotification({commit}){
      let notifUser = {
          total: 0,
          notif: []
      }
      commit('UPDATE_NOTIF_USER', notifUser)
      getNotification(commit);
  },




  // /////////////////////////////////////////////
  // User/Account
  // /////////////////////////////////////////////

  updateUserInfo ({ commit }, payload) {
    commit('UPDATE_USER_INFO', payload)
  },
  updateUserRole ({ dispatch }, payload) {
    // Change client side
    payload.aclChangeRole(payload.userRole)

    // Make API call to server for changing role

    // Change userInfo in localStorage and store
    dispatch('updateUserInfo', {userRole: payload.userRole})
  }
}

export default actions
