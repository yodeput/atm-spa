/*=========================================================================================
  File Name: moduleAuthActions.js
  Description: Auth Module Actions
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import jwt from '../../http/requests/auth/jwt/index.js'
import router from '../../router'

export default {
    // JWT
    loginJWT ({commit}, payload) {
        return new Promise((resolve, reject) => {
            jwt.login(payload.userDetails.login, payload.userDetails.password)
                .then(response => {

                    // If there's user data in response
                    if (response.data.userData) {
                        // Navigate User to homepage


                        // Set accessToken

                        commit('UPDATE_USER_INFO', response.data.userData, {root: true})
                        commit('SET_BEARER', response.data.token)
                        localStorage.setItem('accessToken', response.data.token)
                        if (response.data.permission.find((e) => e === 'login-web')) {
                            localStorage.setItem('userInfo', JSON.stringify(response.data.userData))
                            localStorage.setItem('accessToken', response.data.token)
                            localStorage.setItem('tokenType', response.data.token_type)
                            localStorage.setItem('tokenExpiry', new Date(response.data.expires_in * 1000))
                            localStorage.setItem('roles', response.data.roles[0])
                            localStorage.setItem('permission', JSON.stringify(response.data.permission))

                            let path = `/app`
                            router.push(router.currentRoute.query.to || path).catch(err => {
                            })


                            resolve(response)

                        } else {
                            reject({data: {msg: 'Anda tidak punya akses, silahkan hubungi Administrator'}})
                            jwt.logout()
                                .then(response => {

                                })
                        }

                    } else {
                        reject({message: 'Username/Email atau Password Salah'})
                    }

                })
                .catch(error => {
                    reject(error)
                })
        })
    },

    logoutJWT ({commit}, payload) {
        return new Promise((resolve, reject) => {
            jwt.logout()
                .then(response => {

                    localStorage.removeItem('accessToken')
                    localStorage.removeItem('userInfo')
                    localStorage.removeItem('tokenExpiry')
                    localStorage.removeItem('tokenType')
                    localStorage.removeItem('roles')
                    router.push('/login' || router.currentRoute.query.from)
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },

    registerUserJWT ({commit}, payload) {

        const {displayName, email, password, confirmPassword} = payload.userDetails

        return new Promise((resolve, reject) => {

            // Check confirm password
            if (password !== confirmPassword) {
                reject({message: 'Password doesn\'t match. Please try again.'})
            }

            jwt.registerUser(displayName, email, password)
                .then(response => {
                    // Redirect User
                    router.push(router.currentRoute.query.to || '/')

                    // Update data in localStorage
                    localStorage.setItem('accessToken', response.data.accessToken)
                    commit('UPDATE_USER_INFO', response.data.userData, {root: true})

                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },

    fetchAccessToken () {
        return new Promise((resolve) => {
            jwt.refreshToken().then(response => {
                resolve(response)
            })
                .catch(error => {
                    localStorage.removeItem('accessToken')
                    localStorage.removeItem('userInfo')
                    router.push(router.currentRoute.query.to || '/login')
                })
        })
    },

    resetPassword (payload) {
        return new Promise((resolve) => {
            jwt.resetPassword(payload.email).then(response => {
                resolve(response)
            })
                .catch(error => {
                    reject(error)
                })
        })
    }


}
