import axios from '@/http/axios'
import service from '@/services/task.service'
export default {
    get ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.get(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    show ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.show(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    create ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.create(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    edit ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.edit(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    delete ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.delete(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    deleteTaskUser ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.deleteTaskUser(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    deleteTaskDetail ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.deleteTaskDetail(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    restore ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.restore(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    getTaskUser ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.getTaskUser(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    getAllTaskUser ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.getAllTaskUser(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
}
