import axios from '@/http/axios'
import service from '@/services/master.region.service'
export default {
    get ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.get(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    create ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.create(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    edit ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.edit(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    delete ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.delete(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    restore ({commit}, payload) {
        return new Promise((resolve, reject) => {
            service.restore(payload)
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
        })
    },
    export ({commit}) {
        return new Promise((resolve, reject) => {
            service.exportData()
              .then(response => {
                  resolve(response)
              })
              .catch(error => {
                  reject(error)
              })
        })
    },
}
