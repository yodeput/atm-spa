// axios
import axios from 'axios'
import router from './router'

axios.interceptors.request.use(
  config => {
      let token = localStorage.getItem('accessToken')
      if (token) {
          config.headers.common["Authorization"] = 'Bearer ' + token;
      }
      return config;
  },
  error => {
      return Promise.reject(error);
  }
);
axios.interceptors.response.use(
  response => {
      if (response.status === 200 || response.status === 201) {
          return Promise.resolve(response);
      } else {
          return Promise.reject(response);
      }
  },
  error => {
      if (error.response.status) {
          switch (error.response.status) {
          case 400:

              //do something
              break;

          case 401:
              alert("session expired");
              break;
          case 403:
              localStorage.removeItem('accessToken')
              localStorage.removeItem('userInfo')
              localStorage.removeItem('tokenExpiry')
              localStorage.removeItem('tokenType')
              localStorage.removeItem('roles')
              router.replace({
                  path: "/login",
                  query: { redirect: router.currentRoute.fullPath }
              });
              break;
          case 404:
              alert('page not exist');
              break;
          case 502:
              setTimeout(() => {
                  router.replace({
                      path: "/login",
                      query: {
                          redirect: router.currentRoute.fullPath
                      }
                  });
              }, 1000);
          }
          return Promise.reject(error.response);
      }
  }
);

export default axios;
