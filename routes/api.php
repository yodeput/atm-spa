<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:api','checksinglesession']], function () {
    Route::post('auth/logout', 'Auth\LoginController@logout');
    //SETTING DATA
    Route::group(['namespace' => 'Settings'], function () {

        Route::get('auth/me', 'UserController@getUserMe');
        Route::put('settings/profile', 'ProfileController@updateAuthUser');
        Route::put('settings/password', 'ProfileController@updatePasswordAuthUser');
        Route::post('settings/profile/uploadPhoto', 'ProfileController@uploadAvatarAuthUser');
        Route::post('settings/profile/resetPhoto', 'ProfileController@removeAvatarAuthUser');

        Route::group(['prefix' => 'users'], function () {
            Route::get('/getUserRoles/{user}', 'UserController@getUserRoles');
            Route::get('/count', 'UserController@count');
            Route::get('/getAll', 'UserController@getAll');
            Route::post('/filter', 'UserController@filter')->middleware('permission:read-user');
            Route::get('/{user}', 'UserController@show')->middleware('permission:read-user');
            Route::get('/all', 'UserController@getAll');
            Route::post('/store', 'UserController@store')->middleware('permission:create-user');
            Route::put('/update/{user}', 'UserController@update')->middleware('permission:update-user');
            Route::delete('/{user}', 'UserController@destroy')->middleware('permission:delete-user');
            Route::post('/recover/{user}', 'UserController@recover')->middleware('permission:create-user');
            Route::post('/import', 'UserController@import')->middleware('permission:create-user');
            Route::post('/filterMap', 'UserController@filterMap')->middleware('permission:read-user');
        });

        Route::group(['prefix' => 'logs'], function () {
            Route::post('/filter', 'LogsController@filter');
        });


        Route::group(['prefix' => 'settings'], function () {
            Route::put('/update', 'SettingsController@update');
            Route::get('/', 'SettingsController@filter');
        });

        Route::group(['namespace' => 'Roles'], function () {
            Route::group(['prefix' => 'roles'], function () {
                Route::get('/', 'RoleController');
                Route::get('/count', 'RoleController@count');
                Route::get('/all', 'RoleController@all');
                Route::get('/getRoleModulesPermissions/{role}', 'RoleController@getRoleModulesPermissions');
                Route::post('/filter', 'RoleController@filter')->middleware('permission:read-roles');
                Route::get('/{role}', 'RoleController@show')->middleware('permission:read-roles');
                Route::post('/store', 'RoleController@store')->middleware('permission:create-roles');
                Route::put('/update/{role}', 'RoleController@update')->middleware('permission:update-roles');
                Route::delete('/{user}', 'RoleController@destroy')->middleware('permission:delete-roles');
            });

            Route::group(['prefix' => 'modules'], function() {
                Route::get('/getModulesPermissions', 'ModuleController@getModulesPermissions');
            });

            Route::group(['prefix' => 'permissions'], function () {
                Route::get('/', 'PermissionController');
                Route::get('/count', 'PermissionController@count');
                Route::get('/all', 'PermissionController@all');

            });

        });
    });

    //MASTER DATA
    Route::group(['namespace' => 'Master'], function () {

        Route::group(['prefix' => 'master'], function () {
            Route::group(['prefix' => '/checklists'], function () {
                Route::get('/count', 'ChecklistsController@count');
                Route::get('/all', 'ChecklistsController@all');
                Route::get('/allByCustomer', 'ChecklistsController@allByCustomer');
                Route::get('/getDeleted', 'ChecklistsController@getDeleted')->middleware('permission:read-checklist');
                Route::post('/filter', 'ChecklistsController@filter')->middleware(['permission:read-checklist']);
                Route::get('/{id}', 'ChecklistsController@show')->middleware('permission:read-checklist');
                Route::post('/store', 'ChecklistsController@store')->middleware('permission:create-checklist');
                Route::put('/update/{id}', 'ChecklistsController@update')->middleware('permission:update-checklist');
                Route::put('/changeStatus/{id}', 'ChecklistsController@updateStatus')->middleware('permission:update-checklist');
                Route::post('/recover/{id}', 'ChecklistsController@recover')->middleware('permission:update-checklist');
                Route::delete('/{id}', 'ChecklistsController@destroy')->middleware('permission:delete-servicebase');
            });

            Route::group(['prefix' => '/servicebases'], function() {
                Route::get('/count', 'ServicebasesController@count');
                Route::get('/all', 'ServicebasesController@all');
                Route::post('/byRegion', 'ServicebasesController@byRegion');
                Route::post('/filter', 'ServicebasesController@filter')->middleware('permission:read-servicebase');
                Route::get('/{id}', 'ServicebasesController@show')->middleware('permission:read-servicebase');
                Route::post('/store', 'ServicebasesController@store')->middleware('permission:create-servicebase');
                Route::put('/update/{id}', 'ServicebasesController@update')->middleware('permission:update-servicebase');
                Route::post('/import', 'ServicebasesController@import')->middleware('permission:create-servicebase');
                Route::post('/recover/{id}', 'ServicebasesController@recover')->middleware('permission:update-servicebase');
                Route::delete('/{id}', 'ServicebasesController@destroy')->middleware('permission:delete-servicebase');
            });

            Route::group(['prefix' => '/regions'], function() {
                Route::get('/count', 'RegionsController@count');
                Route::get('/all', 'RegionsController@all');
                Route::get('/byServiceBase', 'RegionsController@byServiceBase');
                Route::get('/export', 'RegionsController@exportData')->middleware('permission:read-region');
                Route::get('/{id}', 'RegionsController@show')->middleware('permission:read-region');
                Route::post('/filter', 'RegionsController@filter')->middleware('permission:read-region');
                Route::post('/store', 'RegionsController@store')->middleware('permission:create-region');
                Route::post('/import', 'RegionsController@import')->middleware('permission:create-region');
                Route::post('/recover/{id}', 'RegionsController@recover')->middleware('permission:update-region');
                Route::put('/update/{id}', 'RegionsController@update')->middleware('permission:update-region');
                Route::delete('/{id}', 'RegionsController@destroy')->middleware('permission:delete-region');
            });

            Route::group(['prefix' => '/wo-types'], function() {
                Route::get('/count', 'WoTypeController@count');
                Route::get('/all', 'WoTypeController@all');
                Route::get('/export', 'WoTypeController@exportData')->middleware('permission:read-region');
                Route::post('/import', 'WoTypeController@import')->middleware('permission:create-region');
                Route::get('/{id}', 'WoTypeController@show')->middleware('permission:read-wo-type');
                Route::post('/filter', 'WoTypeController@filter')->middleware('permission:read-wo-type');
                Route::post('/store', 'WoTypeController@store')->middleware('permission:create-wo-type');
                Route::put('/update/{id}', 'WoTypeController@update')->middleware('permission:update-wo-type');
                Route::delete('/{id}', 'WoTypeController@destroy')->middleware('permission:delete-wo-type');
            });

            Route::group(['prefix' => '/parts'], function() {
                Route::get('/count', 'PartNumberController@count');
                Route::get('/all', 'PartNumberController@all');
                Route::post('/filter', 'PartNumberController@filter')->middleware('permission:read-part_number');
                Route::post('/byType', 'PartNumberController@byType')->middleware('permission:read-part_number');
                Route::get('/{id}', 'PartNumberController@show')->middleware('permission:read-part_number');
                Route::post('/store', 'PartNumberController@store')->middleware('permission:create-part_number');
                Route::put('/update/{id}', 'PartNumberController@update')->middleware('permission:update-part_number');
                Route::delete('/{id}', 'PartNumberController@destroy')->middleware('permission:delete-part_number');
                Route::post('/import', 'PartNumberController@import')->middleware('permission:create-part_number');
                Route::post('/recover/{id}', 'PartNumberController@recover')->middleware('permission:update-part_number');
            });

            Route::group(['prefix' => '/customers'], function() {
                Route::get('/count', 'CustomersController@count');
                Route::get('/all', 'CustomersController@all');
                Route::get('/getDeleted', 'CustomersController@getDeleted')->middleware('permission:read-customer');
                Route::post('/filter', 'CustomersController@filter')->middleware(['permission:read-customer']);
                Route::get('/{id}', 'CustomersController@show')->middleware('permission:read-customer');
                Route::post('/store', 'CustomersController@store')->middleware('permission:create-customer');
                Route::put('/update/{id}', 'CustomersController@update')->middleware('permission:update-customer');
                Route::put('/restore/{id}', 'CustomersController@restore')->middleware('permission:update-customer');
                Route::post('/recover/{id}', 'CustomersController@recover')->middleware('permission:update-customer');
                Route::post('/import', 'CustomersController@import')->middleware('permission:create-customer');
                Route::delete('/{id}', 'CustomersController@destroy')->middleware('permission:delete-customer');
            });

                Route::group(['prefix' => '/vendors'], function() {
                    Route::get('/count', 'VendorController@count');
                    Route::get('/all', 'VendorController@all');
                    Route::get('/getDeleted', 'VendorController@getDeleted')->middleware('permission:read-vendor');
                    Route::post('/filter', 'VendorController@filter')->middleware(['permission:read-vendor']);
                    Route::get('/{id}', 'VendorController@show')->middleware('permission:read-vendor');
                    Route::post('/store', 'VendorController@store')->middleware('permission:create-vendor');
                    Route::put('/update/{id}', 'VendorController@update')->middleware('permission:update-vendor');
                    Route::put('/restore/{id}', 'VendorController@restore')->middleware('permission:update-vendor');
                    Route::post('/recover/{id}', 'VendorController@recover')->middleware('permission:update-vendor');
                    Route::delete('/{id}', 'VendorController@destroy')->middleware('permission:delete-vendor');
                });

            Route::group(['prefix' => '/locations'], function() {
                Route::get('/count', 'LocationsController@count');
                Route::get('/countMap', 'LocationsController@countMap');
                Route::get('/all', 'LocationsController@all');
                Route::get('/{id}', 'LocationsController@show')->middleware('permission:read-location');
                Route::post('/filter', 'LocationsController@filter')->middleware('permission:read-location');
                Route::post('/filterMap', 'LocationsController@filterMap')->middleware('permission:read-location');
                Route::post('/filterCustomer', 'LocationsController@filterCustomer')->middleware('permission:read-location');
                Route::post('/filterPosition', 'LocationsController@filterPosition')->middleware('permission:read-location');
                Route::post('/filterExport', 'LocationsController@filterExport')->middleware('permission:read-location');
                Route::post('/byCustomer', 'LocationsController@byCustomer')->middleware('permission:read-location');
                Route::post('/store', 'LocationsController@store')->middleware('permission:create-location');
                Route::post('/recover/{id}', 'LocationsController@recover')->middleware('permission:update-location');
                Route::post('/import', 'LocationsController@import')->middleware('permission:create-location');
                Route::post('/uploadPhoto', 'LocationsController@uploadPhoto')->middleware('permission:update-location');
                Route::put('/update/{id}', 'LocationsController@update')->middleware('permission:update-location');
                Route::delete('/{id}', 'LocationsController@destroy')->middleware('permission:delete-location');
            });
        });

    });

    Route::group(['namespace' => 'Visits'], function () {

            Route::group(['prefix' => '/visits'], function () {
                Route::get('/getByWo', 'VisitController@getByWo');
                Route::get('/countPerDay', 'VisitController@countPerDay');
                Route::get('/countPerMonth', 'VisitController@countPerMonth');
                Route::get('/countPerRegion', 'VisitController@countPerRegion');
                Route::get('/counPerRegionPerDay', 'VisitController@counPerRegionPerDay');
                Route::get('/countOpenPerUser', 'VisitController@countPerUser');
                Route::get('/countPerDayByRole', 'VisitController@countPerDayByRole');
                Route::get('/countTotalByRole', 'VisitController@countTotalByRole');
                Route::get('/all', 'VisitController@all');
                Route::get('/getDeleted', 'VisitController@getDeleted')->middleware('permission:read-visit');
                Route::get('/{id}', 'VisitController@show')->middleware('permission:read-visit');
                Route::post('/getReport', 'VisitController@getReport')->middleware('permission:read-visit');
                Route::post('/getReport2', 'VisitController@getReport2')->middleware('permission:read-visit');
                Route::post('/filter', 'VisitController@filter')->middleware(['permission:read-visit']);
                Route::post('/filter2', 'VisitController@filter2')->middleware(['permission:read-visit']);
                Route::post('/store', 'VisitController@store')->middleware('permission:create-visit');
                Route::post('/uploadAr', 'VisitController@uploadPhotoAr')->middleware('permission:update-visit');
                Route::post('/upload', 'VisitChecklistsController@uploadPhoto')->middleware('permission:update-visit');
                Route::post('/recover/{id}', 'VisitController@recover')->middleware('permission:update-visit');
                Route::put('/checkout/{id}', 'VisitController@checkout')->middleware('permission:update-visit');
                Route::put('/update/{id}', 'VisitController@update')->middleware('permission:update-visit');
                Route::put('/changeStatus/{id}', 'VisitController@updateStatus')->middleware('permission:update-visit');
                Route::delete('/{id}', 'VisitController@destroy')->middleware('permission:delete-visit');
            });

        Route::group(['prefix' => '/visits/history'], function () {
            Route::get('/{id}', 'VisitHistoryController@show')->middleware('permission:read-visit-history');
            Route::post('/filter', 'VisitHistoryController@filter')->middleware(['permission:read-visit-history']);
            Route::put('/update/{id}', 'VisitHistoryController@update')->middleware('permission:update-visit-history');
            Route::delete('/{id}', 'VisitHistoryController@destroy')->middleware('permission:delete-visit-history');
        });

        Route::group(['prefix' => '/parts'], function() {
            Route::get('/report', 'SparepartController@getReport')->middleware('permission:read-visit');
            Route::get('/{id}', 'SparepartController@show')->middleware('permission:create-visit');
            Route::post('/filter', 'SparepartController@filter')->middleware('permission:read-visit');
            Route::put('/update/{id}', 'SparepartController@update')->middleware('permission:update-visit');
        });

        Route::group(['prefix' => '/outstanding'], function() {
            Route::get('/count', 'OpenWoController@count');
            Route::get('/countByEnginer', 'OpenWoController@countByEnginer');
            Route::get('/countPerEnginer', 'OpenWoController@countPerEnginer');
            Route::get('/countPerRegion', 'OpenWoController@countPerRegion');
            Route::get('/getByUser', 'OpenWoController@getByUser')->middleware('permission:read-open-wo');
            Route::get('/{id}', 'OpenWoController@show')->middleware('permission:read-open-wo');
            Route::post('/filter', 'OpenWoController@filter')->middleware('permission:read-open-wo');
            Route::put('/update/{id}', 'OpenWoController@update')->middleware('permission:update-open-wo');
            Route::post('/store', 'OpenWoController@store')->middleware('permission:create-open-wo');
            Route::delete('/{id}', 'OpenWoController@destroy')->middleware('permission:delete-open-wo');
            Route::post('/import', 'OpenWoController@import')->middleware('permission:create-open-wo');
            Route::post('/destroyClose', 'OpenWoController@deleteClose')->middleware('permission:delete-open-wo');
            Route::post('/export', 'OpenWoController@export')->middleware('permission:read-open-wo');
        });
    });

    Route::group(['namespace' => 'Task'], function () {
        Route::group(['prefix' => '/task'], function () {
            Route::get('/{id}', 'TaskController@show')->middleware('permission:read-task');
            Route::get('getDetailByTask/{id}', 'TaskDetailController@getDetailByTask')->middleware('permission:read-task');
            Route::post('/filter', 'TaskController@filter')->middleware(['permission:read-task']);
            Route::post('/store', 'TaskController@store')->middleware('permission:create-task');
            Route::put('/update/{id}', 'TaskController@update')->middleware('permission:update-task');
            Route::delete('/{id}', 'TaskController@destroy')->middleware('permission:delete-task');

            Route::group(['prefix' => '/user'], function () {
                Route::get('/{id}', 'TaskUserController@show')->middleware('permission:read-task');
                Route::get('/getByTask/{id}', 'TaskUserController@getByTask')->middleware(['permission:read-task']);
                Route::post('/filter', 'TaskUserController@filter')->middleware(['permission:read-task']);
                Route::post('/store', 'TaskUserController@store')->middleware('permission:create-task');
                Route::put('/update/{id}', 'TaskUserController@update')->middleware('permission:update-task');
                Route::delete('/{id}', 'TaskUserController@destroy')->middleware('permission:delete-task');

                Route::group(['prefix' => '/detail'], function () {
                    Route::get('/{id}', 'TaskDetailController@show')->middleware('permission:read-task');
                    Route::post('/filter', 'TaskDetailController@filter')->middleware(['permission:read-task']);
                    Route::post('/store', 'TaskDetailController@store')->middleware('permission:create-task');
                    Route::put('/update/{id}', 'TaskDetailController@update')->middleware('permission:update-task');
                    Route::delete('/{id}', 'TaskDetailController@destroy')->middleware('permission:delete-task');
                });

            });

        });
    });

    Route::group(['namespace' => 'TT'], function () {
        Route::group(['prefix' => '/tt'], function () {
            Route::get('/{id}', 'TroubleTicketController@show')->middleware('permission:read-tt');
            Route::post('/filter', 'TroubleTicketController@filter')->middleware(['permission:read-tt']);
            Route::post('/store', 'TroubleTicketController@store')->middleware('permission:create-tt');
            Route::put('/update/{id}', 'TroubleTicketController@update')->middleware('permission:update-tt');
            Route::put('/updateCheckin/{id}', 'TroubleTicketController@updateCheckin')->middleware('permission:update-tt');
            Route::delete('/{id}', 'TroubleTicketController@destroy')->middleware('permission:delete-tt');
        });
    });

    Route::get('/notification', 'NotificationController@notification');



});
Route::group(['middleware' => 'guest:api'], function () {
    Route::post('auth/login', 'Auth\LoginController@login');
    Route::post('auth/register', 'Auth\RegisterController@register');
    Route::post('auth/refresh-token', 'Auth\LoginController@refresh');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');

    Route::group(['prefix' => 'config'], function () {
        Route::get('/', 'Settings\SettingsController@filter');
    });
});
